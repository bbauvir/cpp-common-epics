/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/test/c++/unit/PVAccessMonitor-tests.cpp $
* $Id: PVAccessMonitor-tests.cpp 114049 2020-10-20 06:28:48Z bauvirb $
*
* Project	: CODAC Core System
*
* Description	: Unit test code
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2019 ITER Organization,
*				  CS 90 046
*				  13067 St. Paul-lez-Durance Cedex
*				  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <gtest/gtest.h> // Google test framework

#include <common/BasicTypes.h> // Misc. type definition
#include <common/SysTools.h> // Misc. helper functions
#include <common/TimeTools.h> // Misc. helper functions

#include <common/log-api.h> // Syslog wrapper routines

#include <common/PVMonitor.h>

// Local header files

#include "PVAccessTypes.h"
#include "PVAccessServer.h"
#include "PVAccessMonitor.h"

#include "PVMonitorCache.h"

// Constants

// Type definition

// Function declaration

// Global variables

static ccs::log::Func_t __handler = ccs::log::SetStdout();
static ccs::base::PVAccessServer* __pva_srv = static_cast<ccs::base::PVAccessServer*>(NULL);

// Function definition

static inline bool Initialise (void)
{

  __pva_srv = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>();

  bool status = (static_cast<ccs::base::PVAccessServer*>(NULL) != __pva_srv);

  return status;

}

static inline bool Terminate (void)
{

  bool status = (static_cast<ccs::base::PVAccessServer*>(NULL) != __pva_srv);

  if (status)
    {
      ccs::base::PVAccessInterface::Terminate<ccs::base::PVAccessServer>();
      __pva_srv = static_cast<ccs::base::PVAccessServer*>(NULL);
    }

  return status;

}

TEST(Bug13512, Default) // Static initialisation
{
  bool ret = Initialise();

  if (ret)
    {
      ret = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->AddVariable("BUG13512:TEST:STRUCT", ccs::types::AnyputVariable, ccs::base::PVAccessTypes::Collection_int);
    }
  
  if (ret)
    {
      ret = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->SetPeriod(10000000ul);
    }

  if (ret)
    {
      ret = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->Launch();
    }

  ccs::base::gtest::PVMonitorCache cache;

  if (ret)
    {
      ret = cache.SetChannel("BUG13512:TEST:STRUCT");
    }
  
  if (ret)
    {
      ccs::HelperTools::SleepFor(100000000ul);
    }
  
  // Write to variable
  if (ret)
    {
      ret = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->IsValid("BUG13512:TEST:STRUCT");
    }

  if (ret)
    {
      log_info("TEST(Bug13512, Default) - Update array ..");
      ccs::types::AnyValue* _value = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->GetVariable("BUG13512:TEST:STRUCT");
      ret = (ccs::HelperTools::SetAttributeValue(_value, "arrays.uint32[0]", 1u) &&
	     ccs::HelperTools::SetAttributeValue(_value, "arrays.uint32[1]", 2u));
    }

  if (ret)
    {
      log_info("TEST(Bug13512, Default) - Update array ..");
      ccs::types::AnyValue* _value = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->GetVariable("BUG13512:TEST:STRUCT");
      ret = (ccs::HelperTools::SetAttributeValue(_value, "arrays.uint32[2]", 3u) &&
	     ccs::HelperTools::SetAttributeValue(_value, "arrays.uint32[3]", 4u));
    }

  if (ret)
    {
      ccs::types::char8 buffer [1024];
      ccs::types::AnyValue* _value = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->GetVariable("BUG13512:TEST:STRUCT");
      _value->SerialiseInstance(buffer, 1024u);      
      log_info("TEST(Bug13512, Default) - Caching '%s' ..", buffer);
    }

  if (ret)
    {
      log_info("TEST(Bug13512, Default) - Trigger variable publication ..");
      ret = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->UpdateVariable("BUG13512:TEST:STRUCT");
      ccs::HelperTools::SleepFor(100000000ul);
    }

  if (ret)
    { // Compose system command
      std::string command = std::string("/usr/bin/pvget -v BUG13512:TEST:STRUCT > /tmp/pvget.out");
      ret = (std::system(command.c_str()) == 0);
      std::stringstream sstr; sstr << std::ifstream("/tmp/pvget.out").rdbuf();
      std::string result = sstr.str();
      log_info("TEST(Bug13512, Default) - Check variable on network ..");
      std::cout << result << std::endl;
     }

  ccs::types::AnyValue value; // Placeholder

  if (ret)
    {
      log_info("TEST(Bug13512, Default) - Read from cache ..");
      ret = cache.GetValue(value);
    }

  if (ret)
    {
      ret = static_cast<bool>(value.GetType());
    }

  if (ret)
    {
      ret = ((1u == ccs::HelperTools::GetAttributeValue<ccs::types::uint32>(&value, "arrays.uint32[0]")) &&
	     (2u == ccs::HelperTools::GetAttributeValue<ccs::types::uint32>(&value, "arrays.uint32[1]")) &&
	     (3u == ccs::HelperTools::GetAttributeValue<ccs::types::uint32>(&value, "arrays.uint32[2]")) &&
	     (4u == ccs::HelperTools::GetAttributeValue<ccs::types::uint32>(&value, "arrays.uint32[3]")));
    }

  (void)Terminate();
  (void)ccs::HelperTools::SleepFor(100000000ul);

  ASSERT_EQ(true, ret);
}

