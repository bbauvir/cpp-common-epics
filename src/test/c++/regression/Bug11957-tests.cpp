/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/test/c++/regression/Bug11957-tests.cpp $
* $Id: Bug11957-tests.cpp 112183 2020-08-06 07:36:26Z bauvirb $
*
* Project	: CODAC Core System
*
* Description	: Unit test code
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2019 ITER Organization,
*				  CS 90 046
*				  13067 St. Paul-lez-Durance Cedex
*				  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <gtest/gtest.h> // Google test framework

#include <common/BasicTypes.h> // Misc. type definition
#include <common/SysTools.h> // Misc. helper functions
#include <common/TimeTools.h> // Misc. helper functions

#include <common/log-api.h> // Syslog wrapper routines

// Local header files

#include "ChannelAccessClient.h"

// Constants

// Type definition

// Function declaration

// Global variables

static ccs::log::Func_t __handler = ccs::log::SetStdout();
static ccs::base::ChannelAccessClient* __ca_client = static_cast<ccs::base::ChannelAccessClient*>(NULL);

// Function definition

static inline bool Initialise (void)
{

  __ca_client = ccs::base::ChannelAccessInterface::GetInstance<ccs::base::ChannelAccessClient>();

  bool status = (static_cast<ccs::base::ChannelAccessClient*>(NULL) != __ca_client);

  if (status)
    { // Start test IOC
      // Compose system command
      std::string command = std::string("/usr/bin/screen -d -m /usr/bin/softIoc -d ./regression/Bug11957-tests.db > /tmp/ioc.out");
      status = (std::system(command.c_str()) == 0);
     }

  if (status)
    {
      ccs::HelperTools::SleepFor(1000000000ul);
    }
#if 0
  if (status)
    { // Compose system command
      std::string command = std::string("/usr/bin/caget D1:COS-ALIVE D1:COS-OPREQ D1:COS-OPSTATE > /tmp/caget.out");
      status = (std::system(command.c_str()) == 0);
      std::stringstream sstr; sstr << std::ifstream("/tmp/caget.out").rdbuf();
      std::string result = sstr.str();
      log_info("TEST(ChannelAccessClient, Initialise) - Check variables exist ..");
      std::cout << result << std::endl;
     }
#endif
  return status;

}

static inline bool Terminate (void)
{

  bool status = (static_cast<ccs::base::ChannelAccessClient*>(NULL) != __ca_client);

  if (status)
    {
      ccs::base::ChannelAccessInterface::Terminate<ccs::base::ChannelAccessClient>();
      __ca_client = static_cast<ccs::base::ChannelAccessClient*>(NULL);
    }

  if (status)
    { // Stop test IOC
      // Compose system command
      std::string command = std::string("/usr/bin/kill -9 `/usr/sbin/pidof softIoc`");
      status = (std::system(command.c_str()) == 0);
     }

  return status;

}

TEST(Bug11957_Test, Default) // Static initialisation
{
  bool ret = Initialise();

  if (ret)
    {
      ret = ccs::base::ChannelAccessInterface::GetInstance<ccs::base::ChannelAccessClient>()->AddVariable("D1:COS-ALIVE", ccs::types::AnyputVariable, ccs::types::UnsignedInteger32);
    }

  if (ret)
    {
      ret = ccs::base::ChannelAccessInterface::GetInstance<ccs::base::ChannelAccessClient>()->AddVariable("D1:COS-OPREQ", ccs::types::AnyputVariable, ccs::types::String);
    }

  if (ret)
    {
      ret = ccs::base::ChannelAccessInterface::GetInstance<ccs::base::ChannelAccessClient>()->AddVariable("D1:COS-OPSTATE", ccs::types::AnyputVariable, ccs::types::String);
    }
#if 0
  if (ret)
    {
      ret = ccs::base::ChannelAccessInterface::GetInstance<ccs::base::ChannelAccessClient>()->SetPeriod(10000000ul);
    }
#endif
  if (ret)
    {
      ret = ccs::base::ChannelAccessInterface::GetInstance<ccs::base::ChannelAccessClient>()->Launch();
    }

  ccs::HelperTools::SleepFor(1000000000ul);

  log_info("TEST(Bug11957, Default) - Check variables exist ..");

  if (ret)
    {
      ccs::types::string variable;
      ccs::HelperTools::SafeStringCopy(variable, reinterpret_cast<char*>(ccs::base::ChannelAccessInterface::GetInstance<ccs::base::ChannelAccessClient>()->GetVariable("D1:COS-OPREQ")->GetInstance()), ccs::types::MaxStringLength);
      log_info("TEST(Bug11957, Default) - .. '%s'", variable);
    }

  if (ret)
    {
      ccs::types::string variable;
      ccs::HelperTools::SafeStringCopy(variable, reinterpret_cast<char*>(ccs::base::ChannelAccessInterface::GetInstance<ccs::base::ChannelAccessClient>()->GetVariable("D1:COS-OPSTATE")->GetInstance()), ccs::types::MaxStringLength);
      log_info("TEST(Bug11957, Default) - .. '%s'", variable);
    }

  if (ret)
    {
      ccs::types::string variable; ccs::HelperTools::SafeStringCopy(variable, "Execute", ccs::types::MaxStringLength);
      ret = ccs::base::ChannelAccessInterface::GetInstance<ccs::base::ChannelAccessClient>()->SetVariable("D1:COS-OPREQ", variable);
    }

  if (ret)
    {
      ccs::types::string variable; ccs::HelperTools::SafeStringCopy(variable, "Executing", ccs::types::MaxStringLength);
      ret = ccs::base::ChannelAccessInterface::GetInstance<ccs::base::ChannelAccessClient>()->SetVariable("D1:COS-OPSTATE", variable);
    }

  ccs::HelperTools::SleepFor(1000000000ul);

  if (ret)
    { // Compose system command
      std::string command = std::string("/usr/bin/caget D1:COS-ALIVE D1:COS-OPREQ D1:COS-OPSTATE > /tmp/caget.out");
      ret = (std::system(command.c_str()) == 0);
      std::stringstream sstr; sstr << std::ifstream("/tmp/caget.out").rdbuf();
      std::string result = sstr.str();
      log_info("TEST(Bug11957, Default) - Check variables have been updated ..");
      std::cout << result << std::endl;
     }

  if (ret)
    {
      ccs::types::string variable;
      ccs::HelperTools::SafeStringCopy(variable, reinterpret_cast<char*>(ccs::base::ChannelAccessInterface::GetInstance<ccs::base::ChannelAccessClient>()->GetVariable("D1:COS-OPREQ")->GetInstance()), ccs::types::MaxStringLength);
      log_info("TEST(Bug11957, Default) - .. '%s'", variable);
    }

  if (ret)
    {
      ccs::types::string variable;
      ccs::HelperTools::SafeStringCopy(variable, reinterpret_cast<char*>(ccs::base::ChannelAccessInterface::GetInstance<ccs::base::ChannelAccessClient>()->GetVariable("D1:COS-OPSTATE")->GetInstance()), ccs::types::MaxStringLength);
      log_info("TEST(Bug11957, Default) - .. '%s'", variable);
    }

  if (ret)
    {
      ret = Terminate();
    }

  ASSERT_EQ(true, ret);
}

