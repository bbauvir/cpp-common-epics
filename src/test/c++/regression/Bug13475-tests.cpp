/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/branches/codac-core-6.3/src/test/c++/unit/ChannelAccessClient-tests.cpp $
* $Id: ChannelAccessClient-tests.cpp 116150 2021-01-04 09:45:04Z kimh19 $
*
* Project	: CODAC Core System
*
* Description	: Unit test code
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2021 ITER Organization,
*				  CS 90 046
*				  13067 St. Paul-lez-Durance Cedex
*				  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <cadef.h>

#include <gtest/gtest.h> // Google test framework

#include <common/BasicTypes.h> // Misc. type definition
#include <common/SysTools.h> // Misc. helper functions
#include <common/TimeTools.h> // Misc. helper functions

#include <common/log-api.h> // Syslog wrapper routines

// Local header files

#include "ChannelAccessClient.h"

// Constants

// Type definition

// Function declaration

// Global variables

static ccs::log::Func_t __handler = ccs::log::SetStdout();
static ccs::base::ChannelAccessClient* __ca_client = static_cast<ccs::base::ChannelAccessClient*>(NULL);

// Function definition

static inline bool Initialise (void)
{

  __ca_client = ccs::base::ChannelAccessInterface::GetInstance<ccs::base::ChannelAccessClient>();

  bool status = (static_cast<ccs::base::ChannelAccessClient*>(NULL) != __ca_client);

  if (status)
    { // Start test IOC
      status = (std::system("/usr/bin/screen -d -m /usr/bin/softIoc -d ./regression/Bug13475-tests.db &> /dev/null") == 0);
     }

  if (status)
    {
      (void)ccs::HelperTools::SleepFor(1000000000ul);
    }

  if (status)
    {
      status = (std::system("/usr/bin/caget BUG13475-TESTS:ENUM > /tmp/caget.out") == 0);
      std::stringstream sstr; sstr << std::ifstream("/tmp/caget.out").rdbuf();
      std::string result = sstr.str();
      log_info("TEST(Bug13475, Initialise) - Check variable exists ..");
      std::cout << result << std::endl;
     }

  return status;

}

static inline bool Terminate (void)
{

  bool status = (static_cast<ccs::base::ChannelAccessClient*>(NULL) != __ca_client);

  if (status)
    {
      ccs::base::ChannelAccessInterface::Terminate<ccs::base::ChannelAccessClient>();
      __ca_client = static_cast<ccs::base::ChannelAccessClient*>(NULL);
    }

  if (status)
    { // Stop test IOC
      status = (std::system("/usr/bin/kill -9 `/usr/sbin/pidof softIoc`") == 0);
    }

  return status;

}

TEST(Bug13475, Default)
{
  bool ret = Initialise();

  if (ret)
    {
      ret = __ca_client->AddVariable("BUG13475-TESTS:ENUM", ccs::types::AnyputVariable, ccs::types::String);
    }

  if (ret)
    {
      (void)__ca_client->SetPeriod(10000000ul);
      ret = __ca_client->Launch();
    }

  (void)ccs::HelperTools::SleepFor(1000000000ul);

  // A 'string' variable in cache is defaulted to empty string and only after successful
  // monitor has happened is the variable in the cache not empty anymore.

  if (ret)
    {
      ret = (false == ccs::HelperTools::IsUndefinedString(reinterpret_cast<char*>(__ca_client->GetVariable("BUG13475-TESTS:ENUM")->GetInstance())));
    }

  if (ret)
    {
      ret = Terminate();
    }

  ASSERT_EQ(true, ret);
}

