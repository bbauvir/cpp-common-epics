/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/test/c++/pva-record/pva-record.cpp $
* $Id: pva-record.cpp 111795 2020-07-20 06:38:56Z bauvirb $
*
* Project	: CODAC Core System
*
* Description	: SDN Software - Communication API - Simple examples
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2019 ITER Organization,
*				  CS 90 046
*				  13067 St. Paul-lez-Durance Cedex
*				  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <stdio.h> // sscanf, printf, etc.
#include <string.h> // strncpy, etc.
#include <stdarg.h> // va_start, etc.
#include <signal.h> // sigset, etc.

#include <new> // std::nothrow

#include <common/BasicTypes.h> // Misc. type definition, e.g. RET_STATUS

#include <common/Base64.h> // Encoding helper functions

#include <common/log-api.h> // Logging helper functions

#include <common/PVMonitor.h>
#include <common/PVMonitorImplFactory.h>

// Local header files

#include "PVAccessMonitor.h"

// Constants

#define DEFAULT_CHANNEL_NAME "rpc@service"

// Type definition

class PVRecordImpl : public ::ccs::base::PVMonitor
{

  private:
  
    std::ofstream __file;
    ::ccs::types::boolean __init = false;
  
  public:
  
  PVRecordImpl (const ::ccs::types::char8 * const name) : ::ccs::base::PVMonitor(name) {};
  
  virtual ~PVRecordImpl (void) {

    if (__init)
      {
        __file << "null" << std::endl << "]}" << std::endl;
	__file.close();
	__init = false;
      } 

    return;

  };
  
  virtual void HandleMonitor (const ccs::types::AnyValue& value) {
    
    log_info("Channel '%s' updated at '%lu'", this->GetChannel(), ccs::HelperTools::GetCurrentTime());

    char buffer [4096u] = STRING_UNDEFINED;
    bool status = true;

    if (!__init)
      {
	std::string file = std::string("/tmp/") + std::string(this->GetChannel()) + std::string(".record");
	__file.open (file, { std::ios::out | std::ios::trunc });

	// Header with recovered datatype
	status = value.SerialiseType(buffer, 4096u);

	if (status)
	  {
	    __file << "{\"datatype\":" << buffer << "," << std::endl;
#ifdef SAMPLES_AS_JSON_OBJECTS
	    __file << " \"encoding\":\"json\"," << std::endl;
#else
	    __file << " \"encoding\":\"base64\"," << std::endl;
#endif
	    __file << " \"monitors\":[" << std::endl;
	  }

	__init = true;
      }
    
    if (status)
      {
#ifdef SAMPLES_AS_JSON_OBJECTS
	status = value.SerialiseInstance(buffer, 4096u);
#else
	status = (0u < ::ccs::HelperTools::Base64Encode(static_cast<::ccs::types::uint8*>(value.GetInstance()), value.GetSize(), buffer, 4096u));
#endif
      }

    if (status)
      {
#ifdef SAMPLES_AS_JSON_OBJECTS
	__file << buffer << "," << std::endl;
#else
	__file << "\"" << buffer << "\"," << std::endl;
#endif
      }
    
  };
  
};

// Global variables

bool _terminate = false;

// Function definition

void print_usage (void)
{

  char prog_name [STRING_MAX_LENGTH] = STRING_UNDEFINED;

  get_program_name((char*) prog_name);

  fprintf(stdout, "Usage: %s <options>\n", prog_name);
  fprintf(stdout, "Options: -h|--help: Print usage.\n");
  fprintf(stdout, "         -n|--name <channel>: Try and connect the PVA monitor to the named channel, defaults to '%s'.\n", DEFAULT_CHANNEL_NAME);
  fprintf(stdout, "         -v|--verbose: Logs to stdout.\n");
  fprintf(stdout, "\n");
  fprintf(stdout, "The program instantiates a PVA monitor class and dumps monitored samples to file, etc.\n");
  fprintf(stdout, "\n");

  return;

};

void signal_handler (int signal)
{

  log_info("Received signal '%d' to terminate", signal);
  _terminate = true;

};

int main (int argc, char** argv) 
{

  using namespace ::ccs::types;
  using namespace ::ccs::base;

  // Install signal handler to support graceful termination
  sigset(SIGTERM, signal_handler);
  sigset(SIGINT,  signal_handler);
  sigset(SIGHUP,  signal_handler);

  ::ccs::types::string name = STRING_UNDEFINED; ::ccs::HelperTools::SafeStringCopy(name, DEFAULT_CHANNEL_NAME, MaxStringLength);

  if (argc > 1)
    {
      for (uint_t index = 1; index < (uint_t) argc; index++)
	{
          if ((strcmp(argv[index], "-h") == 0) || (strcmp(argv[index], "--help") == 0))
	    {
	      // Display usage
	      print_usage();
	      return (0);
	    }
	  else if ((strcmp(argv[index], "-n") == 0) || (strcmp(argv[index], "--name") == 0))
	    {
	      if ((index + 1) < (uint_t) argc) ::ccs::HelperTools::SafeStringCopy(name, argv[index + 1], MaxStringLength);
	      else { print_usage(); return (0); } // Display usage
	      index += 1;
            
	    }
	  else if ((strcmp(argv[index], "-v") == 0) || (strcmp(argv[index], "--verbose") == 0))
	    {
	      // Log to stdout
	      ccs::log::Func_t old_plugin = ccs::log::SetStdout();
	      ccs::log::Severity_t old_filter = ccs::log::SetFilter(LOG_DEBUG);

	    }
	}
    }
  else
    {
    }

  // Create instance
  log_info("Create PVA monitor on channel '%s'", name);
  (void)::ccs::base::PVMonitorImplFactory::GetInstance()->SetDefault(PVAccessMonitor_Plugin_Name); // Default plug-in in factory
  PVRecordImpl client (name); (void)::ccs::HelperTools::SleepFor(500000000ul);

  // Connect to server
  bool status = client.IsConnected();

  if (status)
    {
      log_info("Connected to '%s'", name);
    }
  else
    {
      log_error("Unable to connect to '%s'", name);
    }

  while (false == _terminate)
    {
      ::ccs::HelperTools::SleepFor(100000000ul);
    }

  // Terminate
  log_info("Terminate program");

  return (0);

}
