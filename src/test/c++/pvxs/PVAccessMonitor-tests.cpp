/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-sup-common-cpp/branches/codac-core-6.2/src/test/c++/unit/FunctionDatabase-tests.cpp $
* $Id: FunctionDatabase-tests.cpp 105560 2020-01-13 10:24:44Z kimh19 $
*
* Project	: CODAC Core System
*
* Description	: Unit test code
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2020 ITER Organization,
*				  CS 90 046
*				  13067 St. Paul-lez-Durance Cedex
*				  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <gtest/gtest.h> // Google test framework

#include <common/BasicTypes.h> // Misc. type definition
#include <common/SysTools.h> // Misc. helper functions

#include <common/log-api.h> // Syslog wrapper routines

#include <common/AnyValue.h>
#include <common/AnyValueHelper.h>

#include <pvxs/server.h>
#include <pvxs/sharedpv.h>

// Local header files

#include "PVXSValueHelper.h"

#include "PVAccessMonitorImpl.h"

// Constants

// Type definition

namespace pvxs { 
namespace test {

class Server : public pvxs::server::Server
{
  private:

    pvxs::server::SharedPV pv;

  public:

    Server (const ccs::types::char8 * const name, const pvxs::Value& value) : pvxs::server::Server(pvxs::server::Config::from_env().build()) 
    {
      pv = pvxs::server::SharedPV(pvxs::server::SharedPV::buildReadonly());
      this->addPV(name, pv);
      this->start();

      pv.open(value);
    };

  bool Post (pvxs::Value& value) { pv.post(std::move(value)); return true; };

};

class PVAccessMonitor : public ccs::base::PVMonitor
{

  public:

    ccs::types::AnyValue _value;
    ccs::types::uint32 _count = 0u;

    PVAccessMonitor (const ccs::types::char8 * const name) : ccs::base::PVMonitor(name) {};
    virtual ~PVAccessMonitor (void) {};
    virtual void HandleMonitor (const ccs::types::AnyValue& value) {

      log_info("PVAccessMonitor - Channel '%s' updated at '%lu'", this->GetChannel(), ccs::HelperTools::GetCurrentTime());

      bool status = static_cast<bool>(value.GetType());
      
      if (status)
	{
	  _value = value;
	  _count++;
	}
      
      return;
      
    };


};

} // namespace test
} // namespace pvxs

// Global variables

static ccs::log::Func_t __handler = ccs::log::SetStdout();
static ccs::types::uint64 __recv_time = 0ul;

// Function declaration

// Function definition

TEST(PVAccessMonitorImpl_Test, Constructor_default)
{
  bool ret = true;

  pvxs::Value value;

  if (ret)
    {
      value = pvxs::TypeDef(pvxs::TypeCode::Struct, "pvxs::test::MyStruct_t", {
	                      pvxs::members::Struct("header", "pvxs::test::MyHeader_t", {
	                        pvxs::members::UInt64("counter"),
	                        pvxs::members::UInt64("timestamp"),
	                      }),
	                      pvxs::members::Float64("value"),
	                      pvxs::members::UInt8A("padding"),
                            }).create();

      pvxs::shared_array<uint8_t> array (64, 0);
      value["padding"] = array.freeze();
    }

  // Create test server
  pvxs::test::Server srv ("PVXS::TEST::READONLY", value);

  // Create client
  ccs::base::PVAccessMonitorImpl client;

  if (ret)
    {
      ret = client.SetChannel("PVXS::TEST::READONLY");
    }

  if (ret)
    {
      ret = client.Initialise();
    }

  ccs::types::uint64 count = 0u;

  while (ret && (count < 10u))
    {
      ccs::types::uint64 curr_time = ccs::HelperTools::SleepFor(1000000000ul);

      value["header.counter"] = count;
      value["header.timestamp"] = curr_time;
      srv.Post(value);

      count++;

      // ToDo - Test monitor
      //      ccs::HelperTools::SleepFor(10000000ul);
      //ret = (__recv_time == curr_time);
    }

  ASSERT_EQ(true, ret);
}

TEST(PVAccessMonitor_Test, Constructor_default)
{
  bool ret = true;

  pvxs::Value value;

  if (ret)
    {
      value = pvxs::TypeDef(pvxs::TypeCode::Struct, "pvxs::test::MyStruct_t", {
	                      pvxs::members::Struct("header", "pvxs::test::MyHeader_t", {
	                        pvxs::members::UInt64("counter"),
	                        pvxs::members::UInt64("timestamp"),
	                      }),
	                      pvxs::members::Float64("value"),
	                      pvxs::members::UInt8A("padding"),
                            }).create();

      pvxs::shared_array<uint8_t> array (64, 0);
      value["padding"] = array.freeze();
    }

  // Create test server
  pvxs::test::Server srv ("PVXS::TEST::READONLY", value);

  // Create client
  pvxs::test::PVAccessMonitor client ("PVXS::TEST::READONLY");

  ccs::types::uint64 count = 0u;

  while (ret && (count < 10u))
    {
      ccs::types::uint64 curr_time = ccs::HelperTools::SleepFor(1000000000ul);

      value["header.counter"] = count;
      value["header.timestamp"] = curr_time;
      srv.Post(value);

      count++;

      ccs::HelperTools::SleepFor(10000000ul);
      log_info("TEST(PVAccessMonitor_Test, Constructor_default) - Instance received '%lu %lu'", client._count, ccs::HelperTools::GetAttributeValue<ccs::types::uint64>(&(client._value), "header.timestamp"));
      ret = ((client._count == count + 1u) && // Connection update + each post 
	     (ccs::HelperTools::GetAttributeValue<ccs::types::uint64>(&(client._value), "header.timestamp") == curr_time));
    }

  ASSERT_EQ(true, ret);
}

