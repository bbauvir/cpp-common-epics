/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-sup-common-cpp/branches/codac-core-6.2/src/test/c++/unit/FunctionDatabase-tests.cpp $
* $Id: FunctionDatabase-tests.cpp 105560 2020-01-13 10:24:44Z kimh19 $
*
* Project	: CODAC Core System
*
* Description	: Unit test code
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2020 ITER Organization,
*				  CS 90 046
*				  13067 St. Paul-lez-Durance Cedex
*				  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <gtest/gtest.h> // Google test framework

#include <common/BasicTypes.h> // Misc. type definition
#include <common/SysTools.h> // Misc. helper functions

#include <common/log-api.h> // Syslog wrapper routines

#include <common/AnyValue.h>
#include <common/AnyValueHelper.h>

#include <pvxs/data.h>
#include <pvxs/server.h>
#include <pvxs/sharedpv.h>

// Local header files

#include "PVXSValueHelper.h"

// Constants

// Type definition

// Global variables

static ccs::log::Func_t __handler = ccs::log::SetStdout();

// Function declaration

// Function definition

TEST(PVXS_Server_Test, Constructor_default)
{
  pvxs::server::Server srv (pvxs::server::Config::from_env().build());

  pvxs::server::SharedPV pv (pvxs::server::SharedPV::buildReadonly());

  bool ret = static_cast<bool>(pv);

  if (ret)
    {
      srv.addPV("PVXS::TEST::READONLY", pv)
	 .start();
    }

  pvxs::Value value;

  if (ret)
    {
      value = pvxs::TypeDef(pvxs::TypeCode::Struct, "pvxs::test::MyStruct_t", {
	                      pvxs::members::Struct("header", "pvxs::test::MyHeader_t", {
	                        pvxs::members::UInt64("counter"),
	                        pvxs::members::UInt64("timestamp"),
	                      }),
	                      pvxs::members::Float64("value"),
	                      pvxs::members::UInt8A("padding"),
                            }).create();

      pvxs::shared_array<uint8_t> array (64, 0);
      value["padding"] = array.freeze();

      pv.open(value);
      ret = pv.isOpen();
    }

  ccs::types::uint64 count = 0ul;

  while (ret && (count < 10ul))
    {
      pv.fetch(value);
      value["header.counter"] = count;
      value["header.timestamp"] = ccs::HelperTools::SleepFor(1000000000ul);
      std::cout << value << std::endl;
      pv.post(std::move(value));

      count++;
    }

  ASSERT_EQ(true, ret);
}

