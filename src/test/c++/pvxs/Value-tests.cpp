/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-sup-common-cpp/branches/codac-core-6.2/src/test/c++/unit/FunctionDatabase-tests.cpp $
* $Id: FunctionDatabase-tests.cpp 105560 2020-01-13 10:24:44Z kimh19 $
*
* Project	: CODAC Core System
*
* Description	: Unit test code
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2020 ITER Organization,
*				  CS 90 046
*				  13067 St. Paul-lez-Durance Cedex
*				  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <gtest/gtest.h> // Google test framework

#include <common/BasicTypes.h> // Misc. type definition
#include <common/SysTools.h> // Misc. helper functions

#include <common/log-api.h> // Syslog wrapper routines

#include <common/AnyValue.h>
#include <common/AnyValueHelper.h>

#include <pvxs/data.h>

// Local header files

#include "PVXSValueHelper.h"

// Constants

// Type definition

// Global variables

static ccs::log::Func_t __handler = ccs::log::SetStdout();

// Function declaration

// Function definition

TEST(PVXS_Value_Test, Constructor_float64)
{
  pvxs::Value value = pvxs::TypeDef(pvxs::TypeCode::Struct, {
      pvxs::members::Float64("value"),
    }).create();

  value["value"] = 0.1;

  bool ret = (0.1 == value["value"].as<::ccs::types::float64>());

  ASSERT_EQ(true, ret);
}

TEST(PVXS_Value_Test, Constructor_arrays)
{
  pvxs::Value value = pvxs::TypeDef(pvxs::TypeCode::Struct, {
      pvxs::members::UInt64("time"),
      pvxs::members::StructA("array", {
          pvxs::members::Float64("index"),
        }),
    }).create();

  ccs::types::uint64 time = ccs::HelperTools::GetCurrentTime();

  value["time"] = time;

  bool ret = (time == value["time"].as<::ccs::types::uint64>());

  pvxs::shared_array<pvxs::Value> array (2);
  array[0] = value["array"].allocMember();
  array[1] = value["array"].allocMember();
  array[0]["index"] = 0.0;
  array[1]["index"] = 1.0;

  value["array"] = array.freeze();

  std::cout << value << std::endl;

  if (ret)
    {
      value["array[1].index"] = 0.1;
      ret = (0.1 == value["array[1].index"].as<::ccs::types::float64>());
    }

  ASSERT_EQ(true, ret);
}

TEST(PVXS_Value_Test, Constructor_FromAnyValue)
{
  ccs::types::AnyValue value ("{\"type\":\"pvxs::test::MyType_t\",\"attributes\":[{\"header\":{\"type\":\"pvxs::test::MyHeader_t\",\"attributes\":[{\"counter\":{\"type\":\"uint64\"}},{\"timestamp\":{\"type\":\"uint64\"}}]}},{\"value\":{\"type\":\"pvxs::test::MyPayload_t\",\"attributes\":[{\"data\":{\"type\":\"float64\"}},{\"error\":{\"type\":\"float64\"}}]}},{\"padding\":{\"type\":\"pvxs::test::MyPadding_t\",\"multiplicity\":64,\"element\":{\"type\":\"uint8\"}}},{\"array\":{\"type\":\"pvxs::test::MyStructArray_t\",\"multiplicity\":8,\"element\":{\"type\":\"pvxs::test::MyStruct_t\",\"attributes\":[{\"one\":{\"type\":\"uint8\"}},{\"two\":{\"type\":\"uint8\"}}]}}}]}}]}");
  //ccs::types::AnyValue value ("{\"type\":\"pvxs::test::MyType_t\",\"attributes\":[{\"header\":{\"type\":\"pvxs::test::MyHeader_t\",\"attributes\":[{\"counter\":{\"type\":\"uint64\"}},{\"timestamp\":{\"type\":\"uint64\"}}]}},{\"value\":{\"type\":\"pvxs::test::MyPayload_t\",\"attributes\":[{\"data\":{\"type\":\"float64\"}},{\"error\":{\"type\":\"float64\"}}]}},{\"padding\":{\"type\":\"pvxs::test::MyPadding_t\",\"multiplicity\":64,\"element\":{\"type\":\"uint8\"}}}]}}]}");

  ccs::base::SharedReference<const ccs::types::CompoundType> type = value.GetType();

  bool ret = static_cast<bool>(type);

  if (ret)
    {
      ccs::types::char8 buffer [1024];
      ccs::HelperTools::Serialise(type, buffer, 1024u);
      log_info("TEST(PVXS_Value_Test, Constructor_FromAnyValue) - Type '%s' ..", buffer);
    }

  pvxs::Value _value;

  if (ret)
    {
      ret = ccs::HelperTools::SerialiseAnyValueToPVXSValue(value, _value);
    }

  if (ret)
    { 
      std::cout << _value << std::endl;
    }

  if (ret)
    {
      ccs::types::uint32 count = 0u;

      for (auto fld : _value.iall()) 
	{
	  log_info("TEST(PVXS_Value_Test, Constructor_FromAnyValue) - Descendent '%s (%u %s)' at index '%u'", _value.nameOf(fld).c_str(), fld.type(), fld.type().name(), count);
	  count++;
	}

      ret = (8u == count);
    }

  if (ret)
    {
      ccs::types::uint32 count = 0u;

      for (auto fld : _value.ichildren()) 
	{
	  log_info("TEST(PVXS_Value_Test, Constructor_FromAnyValue) - Child '%s (%u %s)' at index '%u'", _value.nameOf(fld).c_str(), fld.type(), fld.type().name(), count);
	  count++;
	}

      ret = (4u == count);
    }

  if (ret)
    {
      ret = ccs::HelperTools::SetAttributeValue<ccs::types::float64>(&value, "value.data", 0.1);
    }

  if (ret)
    {
      ret = ccs::HelperTools::AssignAnyValueToPVXSValue(value, _value);
    }

  if (ret)
    {
      ret = ccs::HelperTools::SetAttributeValue<ccs::types::uint8>(&value, "padding[10]", 10);
    }

  if (ret)
    {
      ret = ccs::HelperTools::AssignAnyValueToPVXSValue(value, _value);
    }

  if (ret)
    {
      ret = (ccs::HelperTools::SetAttributeValue<ccs::types::uint8>(&value, "array[1].one", 1) &&
	     ccs::HelperTools::SetAttributeValue<ccs::types::uint8>(&value, "array[2].two", 2));
    }

  if (ret)
    {
      ret = ccs::HelperTools::AssignAnyValueToPVXSValue(value, _value);
    }

  if (ret)
    { 
      std::cout << _value << std::endl;
    }

  ASSERT_EQ(true, ret);
}

TEST(PVXS_Value_Test, Constructor_ToAnyValue)
{
  pvxs::Value value;

  bool ret = !static_cast<bool>(value);

  try 
    {
      value = pvxs::TypeDef(pvxs::TypeCode::Struct, "pvxs::test::MyStruct_t", {
	  pvxs::members::Struct("header", "pvxs::test::MyHeader_t", {
	      pvxs::members::UInt64("counter"),
	pvxs::members::UInt64("timestamp"),
		}),
	    pvxs::members::Float64("value"),
	    pvxs::members::UInt8A("padding"),
	  //pvxs::members::StructA("array", {
	    pvxs::members::StructA("array", "pvxs::test::MyStructArray_t", {
		pvxs::members::UInt8("one"),
	  pvxs::members::UInt8("two"),
		  }),
	    }).create();
    }
  catch (const std::exception& e)
    {
      log_error("TEST(PVXS_Value_Test, Constructor_ToAnyValue) - Exception '%s' thrown", e.what());
      ret = false;
    }

  { // Initialise scalar array
    pvxs::shared_array<uint8_t> array (64, 0);
    array[10] = 10;
    value["padding"] = array.freeze();
  }

  { // Initialise scalar array
    pvxs::shared_array<pvxs::Value> array (8);
    array[0] = value["array"].allocMember();
    array[1] = value["array"].allocMember();
    array[2] = value["array"].allocMember();
    array[3] = value["array"].allocMember();
    array[4] = value["array"].allocMember();
    array[5] = value["array"].allocMember();
    array[6] = value["array"].allocMember();
    array[7] = value["array"].allocMember();

    value["array"] = array.freeze();
  }

  value["header.timestamp"] = ::ccs::HelperTools::GetCurrentTime();
  value["value"] = 0.1;
  value["array[1].one"] = 1;
  value["array[2].two"] = 2;
 
  ::ccs::types::AnyValue _value;

  if (ret)
    {
      ret = ccs::HelperTools::SerialiseAnyValueFromPVXSValue(_value, value);
    }

  if (ret)
    {
      ccs::types::char8 buffer [1024];
      _value.SerialiseType(buffer, 1024u);
      log_info("TEST(PVXS_Value_Test, Constructor_ToAnyValue) - Type '%s' ..", buffer);
    }

  if (ret)
    {
      ccs::types::char8 buffer [1024];
      _value.SerialiseInstance(buffer, 1024u);
      log_info("TEST(PVXS_Value_Test, Constructor_ToAnyValue) - Instance '%s' ..", buffer);
    }

  // ToDo - Test instances

  ASSERT_EQ(true, ret);
}

TEST(PVXS_Value_Test, ParsingStructA_NoTypeName)
{
  pvxs::Value value;

  bool ret = !static_cast<bool>(value);

  try 
    {
      value = pvxs::TypeDef(
        pvxs::TypeCode::Struct, "pvxs::test::MyStruct_t", {
	  pvxs::members::Struct("header", "pvxs::test::MyHeader_t", {
	      pvxs::members::UInt64("counter"),
              pvxs::members::UInt64("timestamp"),
	    }),
	  pvxs::members::Float64("value"),
	  pvxs::members::UInt8A("padding"),
	  pvxs::members::StructA("array", {
	//pvxs::members::StructA("array", "pvxs::test::MyStructArray_t", {
	    pvxs::members::UInt8("one"),
	    pvxs::members::UInt8("two"),
	  }),
	}).create();
    }
  catch (const std::exception& e)
    {
      log_error("TEST(PVXS_Value_Test, ParsingStructA_NoTypeName) - Exception '%s' thrown", e.what());
      ret = false;
    }

  if (ret)
    { // Initialise scalar array
      pvxs::shared_array<uint8_t> array (64, 0);
      value["padding"] = array.freeze();
    }

  if (ret)
    { // Initialise struct array
      pvxs::shared_array<pvxs::Value> array (8);
      array[0] = value["array"].allocMember();
      array[1] = value["array"].allocMember();
      array[2] = value["array"].allocMember();
      array[3] = value["array"].allocMember();
      array[4] = value["array"].allocMember();
      array[5] = value["array"].allocMember();
      array[6] = value["array"].allocMember();
      array[7] = value["array"].allocMember();
      
      value["array"] = array.freeze();
    }
  
  if (ret)
    { // Parse type definition .. 
      pvxs::Value _value = value["array"].allocMember();
      log_info("TEST(PVXS_Value_Test, ParsingStructA_NoTypeName) - Element has type '%s' ..", _value.id().c_str());
      for (auto field : _value.ichildren())
	{
          log_info("TEST(PVXS_Value_Test, ParsingStructA_NoTypeName) - .. with attribute '%s %s'", field.type().name(), _value.nameOf(field).c_str());
	}
    }

  ASSERT_EQ(true, ret);
}

TEST(PVXS_Value_Test, ParsingStructA_WithTypeName)
{
  pvxs::Value value;

  bool ret = !static_cast<bool>(value);

  try 
    {
      value = pvxs::TypeDef(
        pvxs::TypeCode::Struct, "pvxs::test::MyStruct_t", {
	  pvxs::members::Struct("header", "pvxs::test::MyHeader_t", {
	      pvxs::members::UInt64("counter"),
              pvxs::members::UInt64("timestamp"),
	    }),
	  pvxs::members::Float64("value"),
	  pvxs::members::UInt8A("padding"),
	//pvxs::members::StructA("array", {
	  pvxs::members::StructA("array", "pvxs::test::MyStructArray_t", {
	    pvxs::members::UInt8("one"),
	    pvxs::members::UInt8("two"),
	  }),
	}).create();
    }
  catch (const std::exception& e)
    {
      log_error("TEST(PVXS_Value_Test, ParsingStructA_WithTypeName) - Exception '%s' thrown", e.what());
      ret = false;
    }

  if (ret)
    { // Initialise scalar array
      pvxs::shared_array<uint8_t> array (64, 0);
      value["padding"] = array.freeze();
    }

  if (ret)
    { // Initialise struct array
      pvxs::shared_array<pvxs::Value> array (8);
      array[0] = value["array"].allocMember();
      array[1] = value["array"].allocMember();
      array[2] = value["array"].allocMember();
      array[3] = value["array"].allocMember();
      array[4] = value["array"].allocMember();
      array[5] = value["array"].allocMember();
      array[6] = value["array"].allocMember();
      array[7] = value["array"].allocMember();
      
      value["array"] = array.freeze();
    }
  
  if (ret)
    { // Parse type definition .. 
      pvxs::Value _value = value["array"].allocMember();
      log_info("TEST(PVXS_Value_Test, ParsingStructA_WithTypeName) - Element has type '%s' ..", _value.id().c_str());
      for (auto field : _value.ichildren())
	{
          log_info("TEST(PVXS_Value_Test, ParsingStructA_WithTypeName) - .. with attribute '%s %s'", field.type().name(), _value.nameOf(field).c_str());
	}
    }

  ASSERT_EQ(true, ret);
}

