/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-sup-common-cpp/branches/codac-core-6.2/src/test/c++/unit/FunctionDatabase-tests.cpp $
* $Id: FunctionDatabase-tests.cpp 105560 2020-01-13 10:24:44Z kimh19 $
*
* Project	: CODAC Core System
*
* Description	: Unit test code
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2020 ITER Organization,
*				  CS 90 046
*				  13067 St. Paul-lez-Durance Cedex
*				  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <gtest/gtest.h> // Google test framework

#include <common/BasicTypes.h> // Misc. type definition
#include <common/SysTools.h> // Misc. helper functions

#include <common/log-api.h> // Syslog wrapper routines

#include <common/AnyValue.h>
#include <common/AnyValueHelper.h>

#include <pvxs/data.h>
#include <pvxs/client.h>

#include <pvxs/server.h>
#include <pvxs/sharedpv.h>

// Local header files

#include "PVXSValueHelper.h"

// Constants

// Type definition

namespace pvxs { 
namespace test {

class Server : public pvxs::server::Server
{
  private:

    pvxs::server::SharedPV pv;

  public:

    Server (const ccs::types::char8 * const name, const pvxs::Value& value) : pvxs::server::Server(pvxs::server::Config::from_env().build()) 
    {
      pv = pvxs::server::SharedPV(pvxs::server::SharedPV::buildReadonly());
      this->addPV(name, pv);
      this->start();

      pv.open(value);
    };

  bool Post (pvxs::Value& value) 
  { 
    //pv.post(std::move(value)); 
    pv.post(value); 
    return true; 
  };

};

} // namespace test
} // namespace pvxs

// Global variables

static ccs::log::Func_t __handler = ccs::log::SetStdout();
static ccs::types::uint64 __recv_time = 0ul;

// Function declaration

// Function definition

TEST(PVXS_Monitor_Test, Constructor_default)
{
  bool ret = true;

  pvxs::Value value;

  if (ret)
    {
      value = pvxs::TypeDef(pvxs::TypeCode::Struct, "pvxs::test::MyStruct_t", {
	                      pvxs::members::Struct("header", "pvxs::test::MyHeader_t", {
	                        pvxs::members::UInt64("counter"),
	                        pvxs::members::UInt64("timestamp"),
	                      }),
	                      pvxs::members::Float64("value"),
	                      pvxs::members::UInt8A("padding"),
                            }).create();

      pvxs::shared_array<uint8_t> array (64, 0);
      value["padding"] = array.freeze();
    }

  // Create test server
  pvxs::test::Server srv ("PVXS::TEST::READONLY", value);

  // Create client
  pvxs::client::Context cli (pvxs::client::Config::from_env().build());
  std::shared_ptr<pvxs::client::Subscription> sub;

  log_info("TEST(PVXS_Monitor_Test, Constructor_default) - Create subscription ..");

  // Create subscription.. with lambda
  sub = cli.monitor("PVXS::TEST::READONLY")
    .maskConnected(false)
    .maskDisconnected(false)
    .event([](pvxs::client::Subscription& sub) {
      log_notice("pvxs::client::Subscription callback");
      while (true) // Ensure event queue is empty after invocation of the callback
	{
      try
	{
	  pvxs::Value _value = sub.pop();
	  ::ccs::types::AnyValue value; // 

	  bool ret = static_cast<bool>(_value);

	  if (ret)
	    {
	      ret = ccs::HelperTools::SerialiseAnyValueFromPVXSValue(value, _value);
	    }
	  else
	    {
	      break;
	    }

	  if (ret)
	    {
	      ccs::types::char8 buffer [1024];
	      value.SerialiseType(buffer, 1024u);
	      log_info("TEST(PVXS_Monitor_Test, Constructor_default) - Type '%s' ..", buffer);
	    }
	  
	  if (ret)
	    {
	      ccs::types::char8 buffer [1024];
	      value.SerialiseInstance(buffer, 1024u);
	      log_info("TEST(PVXS_Monitor_Test, Constructor_default) - Instance '%s' ..", buffer);
	    }

	  if (ret)
	    {
	      ret = ccs::HelperTools::GetAttributeValue(&value, "header.timestamp", __recv_time);
	    }
	}
      catch (const pvxs::client::Connected& e)
	{
	  log_notice(".. connect exception (event) caught with peer '%s'", e.peerName.c_str());
	}
      catch (const pvxs::client::Disconnect& e)
	{
	  log_warning(".. disconnect exception caught");
	}
      catch (const pvxs::client::Timeout& e)
	{
	  log_warning(".. timeout exception caught");
	}
      catch (const std::exception& e)
	{
	  log_error(".. '%s' exception caught", e.what());
	}
      catch (...)
	{
	  log_error(".. unknown exception caught");
	}
	}
    })
    .exec();

  ccs::types::uint64 count = 0u;

  while (ret && (count < 10u))
    {
      ccs::types::uint64 curr_time = ccs::HelperTools::SleepFor(1000000000ul);

      value["header.counter"] = count;
      value["header.timestamp"] = curr_time;
      srv.Post(value);

      count++;

      // ToDo - Test monitor
      ccs::HelperTools::SleepFor(1000000000ul);
      ret = (__recv_time == curr_time);
    }

  ASSERT_EQ(true, ret);
}

