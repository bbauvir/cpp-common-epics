/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/test/c++/unit/PVAccessServer-tests.cpp $
* $Id: PVAccessServer-tests.cpp 111303 2020-07-08 05:58:03Z bauvirb $
*
* Project	: CODAC Core System
*
* Description	: Unit test code
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2019 ITER Organization,
*				  CS 90 046
*				  13067 St. Paul-lez-Durance Cedex
*				  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <gtest/gtest.h> // Google test framework

#include <common/BasicTypes.h> // Misc. type definition
#include <common/SysTools.h> // Misc. helper functions
#include <common/TimeTools.h> // Misc. helper functions

#include <common/log-api.h> // Syslog wrapper routines

// Local header files

#include "PVAccessTypes.h"
#include "PVAccessServer.h"

// Constants

// Type definition

class PVAccessServerTest
{

  private:

    ccs::types::uint32 __value = 0u;
    ccs::types::uint32 __count = 0u;
 
  public:

    PVAccessServerTest (void) {};
    virtual ~PVAccessServerTest (void) {};

    ccs::types::uint32 GetValue (void) { return __value; };
    ccs::types::uint32 GetCount (void) { return __count; };

    // Callback for PV update
    void Callback (const ccs::types::AnyValue& value) {

      char buffer [1024] = STRING_UNDEFINED; value.SerialiseInstance(buffer, 1024u);
      log_info("PVAccessServerTest::Callback - Value '%s'", buffer);

      __count += 1u;
      __value = ccs::HelperTools::GetAttributeValue<ccs::types::uint32>(&value, "scalars.uint32");

    };

    bool RegisterCallback (void) {

      return ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->SetCallback("PVA-SRV-TESTS:STRUCT", 
												 std::bind(&PVAccessServerTest::Callback, this, std::placeholders::_1));

    };

};

// Function declaration

// Global variables

static ccs::log::Func_t __handler = ccs::log::SetStdout();
static ccs::base::PVAccessServer* __pva_srv = static_cast<ccs::base::PVAccessServer*>(NULL);

// Function definition

static inline bool Initialise (void)
{

  __pva_srv = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>();

  bool status = (static_cast<ccs::base::PVAccessServer*>(NULL) != __pva_srv);

  return status;

}

static inline bool Terminate (void)
{

  bool status = (static_cast<ccs::base::PVAccessServer*>(NULL) != __pva_srv);

  if (status)
    {
      ccs::base::PVAccessInterface::Terminate<ccs::base::PVAccessServer>();
      __pva_srv = static_cast<ccs::base::PVAccessServer*>(NULL);
    }

  return status;

}

TEST(PVAccessServer, Constructor)
{
  bool ret = Initialise();

  if (ret)
    {
      ret = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->Launch();
    }

  if (ret)
    {
      ret = Terminate();
    }

  ASSERT_EQ(true, ret);
}

TEST(PVAccessServer, Default) // Static initialisation
{
  bool ret = Initialise();

  PVAccessServerTest* test = static_cast<PVAccessServerTest*>(NULL);

  if (ret)
    {
      test = new (std::nothrow) PVAccessServerTest ();
      ret = (static_cast<PVAccessServerTest*>(NULL) != test);
    }

  if (ret)
    {
      ret = (0u == test->GetValue());
    }

  if (ret)
    {
      log_info("TEST(PVAccessServer, Default) - Test RegisterCallback failure");
      ret = (false == test->RegisterCallback()); // Expect failure
    }

  if (ret)
    {
      log_info("TEST(PVAccessServer, Default) - Test AddVariable with scalar (JSON type definition)");
      ret = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->AddVariable("PVA-SRV-TESTS:BOOL", ccs::types::AnyputVariable, "{\"type\":\"bool\"}");
    }

  if (ret)
    {
      log_info("TEST(PVAccessServer, Default) - Test AddVariable with scalar");
      ret = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->AddVariable("PVA-SRV-TESTS:INTEGER", ccs::types::AnyputVariable, ccs::types::UnsignedInteger32);
    }

  if (ret)
    {
      log_info("TEST(PVAccessServer, Default) - Test AddVariable with structure");
      ret = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->AddVariable("PVA-SRV-TESTS:STRUCT", ccs::types::AnyputVariable, ccs::base::PVAccessTypes::Collection_int);
    }

  if (ret)
    {
      ret = test->RegisterCallback();
    }

  if (ret)
    {
      ret = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->SetPeriod(10000000ul);
    }

  if (ret)
    {
      ret = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->Launch();
    }

  ccs::HelperTools::SleepFor(1000000000ul);

  if (ret)
    { // Compose system command
      std::string command = std::string("/usr/bin/pvget -v PVA-SRV-TESTS:BOOL PVA-SRV-TESTS:INTEGER PVA-SRV-TESTS:STRUCT > /tmp/pvget.out");
      ret = (std::system(command.c_str()) == 0);
      std::stringstream sstr; sstr << std::ifstream("/tmp/pvget.out").rdbuf();
      std::string result = sstr.str();
      log_info("TEST(PVAccessServer, Default) - Check variables exist ..");
      std::cout << result << std::endl;
     }

  // Write to variable
  if (ret)
    {
      ret = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->IsValid("PVA-SRV-TESTS:INTEGER");
    }

  if (ret)
    {
      ccs::types::uint32 value = 1234u;
      ret = ccs::HelperTools::SetAttributeValue(ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->GetVariable("PVA-SRV-TESTS:INTEGER"),
						"value",
						value);
    }

  if (ret)
    {
      ret = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->UpdateVariable("PVA-SRV-TESTS:INTEGER");
      ccs::HelperTools::SleepFor(100000000ul);
    }

  if (ret)
    {
      ccs::types::uint32 value = 0u;
      ret = (ccs::HelperTools::GetAttributeValue(ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->GetVariable("PVA-SRV-TESTS:INTEGER"),
						"value",
						 value) && (1234u == value));
    }

  if (ret)
    { // Compose system command
      std::string command = std::string("/usr/bin/pvput PVA-SRV-TESTS:INTEGER \"value\"=4321 > /tmp/pvget.out");
      ret = (std::system(command.c_str()) == 0);
      ccs::HelperTools::SleepFor(100000000ul);
     }

  if (ret)
    {
      ccs::types::uint32 value = 0u;
      ret = (ccs::HelperTools::GetAttributeValue(ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->GetVariable("PVA-SRV-TESTS:INTEGER"),
						"value",
						 value) && (4321u == value));
      log_info("TEST(PVAccessServer, Default) - Test value '%u' .. '%s'", value, (ret ? "success" : "failure"));
    }

  // Write to variable
  if (ret)
    {
      ret = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->IsValid("PVA-SRV-TESTS:STRUCT");
    }

  if (ret)
    {
      ccs::types::uint32 value = 1234u;
      ret = (ccs::HelperTools::SetAttributeValue(ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->GetVariable("PVA-SRV-TESTS:STRUCT"),
						 "scalars.uint32",
						 value) &&
	     ccs::HelperTools::SetAttributeValue(ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->GetVariable("PVA-SRV-TESTS:STRUCT"),
						 "arrays.uint32[3]",
						 value));
    }

  if (ret)
    {
      ret = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->UpdateVariable("PVA-SRV-TESTS:STRUCT");
      ccs::HelperTools::SleepFor(100000000ul);
    }

  if (ret)
    {
      ccs::types::uint32 value = 0u;
      ret = (ccs::HelperTools::GetAttributeValue(ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->GetVariable("PVA-SRV-TESTS:STRUCT"),
						"scalars.uint32",
						 value) && (1234u == value));
      log_info("TEST(PVAccessServer, Default) - Test value '%u' .. '%s'", value, (ret ? "success" : "failure"));
    }

  if (ret)
    { // Compose system command
      std::string command = std::string("/usr/bin/pvput PVA-SRV-TESTS:STRUCT \"scalars.uint32\"=4321 > /tmp/pvget.out");
      ret = (std::system(command.c_str()) == 0);
      ccs::HelperTools::SleepFor(100000000ul);
     }

  if (ret)
    {
      ccs::types::uint32 value = 0u;
      ret = (ccs::HelperTools::GetAttributeValue(ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->GetVariable("PVA-SRV-TESTS:STRUCT"),
						"scalars.uint32",
						 value) && (4321u == value));
    }

  if (ret)
    {
      ret = ((4321u == test->GetValue()) && (0u < test->GetCount()));
      log_info("TEST(PVAccessServer, Default) - Test value '%u' and count '%u' .. '%s'", test->GetValue(), test->GetCount(), (ret ? "success" : "failure"));
    }

  if (ret)
    {
      ret = Terminate();
    }

  if (ret)
    {
      delete test;
    }

  ASSERT_EQ(true, ret);
}

