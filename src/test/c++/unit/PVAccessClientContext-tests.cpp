/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/test/c++/unit/PVAccessClientContext-tests.cpp $
* $Id: PVAccessClientContext-tests.cpp 111303 2020-07-08 05:58:03Z bauvirb $
*
* Project	: CODAC Core System
*
* Description	: Unit test code
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2019 ITER Organization,
*				  CS 90 046
*				  13067 St. Paul-lez-Durance Cedex
*				  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <gtest/gtest.h> // Google test framework

#include <common/BasicTypes.h> // Misc. type definition

#include <common/log-api.h> // Syslog wrapper routines

// Local header files

#include "PVAccessClientContext.h"

// Constants

// Type definition

// Function declaration

// Global variables

static ccs::log::Func_t __handler = ccs::log::SetStdout();

// Function definition

TEST(PVAccessClientContext, GetInstance)
{
  bool ret = static_cast<bool>(ccs::base::PVAccessClientContext::GetInstance());

  if (ret)
    {
      ccs::base::PVAccessClientContext::Terminate();
    }

  ASSERT_EQ(true, ret);
}

TEST(PVAccessClientContext, SetInstance)
{
  ::ccs::base::SharedReference<pvxs::client::Context> context (new (std::nothrow) pvxs::client::Context (pvxs::client::Config::from_env().build()));

  bool ret = static_cast<bool>(context);

  if (ret)
    {
      ret = ccs::base::PVAccessClientContext::SetInstance(context);
    }

  if (ret)
    {
      ret = (2u == context.GetCount());
    }

  if (ret)
    {
      ::ccs::base::SharedReference<pvxs::client::Context> _context = ccs::base::PVAccessClientContext::GetInstance();
      ret = (3u == context.GetCount());
    }

  if (ret)
    {
      ret = (2u == context.GetCount());
    }

  if (ret)
    {
      context.Discard();
      ret = ccs::base::PVAccessClientContext::IsInitialised(); // Singleton instance still there but this instance has discarded as well the reference to the counter
    }

  if (ret)
    {
      ret = ccs::base::PVAccessClientContext::Terminate();
    }

  if (ret)
    {
      ret = !ccs::base::PVAccessClientContext::IsInitialised();
    }

  ASSERT_EQ(true, ret);
}

TEST(PVAccessClientContext, SetInstance_Preexist)
{
  bool ret = static_cast<bool>(ccs::base::PVAccessClientContext::GetInstance());

  if (ret)
    {
      ret = ccs::base::PVAccessClientContext::IsInitialised();
    }

  ::ccs::base::SharedReference<pvxs::client::Context> context (new (std::nothrow) pvxs::client::Context (pvxs::client::Config::from_env().build()));

  if (ret)
    {
      ret = ccs::base::PVAccessClientContext::SetInstance(context);
    }

  if (ret)
    {
      ret = (2u == context.GetCount());
    }

  if (ret)
    {
      context.Discard();
      ret = ccs::base::PVAccessClientContext::IsInitialised(); // Singleton instance still there but this instance has discarded as well the reference to the counter
    }

  if (ret)
    {
      ret = ccs::base::PVAccessClientContext::Terminate();
    }

  if (ret)
    {
      ret = !ccs::base::PVAccessClientContext::IsInitialised();
    }

  ASSERT_EQ(true, ret);
}

