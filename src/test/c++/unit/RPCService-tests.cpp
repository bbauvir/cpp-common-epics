/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/test/c++/unit/RPCService-tests.cpp $
* $Id: RPCService-tests.cpp 114049 2020-10-20 06:28:48Z bauvirb $
*
* Project	: CODAC Core System
*
* Description	: Unit test code
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2019 ITER Organization,
*				  CS 90 046
*				  13067 St. Paul-lez-Durance Cedex
*				  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <gtest/gtest.h> // Google test framework

#include <common/BasicTypes.h> // Misc. type definition
#include <common/SysTools.h> // Misc. helper functions
#include <common/TimeTools.h> // Misc. helper functions
#include <common/SquareRoot.h> // Misc. helper functions

#include <common/log-api.h> // Syslog wrapper routines

#include <common/AnyTypeDatabase.h>

#include <common/CyclicRedundancyCheck.h>
#include <common/Hash.h>

#include <common/RPCClientImpl.h> // Base class for implementing transport plug-in
#include <common/RPCClientImplFactory.h> // and factory

#include <common/RPCServerImpl.h> // Base class for implementing transport plug-in
#include <common/RPCServerImplFactory.h> // .. and factory

#include <common/RPCClient.h>
#include <common/RPCServer.h>

#include <common/RPCTypes.h>

// Local header files

// Constants

#define RPCService_Tests_Server_name "SomeService@SomeLocation"

// Type definition

class RPCClientImpl : public ccs::base::RPCClientImpl // Transport plug-in .. dummy
{

  public:

    RPCClientImpl (void) : ccs::base::RPCClientImpl() {};
    virtual ~RPCClientImpl (void) {};

    virtual bool IsConnected (void) const { return true; };
    virtual bool Launch (void) { return true; };
    virtual bool Terminate (void) { return true; };

    virtual ccs::types::AnyValue SendRequest (const ccs::types::AnyValue& request) const;

};

class RPCServerImpl : public ccs::base::RPCServerImpl // Transport plug-in .. dummy
{

  public:

    RPCServerImpl (void) : ccs::base::RPCServerImpl() {};
    virtual ~RPCServerImpl (void) {};

    virtual bool Launch (void);
    virtual bool Terminate (void) { return true; };

};

class RPCHandler : public ccs::base::RPCServer 
{
  
  private:

    ccs::base::SharedReference<const ccs::types::AnyType> __enable_type;
    ccs::base::SharedReference<const ccs::types::AnyType> __limits_type;
    ccs::base::SharedReference<const ccs::types::AnyType> __config_type;
 
  public:
  
    struct __attribute__((packed)) Config_t {
      ccs::types::boolean enabled = true;
      ccs::types::float64 setpoint = 0.0;
    } __config_data;
  
    struct __attribute__((packed)) Limits_t {
      ccs::types::float64 max = +10.0;
      ccs::types::float64 min = -10.0;
    } __limits_data;
  
    RPCHandler (void) : ccs::base::RPCServer(RPCService_Tests_Server_name) {
      // Introspectable type definition
      __enable_type = ccs::base::SharedReference<const ccs::types::AnyType>(((new (std::nothrow) ccs::types::CompoundType ("user::Enable_t"))
								  ->AddAttribute("enabled", "bool")
								  ->AddAttribute("setpoint", "float64")));
      __limits_type = ccs::base::SharedReference<const ccs::types::AnyType>(((new (std::nothrow) ccs::types::CompoundType ("user::Limits_t"))
								  ->AddAttribute("maximum", "float64")
								  ->AddAttribute("minimum", "float64")));
      __config_type = ccs::base::SharedReference<const ccs::types::AnyType>(new (std::nothrow) ccs::types::ArrayType ("user::Config_t", 
													   ((new (std::nothrow) ccs::types::CompoundType)
													    ->AddAttribute("enable", __enable_type)
													    ->AddAttribute("limits", __limits_type)),
													    24u));
    };

    virtual ~RPCHandler (void) {};
  
    // RPC handler method
    virtual ccs::types::AnyValue HandleRequest (const ccs::types::AnyValue& request);

};
  
// Function declaration

// Global variables

static ccs::log::Func_t __handler = ccs::log::SetStdout();
#if 0 // RPCService plug-in implementations are statically registered .. do not assume this is done at this time
static RPCHandler* __server = new (std::nothrow) RPCHandler ();
static ccs::base::RPCClient* __client = new (std::nothrow) ccs::base::RPCClient (RPCService_Tests_Server_name);
#else
static RPCHandler* __server = static_cast<RPCHandler*>(NULL);
static ccs::base::RPCClient* __client = static_cast<ccs::base::RPCClient*>(NULL);
#endif
static bool __client_impl = false;
static bool __server_impl = false;

// Function definition

static inline bool Initialise (void) // RPCService plug-in implementations are statically registered .. do not assume this is done at this time
{

 bool status = ((static_cast<RPCHandler*>(NULL) != __server) &&
		 (static_cast<ccs::base::RPCClient*>(NULL) != __client));

 if (!status)
   {
     __server = new (std::nothrow) RPCHandler ();
     __client = new (std::nothrow) ccs::base::RPCClient (RPCService_Tests_Server_name);
     
     status = ((static_cast<RPCHandler*>(NULL) != __server) &&
	       (static_cast<ccs::base::RPCClient*>(NULL) != __client));
   }
  
  return status;

}

ccs::base::RPCClientImpl* RPCClientImplConstructor (void) { return dynamic_cast<RPCClientImpl*>(new (std::nothrow) RPCClientImpl ()); }
ccs::base::RPCServerImpl* RPCServerImplConstructor (void) { return dynamic_cast<RPCServerImpl*>(new (std::nothrow) RPCServerImpl ()); }
 
ccs::types::AnyValue RPCClientImpl::SendRequest (const ccs::types::AnyValue& request) const
{

  __client_impl = true;

  bool status = (ccs::HelperTools::HasAttribute(&request, "qualifier") &&
		 (ccs::types::String == ccs::HelperTools::GetAttributeType(&request, "qualifier")));

  std::string qualifier;

  if (status)
    {
      qualifier = std::string(reinterpret_cast<const char*>(ccs::HelperTools::GetAttributeReference(&request, "qualifier")));
    }
  else
    {
      throw std::runtime_error("Unexpected request");
    }

  ccs::types::AnyValue reply;

  if (qualifier == "echo")
    {
      reply = request;
    }

  if (qualifier == "throw")
    {
      throw 0u;
    }

  return reply;

}

bool RPCServerImpl::Launch (void) { __server_impl = true; return true; }

ccs::types::AnyValue RPCHandler::HandleRequest (const ccs::types::AnyValue& request)
{

  char buffer [1024] = STRING_UNDEFINED; request.SerialiseInstance(buffer, 1024u);

  log_info("RPCHandler::HandleRequest - Entering method ..");
  log_info("RPCHandler::HandleRequest - .. with '%s'", buffer);

  bool status = (ccs::HelperTools::HasAttribute(&request, "qualifier") &&
		 (ccs::types::String == ccs::HelperTools::GetAttributeType(&request, "qualifier")));

  std::string qualifier;

  if (status)
    {
      qualifier = std::string(reinterpret_cast<const char*>(ccs::HelperTools::GetAttributeReference(&request, "qualifier")));
    }
  else
    {
      log_error("RPCHandler::HandleRequest - Unexpected request .. no qualifier");
      throw std::runtime_error("Unexpected request");
    }

  ccs::types::AnyValue reply;

  if (qualifier == "echo")
    {
      reply = request;
    }

  if (qualifier == "throw")
    {
      if (ccs::HelperTools::HasAttribute(&request, "value") && 
	  (ccs::types::UnsignedInteger32 == ccs::HelperTools::GetAttributeType(&request, "value")))
	throw 0u;
      else
	throw std::runtime_error("Exception thrown in RPCHandler::HandleRequest");
    }

  if (qualifier == "undef")
    {
    }

  if (qualifier == "bool")
    {
      ccs::types::AnyValue value (ccs::types::Boolean); // PVAccessRPCClient::SendRequest expects a CompoundType
      reply = value;
    }

  // Copy the base request type ..
  ccs::types::CompoundType reply_type (*ccs::types::Reply_int); // Default RPC request type

  if (qualifier == "index")
    {
      reply_type.AddAttribute("[0]", "uint32");
      reply_type.AddAttribute("[1]", "uint32");

      ccs::types::AnyValue value (reply_type);
      reply = value;
    }

  if (qualifier == "config")
    {
      reply_type.AddAttribute("value", __config_type);

      ccs::types::AnyValue value (reply_type);
      reply = value;
    }

  return reply;

}

TEST(RPCService_Test, Constructor) // Static initialisation
{
  bool ret = Initialise();

  ASSERT_EQ(true, ret);
}

TEST(RPCService_Test,RPCClient_GetService)
{
  bool ret = Initialise();

  if (ret)
    {
      std::string service (__client->GetService());
      ret = (service == RPCService_Tests_Server_name);
    }

  ASSERT_EQ(true, ret);
}

TEST(RPCService_Test,RPCServer_GetService)
{
  bool ret = Initialise();

  if (ret)
    {
      std::string service (__server->GetService());
      ret = (service == RPCService_Tests_Server_name);
    }

  ASSERT_EQ(true, ret);
}

TEST(RPCService_Test, RPCClient_IsConnected)
{
  bool ret = Initialise();

  if (ret)
    {
      ccs::HelperTools::SleepFor(100000000ul);
      ret = __client->IsConnected();
    }

  ASSERT_EQ(true, ret);
}

TEST(RPCService_Test, RPCClient_SendRequest)
{
  bool ret = Initialise();

  if (ret)
    {
      ccs::HelperTools::SleepFor(100000000ul);
      ret = __client->IsConnected();
    }

  // Copy the base request type ..
  ccs::types::CompoundType request_type (*ccs::types::Request_int); // Default RPC request type
  request_type.AddAttribute("value", "uint32");

  if (ret)
    {
      ccs::types::AnyValue request (request_type);
      ccs::HelperTools::SetAttributeValue(&request, "qualifier", "echo");
      ccs::HelperTools::SetAttributeValue(&request, "value", ccs::HelperTools::Hash<ccs::types::uint32>("echo"));

      ccs::types::AnyValue reply = __client->SendRequest(request);

      ret = ((ccs::types::UnsignedInteger32 == ccs::HelperTools::GetAttributeType(&reply, "value")) &&
	     (ccs::HelperTools::Hash<ccs::types::uint32>("echo") == ccs::HelperTools::GetAttributeValue<ccs::types::uint32>(&reply, "value")));
    }

  if (ret)
    {
      ccs::types::AnyValue request (request_type);
      ccs::HelperTools::SetAttributeValue(&request, "qualifier", "config");

      ccs::types::AnyValue reply = __client->SendRequest(request);

      ret = ccs::HelperTools::Is<ccs::types::ArrayType>(ccs::HelperTools::GetAttributeType(&reply, "value"));

      char buffer [1024] = STRING_UNDEFINED; reply.SerialiseInstance(buffer, 1024u);
      log_info("TEST(RPCService_Test, RPCClient_SendRequest) - Received '%s'", buffer);
    }

  ASSERT_EQ(true, ret);
}

TEST(RPCService_Test, RPCClient_SendRequest_exception)
{
  bool ret = Initialise();

  if (ret)
    {
      ccs::HelperTools::SleepFor(100000000ul);
      ret = __client->IsConnected();
    }

  if (ret)
    {
      log_info("TEST(RPCService_Test, RPCClient_SendRequest_exception) - Request with scalar type ..");
      ccs::types::AnyValue request (ccs::types::Boolean); // PVAccessRPCClient::SendRequest expects a CompoundType
      ccs::types::AnyValue reply = __client->SendRequest(request);

      ret = ((ccs::types::Boolean == ccs::HelperTools::GetAttributeType(&reply, "status")) &&
	     (false == ccs::HelperTools::GetAttributeValue<ccs::types::boolean>(&reply, "status")));

      if (ret) log_info("TEST(RPCService_Test, RPCClient_SendRequest_exception) - Reason '%s'", reinterpret_cast<const char*>(ccs::HelperTools::GetAttributeReference(&reply, "reason")));
    }

  // Copy the base request type ..
  ccs::types::CompoundType request_type (*ccs::types::Request_int); // Default RPC request type
  request_type.AddAttribute("[0]", "uint32");
  request_type.AddAttribute("[1]", "uint32");

  if (ret)
    {
      ccs::types::AnyValue request (request_type);
      ccs::types::AnyValue reply = __client->SendRequest(request);

      ret = ((ccs::types::Boolean == ccs::HelperTools::GetAttributeType(&reply, "status")) &&
	     (false == ccs::HelperTools::GetAttributeValue<ccs::types::boolean>(&reply, "status")));

      if (ret) log_info("TEST(RPCService_Test, RPCClient_SendRequest_exception) - Reason '%s'", reinterpret_cast<const char*>(ccs::HelperTools::GetAttributeReference(&reply, "reason")));
    }

  if (ret)
    {
      ccs::base::RPCClient* client = new (std::nothrow) ccs::base::RPCClient ();
      client->SetService("NonExistantService"); client->Launch(); ccs::HelperTools::SleepFor(100000000ul);

      ccs::types::AnyValue request (ccs::types::Request_int);

      ccs::types::AnyValue reply = client->SendRequest(request);

      delete client;

      ret = ((ccs::types::Boolean == ccs::HelperTools::GetAttributeType(&reply, "status")) &&
	     (false == ccs::HelperTools::GetAttributeValue<ccs::types::boolean>(&reply, "status")));

      if (ret) log_info("TEST(RPCService_Test, RPCClient_SendRequest_exception) - Reason '%s'", reinterpret_cast<const char*>(ccs::HelperTools::GetAttributeReference(&reply, "reason")));
    }

  ASSERT_EQ(true, ret);
}

TEST(RPCService_Test, RPCServer_CallHandler_exception)
{
  bool ret = Initialise();

  if (ret)
    {
      ccs::HelperTools::SleepFor(100000000ul);
      ret = __client->IsConnected();
    }

  // Copy the base request type ..
  ccs::types::CompoundType request_type (*ccs::types::Request_int); // Default RPC request type

  if (ret)
    {
      ccs::types::AnyValue request (request_type);
      ccs::HelperTools::SetAttributeValue(&request, "qualifier", "throw"); // std::runtime_error

      ccs::types::AnyValue reply = __client->SendRequest(request);

      ret = ((ccs::types::Boolean == ccs::HelperTools::GetAttributeType(&reply, "status")) &&
	     (false == ccs::HelperTools::GetAttributeValue<ccs::types::boolean>(&reply, "status")));

      log_info("TEST(RPCService_Test, RPCServer_CallHandler_exception) - Reason '%s'", reinterpret_cast<const char*>(ccs::HelperTools::GetAttributeReference(&reply, "reason")));
    }

  if (ret)
    {
      request_type.AddAttribute("value", "uint32");

      ccs::types::AnyValue request (request_type);
      ccs::HelperTools::SetAttributeValue(&request, "qualifier", "throw"); // uint32

      ccs::types::AnyValue reply = __client->SendRequest(request);

      ret = ((ccs::types::Boolean == ccs::HelperTools::GetAttributeType(&reply, "status")) &&
	     (false == ccs::HelperTools::GetAttributeValue<ccs::types::boolean>(&reply, "status")));

      log_info("TEST(RPCService_Test, RPCServer_CallHandler_exception) - Reason '%s'", reinterpret_cast<const char*>(ccs::HelperTools::GetAttributeReference(&reply, "reason")));
    }

  ASSERT_EQ(true, ret);
}

TEST(RPCService_Test, RPCServer_Reply_exception)
{
  bool ret = Initialise();

  if (ret)
    {
      ccs::HelperTools::SleepFor(100000000ul);
      ret = __client->IsConnected();
    }

  // Copy the base request type ..
  ccs::types::CompoundType request_type (*ccs::types::Request_int); // Default RPC request type

  if (ret)
    {
      ccs::types::AnyValue request (request_type);
      ccs::HelperTools::SetAttributeValue(&request, "qualifier", "undef");

      ccs::types::AnyValue reply = __client->SendRequest(request);

      ret = ((ccs::types::Boolean == ccs::HelperTools::GetAttributeType(&reply, "status")) &&
	     (false == ccs::HelperTools::GetAttributeValue<ccs::types::boolean>(&reply, "status")));

      log_info("TEST(RPCService_Test, RPCServer_Reply_exception) - Reason '%s'", reinterpret_cast<const char*>(ccs::HelperTools::GetAttributeReference(&reply, "reason")));
    }

  if (ret)
    {
      ccs::types::AnyValue request (request_type);
      ccs::HelperTools::SetAttributeValue(&request, "qualifier", "bool");

      ccs::types::AnyValue reply = __client->SendRequest(request);

      ret = ((ccs::types::Boolean == ccs::HelperTools::GetAttributeType(&reply, "status")) &&
	     (false == ccs::HelperTools::GetAttributeValue<ccs::types::boolean>(&reply, "status")));

      log_info("TEST(RPCService_Test, RPCServer_Reply_exception) - Reason '%s'", reinterpret_cast<const char*>(ccs::HelperTools::GetAttributeReference(&reply, "reason")));
    }

  if (ret)
    {
      ccs::types::AnyValue request (request_type);
      ccs::HelperTools::SetAttributeValue(&request, "qualifier", "index");

      ccs::types::AnyValue reply = __client->SendRequest(request);

      ret = ((ccs::types::Boolean == ccs::HelperTools::GetAttributeType(&reply, "status")) &&
	     (false == ccs::HelperTools::GetAttributeValue<ccs::types::boolean>(&reply, "status")));

      log_info("TEST(RPCService_Test, RPCServer_Reply_exception) - Reason '%s'", reinterpret_cast<const char*>(ccs::HelperTools::GetAttributeReference(&reply, "reason")));
    }

  ASSERT_EQ(true, ret);
}

TEST(RPCService_Test, RPCClientImpl)
{
  bool ret = (ccs::base::RPCClientImplFactory::GetInstance()->Register("ccs::test::RPCClientImpl", &RPCClientImplConstructor) &&
	      ccs::base::RPCClientImplFactory::GetInstance()->SetDefault("ccs::test::RPCClientImpl"));

  __client_impl = false;

  ccs::base::RPCClient* client;

  if (ret)
    {
      client = new (std::nothrow) ccs::base::RPCClient ();
      client->SetService("NonExistantService"); client->Launch();
      ret = client->IsConnected();
    }

  // Copy the base request type ..
  ccs::types::CompoundType request_type (*ccs::types::Request_int); // Default RPC request type
  request_type.AddAttribute("value", "uint32");

  if (ret)
    {
      ccs::types::AnyValue request (request_type);
      ccs::HelperTools::SetAttributeValue(&request, "qualifier", "echo");
      ccs::HelperTools::SetAttributeValue(&request, "value", ccs::HelperTools::Hash<ccs::types::uint32>("echo"));

      ccs::types::AnyValue reply = client->SendRequest(request);

      ret = ((ccs::types::UnsignedInteger32 == ccs::HelperTools::GetAttributeType(&reply, "value")) &&
	     (ccs::HelperTools::Hash<ccs::types::uint32>("echo") == ccs::HelperTools::GetAttributeValue<ccs::types::uint32>(&reply, "value")));
    }

  if (ret)
    {
      ccs::types::AnyValue request (request_type);
      ccs::HelperTools::SetAttributeValue(&request, "qualifier", "throw");

      ccs::types::AnyValue reply = client->SendRequest(request);

      ret = ((ccs::types::Boolean == ccs::HelperTools::GetAttributeType(&reply, "status")) &&
	     (false == ccs::HelperTools::GetAttributeValue<ccs::types::boolean>(&reply, "status")));

      log_info("TEST(RPCService_Test, RPCClientImpl) - Reason '%s'", reinterpret_cast<const char*>(ccs::HelperTools::GetAttributeReference(&reply, "reason")));
    }

  delete client;

  if (ret)
    {
      ret = __client_impl;
    }

  ASSERT_EQ(true, ret);
}

TEST(RPCService_Test, RPCServerImpl)
{
  bool ret = (ccs::base::RPCServerImplFactory::GetInstance()->Register("ccs::test::RPCServerImpl", &RPCServerImplConstructor) &&
	      ccs::base::RPCServerImplFactory::GetInstance()->SetDefault("ccs::test::RPCServerImpl"));

  __server_impl = false;

  ccs::base::RPCServer* server;

  if (ret)
    {
      server = new (std::nothrow) RPCHandler ();
    }

  if (ret)
    {
      delete server;
    }

  if (ret)
    {
      ret = __server_impl;
    }

  ASSERT_EQ(true, ret);
}

TEST(RPCService_Test, RPCServerImpl_invalid)
{
  bool ret = (ccs::base::RPCClientImplFactory::GetInstance()->SetDefault("ccs::base::RPCClientUDP") &&
	      ccs::base::RPCServerImplFactory::GetInstance()->SetDefault("ccs::base::RPCServerUDP"));

  ccs::base::RPCClient* client;
  ccs::base::RPCServer* server;

  if (ret)
    {
      client = new (std::nothrow) ccs::base::RPCClient (RPCService_Tests_Server_name);
      server = new (std::nothrow) RPCHandler ();
    }

  // Copy the base request type ..
  ccs::types::CompoundType request_type (*ccs::types::Request_int); // Default RPC request type
  request_type.AddAttribute("value", "uint32");

  if (ret)
    {
      ccs::types::AnyValue request (request_type);
      ccs::HelperTools::SetAttributeValue(&request, "qualifier", "echo");
      ccs::HelperTools::SetAttributeValue(&request, "value", ccs::HelperTools::Hash<ccs::types::uint32>("echo"));

      ccs::types::AnyValue reply = client->SendRequest(request);

      ccs::HelperTools::LogSerialisedType(&reply);
      ccs::HelperTools::LogSerialisedInstance(&reply);

      // Unitialised implementation
      ret = ((ccs::types::Boolean == ccs::HelperTools::GetAttributeType(&reply, "status")) &&
	     (false == ccs::HelperTools::GetAttributeValue<bool>(&reply, "status")));
    }

  if (ret)
    {
      log_info("TEST(RPCService_Test, RPCServiceUDP) - Test successful");
    }

  delete client;
  delete server;

  ASSERT_EQ(true, ret);
}
