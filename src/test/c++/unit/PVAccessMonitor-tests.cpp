/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/test/c++/unit/PVAccessMonitor-tests.cpp $
* $Id: PVAccessMonitor-tests.cpp 114049 2020-10-20 06:28:48Z bauvirb $
*
* Project	: CODAC Core System
*
* Description	: Unit test code
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2019 ITER Organization,
*				  CS 90 046
*				  13067 St. Paul-lez-Durance Cedex
*				  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <gtest/gtest.h> // Google test framework

#include <common/BasicTypes.h> // Misc. type definition
#include <common/SysTools.h> // Misc. helper functions
#include <common/TimeTools.h> // Misc. helper functions

#include <common/log-api.h> // Syslog wrapper routines

#include <common/PVMonitor.h>

// Local header files

#include "PVAccessTypes.h"
#include "PVAccessServer.h"
#include "PVAccessMonitor.h"

// Constants

// Type definition

class PVAccessMonitorTest : public ccs::base::PVMonitor
{

  private:

    ccs::base::PVMonitor::Event __event = ccs::base::PVMonitor::Event::Disconnect;
    ccs::types::uint32 __value = 1234u;
    ccs::types::uint32 __count = 0u;
 
  public:

    PVAccessMonitorTest (const char* name) : ccs::base::PVMonitor(name) {};
    virtual ~PVAccessMonitorTest (void) {};

    // Implementation of ccs::base::PVMonitor interface
    virtual void HandleEvent (const ccs::base::PVMonitor::Event& event) {

      // For the sake of coverage of the default implementation ..
      ccs::base::PVMonitor::HandleEvent(event);
      __event = event;

    };

    virtual void HandleMonitor (const ccs::types::AnyValue& value) {

      bool status = ccs::HelperTools::GetAttributeValue<ccs::types::uint32>(&value, "value", __value);

      if (!status)
	{
	  ccs::types::uint32 value_sc = 0u;
	  ccs::types::uint32 value_ar = 0u;
          
	  status = (ccs::HelperTools::GetAttributeValue<ccs::types::uint32>(&value, "scalars.uint32", value_sc) &&
		    ccs::HelperTools::GetAttributeValue<ccs::types::uint32>(&value, "arrays.uint32[3]", value_ar) &&
		    (value_sc == value_ar));

	  if (status)
	    {
	      __value = value_sc;
	    }
	}

      if (status)
	{
	  __count += 1u;
	  log_info("PVAccessMonitorTest::HandleMonitor('%s') - Value '%u'", ccs::base::PVMonitor::GetChannel(), __value);
	}

    };

    ccs::base::PVMonitor::Event GetEvent (void) { return __event; };
    ccs::types::uint32 GetValue (void) { return __value; };
    ccs::types::uint32 GetCount (void) { return __count; };

};

// Function declaration

// Global variables

static ccs::log::Func_t __handler = ccs::log::SetStdout();
static ccs::base::PVAccessServer* __pva_srv = static_cast<ccs::base::PVAccessServer*>(NULL);

// Function definition

static inline bool Initialise (void)
{

  __pva_srv = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>();

  bool status = (static_cast<ccs::base::PVAccessServer*>(NULL) != __pva_srv);

  return status;

}

static inline bool Terminate (void)
{

  bool status = (static_cast<ccs::base::PVAccessServer*>(NULL) != __pva_srv);

  if (status)
    {
      ccs::base::PVAccessInterface::Terminate<ccs::base::PVAccessServer>();
      __pva_srv = static_cast<ccs::base::PVAccessServer*>(NULL);
    }

  return status;

}

TEST(PVAccessMonitor, Default) // Static initialisation
{
  bool ret = Initialise();

  ccs::base::PVMonitor* __mon = static_cast<ccs::base::PVMonitor*>(NULL);

  if (ret)
    {
      __mon = dynamic_cast<ccs::base::PVMonitor*>(new (std::nothrow) PVAccessMonitorTest ("PVA-SRV-TESTS:STRUCT"));
      ret = (static_cast<ccs::base::PVMonitor*>(NULL) != __mon);
    }
  
  if (ret)
    {
      ccs::HelperTools::SleepFor(100000000ul);
      ret = !__mon->IsConnected();
    }
  
  PVAccessMonitorTest* mon = dynamic_cast<PVAccessMonitorTest*>(__mon);
  
  if (ret)
    {
      ret = ((1234u == mon->GetValue()) && (0u == mon->GetCount()));
    }

  if (ret)
    {
      ret = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->AddVariable("PVA-SRV-TESTS:STRUCT", ccs::types::AnyputVariable, ccs::base::PVAccessTypes::Collection_int);
    }
  
  if (ret)
    {
      ret = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->SetPeriod(10000000ul);
    }

  if (ret)
    {
      ret = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->Launch();
    }

  ccs::HelperTools::SleepFor(1000000000ul);

  if (ret)
    { // Compose system command
      std::string command = std::string("/usr/bin/pvget -v PVA-SRV-TESTS:STRUCT > /tmp/pvget.out");
      ret = (std::system(command.c_str()) == 0);
      std::stringstream sstr; sstr << std::ifstream("/tmp/pvget.out").rdbuf();
      std::string result = sstr.str();
      log_info("TEST(PVAccessMonitor, Default) - Check variables exist ..");
      std::cout << result << std::endl;
     }

  if (ret)
    {
      log_info("TEST(PVAccessMonitor, Default) - Test variable connection ..");
      ccs::HelperTools::SleepFor(100000000ul);
      ret = __mon->IsConnected();
    }

  if (ret)
    {
      log_info("TEST(PVAccessMonitor, Default) - Test variable value ..");
      ret = ((0u == mon->GetValue()) && (1u == mon->GetCount()) && (ccs::base::PVMonitor::Event::Connect == mon->GetEvent()));
    }

  // Write to variable
  if (ret)
    {
      ret = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->IsValid("PVA-SRV-TESTS:STRUCT");
    }

  if (ret)
    {
      ccs::types::uint32 value = 1234u;
      ret = (ccs::HelperTools::SetAttributeValue(ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->GetVariable("PVA-SRV-TESTS:STRUCT"),
						 "scalars.uint32",
						 value) &&
	     ccs::HelperTools::SetAttributeValue(ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->GetVariable("PVA-SRV-TESTS:STRUCT"),
						 "arrays.uint32[3]",
						 value));
    }

  if (ret)
    {
      ret = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->UpdateVariable("PVA-SRV-TESTS:STRUCT");
      ccs::HelperTools::SleepFor(100000000ul);
    }

  if (ret)
    {
      ret = ((1234u == mon->GetValue()) && (2u == mon->GetCount()));
    }

  if (ret)
    {
      ret = Terminate();
      ccs::HelperTools::SleepFor(100000000ul);
    }

  if (ret)
    {
      ret = !__mon->IsConnected();
    }

  if (ret)
    {
      ret = (ccs::base::PVMonitor::Event::Disconnect == mon->GetEvent());
    }

  if (ret)
    {
      delete __mon;
    }

  ASSERT_EQ(true, ret);
}

TEST(PVAccessMonitor, Instances) // Static initialisation
{
  bool ret = Initialise();

  PVAccessMonitorTest* mon_inv = new (std::nothrow) PVAccessMonitorTest ("PVA-SRV-TESTS:INVALID");
  PVAccessMonitorTest* mon_int = new (std::nothrow) PVAccessMonitorTest ("PVA-SRV-TESTS:INTEGER");
  PVAccessMonitorTest* mon_str = new (std::nothrow) PVAccessMonitorTest ("PVA-SRV-TESTS:STRUCT");

  if (ret)
    {
      ccs::HelperTools::SleepFor(100000000ul);
      ret = (!mon_inv->IsConnected() && !mon_int->IsConnected() && !mon_str->IsConnected());
    }
  
  if (ret)
    {
      ret = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->AddVariable("PVA-SRV-TESTS:INTEGER", ccs::types::AnyputVariable, ccs::types::UnsignedInteger32);
    }
  
  if (ret)
    {
      ret = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->AddVariable("PVA-SRV-TESTS:STRUCT", ccs::types::AnyputVariable, ccs::base::PVAccessTypes::Collection_int);
    }
  
  if (ret)
    {
      ret = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->SetPeriod(10000000ul);
    }

  if (ret)
    {
      ret = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->Launch();
    }

  ccs::HelperTools::SleepFor(1000000000ul);

  if (ret)
    { // Compose system command
      std::string command = std::string("/usr/bin/pvget -v PVA-SRV-TESTS:INTEGER PVA-SRV-TESTS:STRUCT > /tmp/pvget.out");
      ret = (std::system(command.c_str()) == 0);
      std::stringstream sstr; sstr << std::ifstream("/tmp/pvget.out").rdbuf();
      std::string result = sstr.str();
      log_info("TEST(PVAccessServer, Instances) - Check variables exist ..");
      std::cout << result << std::endl;
     }

  if (ret)
    {
      ccs::HelperTools::SleepFor(100000000ul);
      ret = (!mon_inv->IsConnected() && mon_int->IsConnected() && mon_str->IsConnected());
    }

  if (ret)
    {
      ret = ((0u == mon_int->GetValue()) && (1u == mon_int->GetCount()) && (ccs::base::PVMonitor::Event::Connect == mon_int->GetEvent()));
    }

  if (ret)
    {
      ret = ((0u == mon_str->GetValue()) && (1u == mon_str->GetCount()) && (ccs::base::PVMonitor::Event::Connect == mon_str->GetEvent()));
    }

  // Write to variable
  if (ret)
    {
      ret = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->IsValid("PVA-SRV-TESTS:STRUCT");
    }

  if (ret)
    {
      ccs::types::uint32 value = 1234u;
      ret = (ccs::HelperTools::SetAttributeValue(ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->GetVariable("PVA-SRV-TESTS:STRUCT"),
						 "scalars.uint32",
						 value) &&
	     ccs::HelperTools::SetAttributeValue(ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->GetVariable("PVA-SRV-TESTS:STRUCT"),
						 "arrays.uint32[3]",
						 value));
    }

  if (ret)
    {
      ret = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->UpdateVariable("PVA-SRV-TESTS:STRUCT");
      ccs::HelperTools::SleepFor(100000000ul);
    }

  if (ret)
    {
      ret = ((0u == mon_int->GetValue()) && (1u == mon_int->GetCount()));
    }

  if (ret)
    {
      ret = ((1234u == mon_str->GetValue()) && (2u == mon_str->GetCount()));
    }

  if (ret)
    {
      ret = Terminate();
      ccs::HelperTools::SleepFor(100000000ul);
    }

  if (ret)
    {
      ret = (!mon_inv->IsConnected() && !mon_int->IsConnected() && !mon_str->IsConnected());
    }

  if (ret)
    {
      ret = ((ccs::base::PVMonitor::Event::Disconnect == mon_int->GetEvent()) && (ccs::base::PVMonitor::Event::Disconnect == mon_str->GetEvent()));
    }

  if (ret)
    {
      delete mon_inv;
      delete mon_int;
      delete mon_str;
    }

  ASSERT_EQ(true, ret);
}

