/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/test/c++/unit/PVAccessServerContext-tests.cpp $
* $Id: PVAccessServerContext-tests.cpp 111234 2020-07-06 11:58:25Z bauvirb $
*
* Project	: CODAC Core System
*
* Description	: Unit test code
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2019 ITER Organization,
*				  CS 90 046
*				  13067 St. Paul-lez-Durance Cedex
*				  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <gtest/gtest.h> // Google test framework

#include <common/BasicTypes.h> // Misc. type definition

#include <common/log-api.h> // Syslog wrapper routines

// Local header files

#include "PVAccessServerContext.h"

// Constants

// Type definition

// Function declaration

// Global variables

static ccs::log::Func_t __handler = ccs::log::SetStdout();

// Function definition

TEST(PVAccessServerContext, GetInstance)
{
  bool ret = static_cast<bool>(ccs::base::PVAccessServerContext::GetInstance());

  if (ret)
    {
      ccs::base::PVAccessServerContext::Terminate();
    }

  ASSERT_EQ(true, ret);
}

TEST(PVAccessServerContext, SetInstance)
{
  ::ccs::base::SharedReference<pvxs::server::Server> context (new (std::nothrow) pvxs::server::Server (pvxs::server::Config::from_env().build()));

  bool ret = static_cast<bool>(context);

  if (ret)
    {
      ret = ccs::base::PVAccessServerContext::SetInstance(context);
    }

  if (ret)
    {
      ret = (2u == context.GetCount());
    }

  if (ret)
    {
      ::ccs::base::SharedReference<pvxs::server::Server> _context = ccs::base::PVAccessServerContext::GetInstance();
      ret = (3u == context.GetCount());
    }

  if (ret)
    {
      ret = (2u == context.GetCount());
    }

  if (ret)
    {
      context.Discard();
      ret = ccs::base::PVAccessServerContext::IsInitialised(); // Singleton instance still there but this instance has discarded as well the reference to the counter
    }

  if (ret)
    {
      ret = ccs::base::PVAccessServerContext::Terminate();
    }

  if (ret)
    {
      ret = !ccs::base::PVAccessServerContext::IsInitialised();
    }

  ASSERT_EQ(true, ret);
}

TEST(PVAccessServerContext, SetInstance_Preexist)
{
  bool ret = static_cast<bool>(ccs::base::PVAccessServerContext::GetInstance());

  if (ret)
    {
      ret = ccs::base::PVAccessServerContext::IsInitialised();
    }

  ::ccs::base::SharedReference<pvxs::server::Server> context (new (std::nothrow) pvxs::server::Server (pvxs::server::Config::from_env().build()));

  if (ret)
    {
      ret = ccs::base::PVAccessServerContext::SetInstance(context);
    }

  if (ret)
    {
      ret = (2u == context.GetCount());
    }

  if (ret)
    {
      context.Discard();
      ret = ccs::base::PVAccessServerContext::IsInitialised(); // Singleton instance still there but this instance has discarded as well the reference to the counter
    }

  if (ret)
    {
      ret = ccs::base::PVAccessServerContext::Terminate();
    }

  if (ret)
    {
      ret = !ccs::base::PVAccessServerContext::IsInitialised();
    }

  ASSERT_EQ(true, ret);
}

