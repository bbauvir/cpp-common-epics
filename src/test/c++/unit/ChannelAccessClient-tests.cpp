/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/test/c++/unit/ChannelAccessClient-tests.cpp $
* $Id: ChannelAccessClient-tests.cpp 110289 2020-06-12 12:21:14Z bauvirb $
*
* Project	: CODAC Core System
*
* Description	: Unit test code
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2019 ITER Organization,
*				  CS 90 046
*				  13067 St. Paul-lez-Durance Cedex
*				  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <cadef.h>

#include <gtest/gtest.h> // Google test framework

#include <common/BasicTypes.h> // Misc. type definition
#include <common/SysTools.h> // Misc. helper functions
#include <common/TimeTools.h> // Misc. helper functions

#include <common/log-api.h> // Syslog wrapper routines

// Local header files

#include "ChannelAccessClient.h"

// Constants

#define SDD_PVMAXLENGTH 40
#define SDD_STRING_MAX_LENGTH 100
#define SDD_NB_OF_PONVAR 3

// Type definition

struct SDD_PONVar { // SDD-generated lists

  char name[SDD_PVMAXLENGTH];
  
  bool isMonitored;
  
  chtype type;

  // cppcheck-suppress unusedStructMember // SDD-generated type outside the scope of the unit under test
  char initialvalue[SDD_STRING_MAX_LENGTH];

};

// Function declaration

// Global variables

static ccs::log::Func_t __handler = ccs::log::SetStdout();
static ccs::base::ChannelAccessClient* __ca_client = static_cast<ccs::base::ChannelAccessClient*>(NULL);

static SDD_PONVar __pvlist [] = {

  {"CA-TESTS-SDD:BOOL", true, DBR_ENUM, "FALSE"},
  {"CA-TESTS-SDD:FLOAT", true, DBR_DOUBLE, "0"},
  {"CA-TESTS-SDD:STRING", false, DBR_STRING, "0"},

};

// Function definition

static inline bool Initialise (void)
{

  __ca_client = ccs::base::ChannelAccessInterface::GetInstance<ccs::base::ChannelAccessClient>();

  bool status = (static_cast<ccs::base::ChannelAccessClient*>(NULL) != __ca_client);

  if (status)
    { // Start test IOC
      // Compose system command
      std::string command = std::string("/usr/bin/screen -d -m /usr/bin/softIoc -d ./unit/ChannelAccessClient-tests.db > /tmp/ioc.out");
      status = (std::system(command.c_str()) == 0);
     }

  if (status)
    {
      ccs::HelperTools::SleepFor(1000000000ul);
    }

  if (status)
    { // Compose system command
      std::string command = std::string("/usr/bin/caget CA-TESTS:BOOL CA-TESTS:FLOAT CA-TESTS:STRING > /tmp/caget.out");
      status = (std::system(command.c_str()) == 0);
      std::stringstream sstr; sstr << std::ifstream("/tmp/caget.out").rdbuf();
      std::string result = sstr.str();
      log_info("TEST(ChannelAccessClient, Initialise) - Check variables exist ..");
      std::cout << result << std::endl;
     }

  return status;

}

static inline bool Terminate (void)
{

  bool status = (static_cast<ccs::base::ChannelAccessClient*>(NULL) != __ca_client);

  if (status)
    {
      ccs::base::ChannelAccessInterface::Terminate<ccs::base::ChannelAccessClient>();
      __ca_client = static_cast<ccs::base::ChannelAccessClient*>(NULL);
    }

  if (status)
    { // Stop test IOC
      // Compose system command
      std::string command = std::string("/usr/bin/kill -9 `/usr/sbin/pidof softIoc`");
      status = (std::system(command.c_str()) == 0);
     }

  return status;

}

TEST(ChannelAccessClient, Default) // Static initialisation
{
  bool ret = Initialise();

  if (ret)
    {
      ret = ccs::base::ChannelAccessInterface::GetInstance<ccs::base::ChannelAccessClient>()->AddVariable("CA-TESTS:BOOL", ccs::types::AnyputVariable, ccs::types::Boolean);
    }

  if (ret)
    {
      ret = ccs::base::ChannelAccessInterface::GetInstance<ccs::base::ChannelAccessClient>()->AddVariable("CA-TESTS:FLOAT", ccs::types::AnyputVariable, ccs::types::Float32);
    }

  if (ret)
    {
      ret = ccs::base::ChannelAccessInterface::GetInstance<ccs::base::ChannelAccessClient>()->AddVariable("CA-TESTS:STRING", ccs::types::AnyputVariable, ccs::types::String);
    }

  if (ret)
    {
      ccs::base::SharedReference<ccs::types::AnyType> type (ccs::HelperTools::NewArrayType("char8[]", ccs::types::Character8, 1024));
      ret = ccs::base::ChannelAccessInterface::GetInstance<ccs::base::ChannelAccessClient>()->AddVariable("CA-TESTS:CHARRAY", ccs::types::AnyputVariable, type);
    }

  // SDD-generated PV lists
  for (ccs::types::uint32 index = 0u; (ret && (index < SDD_NB_OF_PONVAR)); index += 1u)
    {
      ret = ccs::base::ChannelAccessInterface::GetInstance<ccs::base::ChannelAccessClient>()->AddVariable(__pvlist[index].name, __pvlist[index].isMonitored, __pvlist[index].type);
    }
#if 0
  if (ret)
    {
      ret = ccs::base::ChannelAccessInterface::GetInstance<ccs::base::ChannelAccessClient>()->SetPeriod(10000000ul);
    }
#endif
  if (ret)
    {
      ret = ccs::base::ChannelAccessInterface::GetInstance<ccs::base::ChannelAccessClient>()->Launch();
    }

  if (ret)
    {
      ccs::types::boolean variable = true;
      ret = ccs::base::ChannelAccessInterface::GetInstance<ccs::base::ChannelAccessClient>()->SetVariable("CA-TESTS:BOOL", variable);
    }

  if (ret)
    {
      ccs::types::float32 variable = 0.25f;
      ret = ccs::base::ChannelAccessInterface::GetInstance<ccs::base::ChannelAccessClient>()->SetVariable("CA-TESTS:FLOAT", variable);
    }

  if (ret)
    {
      ccs::types::string variable = "some value";
      ret = ccs::HelperTools::SafeStringCopy(reinterpret_cast<char*>(ccs::base::ChannelAccessInterface::GetInstance<ccs::base::ChannelAccessClient>()->GetVariable("CA-TESTS:STRING")->GetInstance()), 
					     variable,
					     ccs::types::MaxStringLength);
    }

  if (ret)
    {
      ret = ccs::base::ChannelAccessInterface::GetInstance<ccs::base::ChannelAccessClient>()->UpdateVariable("CA-TESTS:STRING");
    }

  ccs::HelperTools::SleepFor(1000000000ul);

  if (ret)
    { // Compose system command
      std::string command = std::string("/usr/bin/caget CA-TESTS:BOOL CA-TESTS:FLOAT CA-TESTS:STRING > /tmp/caget.out");
      ret = (std::system(command.c_str()) == 0);
      std::stringstream sstr; sstr << std::ifstream("/tmp/caget.out").rdbuf();
      std::string result = sstr.str();
      log_info("TEST(ChannelAccessClient, Default) - ..");
      std::cout << result << std::endl;
    }

  if (ret)
    {
      ccs::types::boolean variable = false;
      ret = (ccs::base::ChannelAccessInterface::GetInstance<ccs::base::ChannelAccessClient>()->GetVariable("CA-TESTS:BOOL", variable) &&
	     (true == variable));
    }

  if (ret)
    {
      ccs::types::float32 variable = 0.0f;
      ret = (ccs::base::ChannelAccessInterface::GetInstance<ccs::base::ChannelAccessClient>()->GetVariable("CA-TESTS:FLOAT", variable) &&
	     (0.25f == variable));
    }

  // ToDo - Test character array

  if (ret)
    {
      ccs::types::char8 variable [1024] = "Some very long string which is longer than the maximum length of EPICSv3 string and should be serialised on a waveform record";
      ret = ccs::HelperTools::SafeStringCopy(reinterpret_cast<char*>(ccs::base::ChannelAccessInterface::GetInstance<ccs::base::ChannelAccessClient>()->GetVariable("CA-TESTS:CHARRAY")->GetInstance()), 
					     variable,
					     1024u);
    }

  if (ret)
    {
      ret = ccs::base::ChannelAccessInterface::GetInstance<ccs::base::ChannelAccessClient>()->UpdateVariable("CA-TESTS:CHARRAY");
    }

  ccs::HelperTools::SleepFor(1000000000ul);

  if (ret)
    { // Compose system command
      std::string command = std::string("/usr/bin/caget -S CA-TESTS:CHARRAY > /tmp/caget.out");
      ret = (std::system(command.c_str()) == 0);
      std::stringstream sstr; sstr << std::ifstream("/tmp/caget.out").rdbuf();
      std::string result = sstr.str();
      log_info("TEST(ChannelAccessClient, Default) - ..");
      std::cout << result << std::endl;
    }

  if (ret)
    {
      ret = Terminate();
    }

  ASSERT_EQ(true, ret);
}

