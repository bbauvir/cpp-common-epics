/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/test/c++/unit/PVAccessClient-tests.cpp $
* $Id: PVAccessClient-tests.cpp 114049 2020-10-20 06:28:48Z bauvirb $
*
* Project	: CODAC Core System
*
* Description	: Unit test code
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2019 ITER Organization,
*				  CS 90 046
*				  13067 St. Paul-lez-Durance Cedex
*				  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <gtest/gtest.h> // Google test framework

#include <common/BasicTypes.h> // Misc. type definition
#include <common/SysTools.h> // Misc. helper functions
#include <common/TimeTools.h> // Misc. helper functions

#include <common/log-api.h> // Syslog wrapper routines

// Local header files

#include "PVAccessTypes.h"
#include "PVAccessClient.h"
#include "PVAccessServer.h"

// Constants

// Type definition

class PVAccessClientTest
{

  private:

    ccs::types::uint32 __value = 0u;
    ccs::types::uint32 __count = 0u;
 
  public:

    PVAccessClientTest (void) {};
    virtual ~PVAccessClientTest (void) {};

    ccs::types::uint32 GetValue (void) { return __value; };
    ccs::types::uint32 GetCount (void) { return __count; };

    // Callback for PV update
    void Callback (const ccs::types::AnyValue& value) {

      char buffer [1024] = STRING_UNDEFINED; value.SerialiseInstance(buffer, 1024u);
      log_info("PVAccessClientTest::Callback - Value '%s'", buffer);

      __count += 1u;
      __value = ccs::HelperTools::GetAttributeValue<ccs::types::uint32>(&value, "scalars.uint32");

    };

    bool RegisterCallback (void) {

      return ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessClient>()->SetCallback("PVA-SRV-TESTS:STRUCT", 
												 std::bind(&PVAccessClientTest::Callback, this, std::placeholders::_1));
    };

};

// Function declaration

// Global variables

static ccs::log::Func_t __handler = ccs::log::SetStdout();
static ccs::base::PVAccessClient* __pva_cln = static_cast<ccs::base::PVAccessClient*>(NULL);
static ccs::base::PVAccessServer* __pva_srv = static_cast<ccs::base::PVAccessServer*>(NULL);

// Function definition

static inline bool Initialise (void)
{

  __pva_cln = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessClient>();
  __pva_srv = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>();

  bool status = ((static_cast<ccs::base::PVAccessClient*>(NULL) != __pva_cln) && 
		 (static_cast<ccs::base::PVAccessServer*>(NULL) != __pva_srv));

  if (status)
    {
      status = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->AddVariable("PVA-SRV-TESTS:INTEGER", ccs::types::AnyputVariable, ccs::types::UnsignedInteger32);
    }

  if (status)
    {
      status = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->AddVariable("PVA-SRV-TESTS:STRUCT", ccs::types::AnyputVariable, ccs::base::PVAccessTypes::Collection_int);
    }

  if (status)
    {
      status = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->Launch();
    }

  return status;

}

static inline bool Terminate (void)
{

  bool status = ((static_cast<ccs::base::PVAccessClient*>(NULL) != __pva_cln) && 
		 (static_cast<ccs::base::PVAccessServer*>(NULL) != __pva_srv));

  if (status)
    {
      ccs::base::PVAccessInterface::Terminate<ccs::base::PVAccessServer>();
      __pva_srv = static_cast<ccs::base::PVAccessServer*>(NULL);
    }

  if (status)
    {
      ccs::base::PVAccessInterface::Terminate<ccs::base::PVAccessClient>();
      __pva_cln = static_cast<ccs::base::PVAccessClient*>(NULL);
    }

  return status;

}

TEST(PVAccessClient, Constructor)
{
  bool ret = Initialise();

  if (ret)
    {
      ret = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessClient>()->Launch();
    }

  if (ret)
    {
      ret = Terminate();
    }

  ASSERT_EQ(true, ret);
}

TEST(PVAccessClient, Default) // Static initialisation
{
  bool ret = Initialise();

  PVAccessClientTest* test = static_cast<PVAccessClientTest*>(NULL);

  if (ret)
    {
      test = new (std::nothrow) PVAccessClientTest ();
      ret = (static_cast<PVAccessClientTest*>(NULL) != test);
    }

  if (ret)
    {
      ret = (0u == test->GetValue());
    }

  if (ret)
    {
      ret = (false == test->RegisterCallback()); // Expect failure
    }

  if (ret)
    {
      ret = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessClient>()->AddVariable("PVA-SRV-TESTS:INVALID", ccs::types::AnyputVariable);
    }

  if (ret)
    {
      ret = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessClient>()->AddVariable("PVA-SRV-TESTS:INTEGER", ccs::types::AnyputVariable);
    }

  if (ret)
    {
      ret = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessClient>()->AddVariable("PVA-SRV-TESTS:STRUCT", ccs::types::AnyputVariable);
    }

  if (ret)
    {
      ret = test->RegisterCallback();
    }

  if (ret)
    {
      ret = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessClient>()->SetPeriod(10000000ul);
    }

  if (ret)
    {
      ret = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessClient>()->Launch();
    }

  ccs::HelperTools::SleepFor(1000000000ul);

  if (ret)
    { // Compose system command
      std::string command = std::string("/usr/bin/pvget -v PVA-SRV-TESTS:INTEGER PVA-SRV-TESTS:STRUCT > /tmp/pvget.out");
      ret = (std::system(command.c_str()) == 0);
      std::stringstream sstr; sstr << std::ifstream("/tmp/pvget.out").rdbuf();
      std::string result = sstr.str();
      log_info("TEST(PVAccessClient, Default) - Check variables exist ..");
      std::cout << result << std::endl;
    }

  if (ret)
    {
      log_info("TEST(PVAccessClient, Default) - Check variables exist in the cache ..");
      ret = (ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessClient>()->IsValid("PVA-SRV-TESTS:INVALID") &&
	     ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessClient>()->IsValid("PVA-SRV-TESTS:INTEGER") &&
	     ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessClient>()->IsValid("PVA-SRV-TESTS:STRUCT"));
    }

  if (ret)
    {
      log_info("TEST(PVAccessClient, Default) - Check variables are connected ..");
      ret = (!ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessClient>()->IsConnected("PVA-SRV-TESTS:INVALID") &&
	     ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessClient>()->IsConnected("PVA-SRV-TESTS:INTEGER") &&
	     ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessClient>()->IsConnected("PVA-SRV-TESTS:STRUCT"));
    }

  if (ret)
    {
      log_info("TEST(PVAccessClient, Default) - Test invalid variable ..");
      ret = ((static_cast<ccs::types::AnyValue*>(NULL) != ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessClient>()->GetVariable("PVA-SRV-TESTS:INVALID")) && // WARNING - Always instantiated, even in absence of PV
	     (false == static_cast<bool>(ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessClient>()->GetVariable("PVA-SRV-TESTS:INVALID")->GetType()))); // Invalid type
    }

  if (ret)
    {
      log_info("TEST(PVAccessClient, Default) - Test valid variable ..");
      ret = ((static_cast<ccs::types::AnyValue*>(NULL) != ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessClient>()->GetVariable("PVA-SRV-TESTS:STRUCT")) &&
	     (true == static_cast<bool>(ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessClient>()->GetVariable("PVA-SRV-TESTS:STRUCT")->GetType()))); // Valid type

      if (ret)
	{
	  ccs::HelperTools::LogSerialisedType(ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessClient>()->GetVariable("PVA-SRV-TESTS:STRUCT"));
	  ccs::HelperTools::LogSerialisedInstance(ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessClient>()->GetVariable("PVA-SRV-TESTS:STRUCT"));
	}
    }

  if (ret)
    {
      log_info("TEST(PVAccessClient, Default) - Set value ..");
      ccs::types::uint32 value = 1234u;
      ret = ccs::HelperTools::SetAttributeValue(ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessClient>()->GetVariable("PVA-SRV-TESTS:STRUCT"),
						"scalars.uint32",
						value);

      if (ret)
	{
	  log_info("TEST(PVAccessClient, Default) - Updated cache ..");
	  ccs::HelperTools::LogSerialisedInstance(ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessClient>()->GetVariable("PVA-SRV-TESTS:STRUCT"));
	}
      else
	{
	  log_error("TEST(PVAccessClient, Default) - .. unforeseen failure");
	}
    }

  if (ret)
    {
      ret = ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessClient>()->UpdateVariable("PVA-SRV-TESTS:STRUCT");
      ccs::HelperTools::SleepFor(1000000000ul);
    }

  if (ret)
    { // Compose system command
      std::string command = std::string("/usr/bin/pvget -v PVA-SRV-TESTS:INTEGER PVA-SRV-TESTS:STRUCT > /tmp/pvget.out");
      ret = (std::system(command.c_str()) == 0);
      std::stringstream sstr; sstr << std::ifstream("/tmp/pvget.out").rdbuf();
      std::string result = sstr.str();
      log_info("TEST(PVAccessClient, Default) - Check variables have been updated ..");
      std::cout << result << std::endl;
    }

  if (ret)
    {
      ccs::types::uint32 value = 0u;
      ret = (ccs::HelperTools::GetAttributeValue(ccs::base::PVAccessInterface::GetInstance<ccs::base::PVAccessServer>()->GetVariable("PVA-SRV-TESTS:STRUCT"),
						"scalars.uint32",
						value) &&
	     (1234u == value)); // Server should have been updated
    }

  if (ret)
    {
      ret = ((0u < test->GetCount()) && (1234u == test->GetValue())); // Local callback should have picked it up
    }

  Terminate();

  if (ret)
    {
      delete test;
    }

  ASSERT_EQ(true, ret);
}

