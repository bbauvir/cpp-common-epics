/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/test/c++/unit/ChannelAccessWrapper-tests.cpp $
* $Id: ChannelAccessWrapper-tests.cpp 111303 2020-07-08 05:58:03Z bauvirb $
*
* Project	: CODAC Core System
*
* Description	: Unit test code
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2019 ITER Organization,
*				  CS 90 046
*				  13067 St. Paul-lez-Durance Cedex
*				  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <gtest/gtest.h> // Google test framework

#include <common/BasicTypes.h> // Misc. type definition
#include <common/SysTools.h> // Misc. helper functions
#include <common/TimeTools.h> // Misc. helper functions

#include <common/log-api.h> // Syslog wrapper routines

// Local header files

#include "ca-wrap.h"

// Constants

// Type definition

// Function declaration

// Global variables

static ccs::log::Func_t __handler = ccs::log::SetStdout();
static bool __initialised = false;

// Function definition

static inline bool Initialise (void)
{

  bool status = (false == __initialised);

  if (status)
    { // Start test IOC
      // Compose system command
      std::string command = std::string("/usr/bin/screen -d -m /usr/bin/softIoc -d ./unit/ChannelAccessClient-tests.db > /tmp/ioc.out");
      status = (std::system(command.c_str()) == 0);
     }

  if (status)
    {
      ccs::HelperTools::SleepFor(1000000000ul);
    }

  if (status)
    { // Compose system command
      std::string command = std::string("/usr/bin/caget CA-TESTS:BOOL CA-TESTS:FLOAT CA-TESTS:STRING > /tmp/caget.out");
      status = (std::system(command.c_str()) == 0);
      std::stringstream sstr; sstr << std::ifstream("/tmp/caget.out").rdbuf();
      std::string result = sstr.str();
      log_info("TEST(ChannelAccessClient, Initialise) - Check variables exist ..");
      std::cout << result << std::endl;
     }

  if (status)
    {
      status = CAInterface_CreateContext();
    }

  if (status)
    {
      __initialised = true;
    }

  return status;

}

static inline bool Terminate (void)
{

  bool status = (true == __initialised);

  if (status)
    {
      status = CAInterface_ClearContext();
    }

  if (status)
    { // Stop test IOC
      // Compose system command
      std::string command = std::string("/usr/bin/kill -9 `/usr/sbin/pidof softIoc`");
      status = (std::system(command.c_str()) == 0);
    }

  if (status)
    {
      __initialised = false;
    }

  return status;

}

TEST(ChannelAccessWrapper, Default) // Static initialisation
{
  bool ret = Initialise();

  chid channel = 0;

  if (ret)
    {
      ret = CAInterface_ConnectVariable("CA-TESTS:BOOL", channel); // By reference
    }

  if (ret)
    {
      ccs::HelperTools::SleepFor(100000000ul);
      ret = CAInterface_IsConnected(channel);
    }

  if (ret)
    {
      ccs::types::boolean variable = true;
      ret = CAInterface_WriteVariable(channel, DBR_CHAR, &variable);
    }

  if (ret)
    {
      ccs::types::boolean variable = false;
      ret = (CAInterface_ReadVariable(channel, DBR_CHAR, &variable) && (true == variable));
    }

  if (ret)
    {
      ret = CAInterface_DetachVariable(channel);
    }

  Terminate();

  ASSERT_EQ(true, ret);
}

