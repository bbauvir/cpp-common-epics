/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/test/c++/rpc-echo/rpc-echo.cpp $
* $Id: rpc-echo.cpp 114049 2020-10-20 06:28:48Z bauvirb $
*
* Project	: CODAC Core System
*
* Description	: SDN Software - Communication API - Simple examples
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2019 ITER Organization,
*				  CS 90 046
*				  13067 St. Paul-lez-Durance Cedex
*				  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <stdio.h> // sscanf, printf, etc.
#include <string.h> // strncpy, etc.
#include <stdarg.h> // va_start, etc.
#include <signal.h> // sigset, etc.

#include <new> // std::nothrow

#include <common/BasicTypes.h> // Misc. type definition, e.g. RET_STATUS

#include <common/log-api.h> // Logging helper functions

#include <common/RPCServer.h>

// Local header files

// Constants

#define DEFAULT_SERVICE_NAME "rpc@service"

// Type definition

class RPCHandler : public ::ccs::base::RPCServer 
{
  
  private:

  public:
  
    RPCHandler (const ::ccs::types::char8 * const service) : ::ccs::base::RPCServer(service) {};
    virtual ~RPCHandler (void) {};
  
    // RPC handler method
    // cppcheck-suppress unusedFunction // Not run part of automated tests
    virtual ccs::types::AnyValue HandleRequest (const ccs::types::AnyValue& request) { return request; };

};
  
// Global variables

bool _terminate = false;

// Function definition

void print_usage (void)
{

  char prog_name [STRING_MAX_LENGTH] = STRING_UNDEFINED;

  get_program_name((char*) prog_name);

  fprintf(stdout, "Usage: %s <options>\n", prog_name);
  fprintf(stdout, "Options: -h|--help: Print usage.\n");
  fprintf(stdout, "         -s|--service <service>: Try and connect RPC client to the named RPC service, defaults to '%s'.\n", DEFAULT_SERVICE_NAME);
  fprintf(stdout, "         -v|--verbose: Logs to stdout.\n");
  fprintf(stdout, "\n");
  fprintf(stdout, "The program instantiates a RPC service and replies whichever message is received, etc.\n");
  fprintf(stdout, "\n");

  return;

};

void signal_handler (int signal)
{

  log_info("Received signal '%d' to terminate", signal);
  _terminate = true;

};

int main (int argc, char** argv) 
{

  using namespace ::ccs::types;
  using namespace ::ccs::base;

  // Install signal handler to support graceful termination
  sigset(SIGTERM, signal_handler);
  sigset(SIGINT,  signal_handler);
  sigset(SIGHUP,  signal_handler);

  char service [STRING_MAX_LENGTH] = STRING_UNDEFINED; ccs::HelperTools::SafeStringCopy(service, DEFAULT_SERVICE_NAME, MaxStringLength);

  if (argc > 1)
    {
      for (uint_t index = 1; index < (uint_t) argc; index++)
	{
          if ((strcmp(argv[index], "-h") == 0) || (strcmp(argv[index], "--help") == 0))
	    {
	      // Display usage
	      print_usage();
	      return (0);
	    }
	  else if ((strcmp(argv[index], "-s") == 0) || (strcmp(argv[index], "--service") == 0))
	    {
	      if ((index + 1) < (uint_t) argc) ccs::HelperTools::SafeStringCopy(service, argv[index + 1], MaxStringLength);
	      else { print_usage(); return (0); } // Display usage
	      index += 1;
            
	    }
	  else if ((strcmp(argv[index], "-v") == 0) || (strcmp(argv[index], "--verbose") == 0))
	    {
	      // Log to stdout
	      ccs::log::Func_t old_plugin = ccs::log::SetStdout();
	      ccs::log::Severity_t old_filter = ccs::log::SetFilter(LOG_DEBUG);

	    }
	}
    }
  else
    {
    }

  // Create ConfigurationLoader instance
  log_info("Create RPC server on '%s' service", service);
  RPCHandler handler (service);

  while (_terminate != true)
    {
      ::ccs::HelperTools::SleepFor(100000000ul);
    }

   // Terminate
  log_info("Terminate program");

  return (0);

}
