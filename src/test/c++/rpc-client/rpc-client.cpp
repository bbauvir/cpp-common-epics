/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/test/c++/rpc-client/rpc-client.cpp $
* $Id: rpc-client.cpp 111795 2020-07-20 06:38:56Z bauvirb $
*
* Project	: CODAC Core System
*
* Description	: SDN Software - Communication API - Simple examples
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2019 ITER Organization,
*				  CS 90 046
*				  13067 St. Paul-lez-Durance Cedex
*				  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <stdio.h> // sscanf, printf, etc.
#include <string.h> // strncpy, etc.
#include <stdarg.h> // va_start, etc.
#include <signal.h> // sigset, etc.

#include <new> // std::nothrow

#include <common/BasicTypes.h> // Misc. type definition, e.g. RET_STATUS

#include <common/log-api.h> // Logging helper functions

#include <common/RPCClient.h>

// Local header files

// Constants

#define DEFAULT_SERVICE_NAME "rpc@service"
#define DEFAULT_REQUEST_TYPE "{\"type\":\"rpcStruct\",\"attributes\":[{\"value\":{\"type\":\"uint64\", \"size\":8}}]}"
#define DEFAULT_REQUEST_INST "{\"value\":0}"

// Type definition

// Global variables

bool _terminate = false;

// Function definition

void print_usage (void)
{

  char prog_name [STRING_MAX_LENGTH] = STRING_UNDEFINED;

  get_program_name((char*) prog_name);

  fprintf(stdout, "Usage: %s <options>\n", prog_name);
  fprintf(stdout, "Options: -h|--help: Print usage.\n");
  fprintf(stdout, "         -s|--service <service>: Try and connect RPC client to the named RPC service, defaults to '%s'.\n", DEFAULT_SERVICE_NAME);
  fprintf(stdout, "         -t|--type <type>: Request data type as JSON string, defaults to '%s'.\n", DEFAULT_REQUEST_TYPE);
  fprintf(stdout, "         -i|--instance <inst>: Request value as JSON string, defaults to '%s'.\n", DEFAULT_REQUEST_INST);
  fprintf(stdout, "         -v|--verbose: Logs to stdout.\n");
  fprintf(stdout, "\n");
  fprintf(stdout, "The program instantiates a RPC client and sends out a typed request to the named service, etc.\n");
  fprintf(stdout, "\n");

  return;

};

void signal_handler (int signal)
{

  log_info("Received signal '%d' to terminate", signal);
  _terminate = true;

};

int main (int argc, char** argv) 
{

  using namespace ::ccs::types;
  using namespace ::ccs::base;

  // Install signal handler to support graceful termination
  sigset(SIGTERM, signal_handler);
  sigset(SIGINT,  signal_handler);
  sigset(SIGHUP,  signal_handler);

  char service [STRING_MAX_LENGTH] = STRING_UNDEFINED; ccs::HelperTools::SafeStringCopy(service, DEFAULT_SERVICE_NAME, MaxStringLength);
  char type [1024] = DEFAULT_REQUEST_TYPE;
  char inst [1024] = DEFAULT_REQUEST_INST;

  if (argc > 1)
    {
      for (uint_t index = 1; index < (uint_t) argc; index++)
	{
          if ((strcmp(argv[index], "-h") == 0) || (strcmp(argv[index], "--help") == 0))
	    {
	      // Display usage
	      print_usage();
	      return (0);
	    }
	  else if ((strcmp(argv[index], "-t") == 0) || (strcmp(argv[index], "--type") == 0))
	    {
	      if ((index + 1) < (uint_t) argc) 
		{
		  ccs::HelperTools::SafeStringCopy(type, argv[index + 1], 1024u);
		}
	      else { print_usage(); return (0); } // Display usage
	      index += 1;
            
	    }
	  else if ((strcmp(argv[index], "-i") == 0) || (strcmp(argv[index], "--instance") == 0))
	    {
	      if ((index + 1) < (uint_t) argc) 
		{
		  ccs::HelperTools::SafeStringCopy(inst, argv[index + 1], 1024u);
		}
	      else { print_usage(); return (0); } // Display usage
	      index += 1;
            
	    }
	  else if ((strcmp(argv[index], "-s") == 0) || (strcmp(argv[index], "--service") == 0))
	    {
	      if ((index + 1) < (uint_t) argc) ccs::HelperTools::SafeStringCopy(service, argv[index + 1], MaxStringLength);
	      else { print_usage(); return (0); } // Display usage
	      index += 1;
            
	    }
	  else if ((strcmp(argv[index], "-v") == 0) || (strcmp(argv[index], "--verbose") == 0))
	    {
	      // Log to stdout
	      ccs::log::Func_t old_plugin = ccs::log::SetStdout();
	      ccs::log::Severity_t old_filter = ccs::log::SetFilter(LOG_DEBUG);

	    }
	}
    }
  else
    {
    }

  // Create ConfigurationLoader instance
  log_info("Create RPC client on '%s' service", service);
  RPCClient client (service); ::ccs::HelperTools::SleepFor(500000000ul);

  // Connect to RPC server
  bool status = client.Launch();

  if (status)
    {
      // Connect to RPC server
      status = client.IsConnected();

      if (!status)
	{
	  log_error("Unable to connect to '%s'", service);
	}
    }

  AnyValue request (type);

  if (status)
    {
      status = static_cast<bool>(request.GetType());

      if (!status)
	{
	  log_error("Unable to parse type '%s'", type);
	}
    }

  if (status)
    {
      status = request.ParseInstance(inst);

      if (!status)
	{
	  log_error("Unable to parse instance '%s'", inst);
	}
    }

  if (status)
    {
      log_info("Send request ..");
      AnyValue reply = client.SendRequest(request);
      char buffer [1024]; reply.SerialiseInstance(buffer, 1024u);
      log_info("..  reply '%s'", buffer);
    }

  if (!status)
    {
      log_error("Failure");
    }

  // Terminate
  log_info("Terminate program");

  return (0);

}
