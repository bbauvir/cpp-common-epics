/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/branches/codac-core-6.2/src/test/c++/rpc-client/rpc-client.cpp $
* $Id: rpc-client.cpp 105742 2020-01-17 13:37:49Z bauvirb $
*
* Project	: CODAC Core System
*
* Description	: SDN Software - Communication API - Simple examples
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2019 ITER Organization,
*				  CS 90 046
*				  13067 St. Paul-lez-Durance Cedex
*				  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <stdio.h> // sscanf, printf, etc.
#include <string.h> // strncpy, etc.
#include <stdarg.h> // va_start, etc.
#include <signal.h> // sigset, etc.

#include <new> // std::nothrow

#include <common/TimeTools.h>

// Local header files

#include "PVAccessServer.h"

// Constants

#define DEFAULT_RECORD_NAME "pva-one-pps"
#define DEFAULT_RECORD_TYPE "{\"type\":\"_1PPSStruct_t\", \"attributes\":[{\"time\":{\"type\":\"uint64\",\"size\":8}},{\"value\":{\"type\":\"uint64\",\"size\":8}}]}"
#define DEFAULT_RECORD_INST "{\"time\":0,\"value\":0}"

// Type definition

// Global variables

bool _terminate = false;

// Function definition

void signal_handler (int signal)
{

  _terminate = true;

};

int main (int argc, char** argv) 
{

  // Install signal handler to support graceful termination
  sigset(SIGTERM, signal_handler);
  sigset(SIGINT,  signal_handler);
  sigset(SIGHUP,  signal_handler);

  ccs::types::string name = DEFAULT_RECORD_NAME;

  ccs::base::PVAccessServer server;
  server.AddVariable(DEFAULT_RECORD_NAME, ccs::types::OutputVariable, DEFAULT_RECORD_TYPE);
  server.Launch();

  bool status = true;

  while (status && (false == _terminate))
    {
      uint64_t curr_time = ::ccs::HelperTools::SleepFor(1000000000ul);
      status = (ccs::HelperTools::SetAttributeValue(server.GetVariable(DEFAULT_RECORD_NAME), "time", curr_time) &&
		server.UpdateVariable(DEFAULT_RECORD_NAME));
    }

  // Terminate

  return (0);

}
