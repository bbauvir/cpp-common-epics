/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/main/c++/pvxs/PVAccessMonitorImpl.h $
* $Id: PVAccessMonitorImpl.h 114214 2020-10-27 10:53:01Z bauvirb $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2019 ITER Organization,
*                 CS 90 046
*                 13067 St. Paul-lez-Durance Cedex
*                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

/**
 * @file PVAccessMonitorImpl.h
 * @brief Header file for PVAccessMonitor implementation class.
 * @date 01/07/2020 (porting to PVXS)
 * @author Bertrand Bauvir (IO)
 * @copyright 2010-2017 ITER Organization
 * @detail This header file contains the definition of the PVAccessMonitor implementation class.
 */

#ifndef _PVAccessMonitorImpl_h_
#define _PVAccessMonitorImpl_h_

// Global header files

#include <functional> // std::function
#include <memory> // std::shared_ptr

#include <pvxs/data.h>
#include <pvxs/client.h>

#include <common/BasicTypes.h> // Global type definition

#include <common/SharedReference.h>

#include <common/AnyValue.h> // Variable with introspectable data type ..
#include <common/AnyValueHelper.h> // .. associated helper routines

#include <common/PVMonitorImpl.h> // Base class definition
#include <common/PVMonitorImplFactory.h> // Transport plug-in factory

// Local header files

#include "PVAccessMonitor.h"

// Constants

// Type definition

namespace ccs {

namespace base {

/**
 * @brief Implementation class providing support for PVA client channel monitor.
 * @detail The base class is instantiated with the name of the PVA channel.
 * The base class constructor and destructor take care of PVA channel
 * life-cycle management.
 */

class PVAccessMonitorImpl : public PVMonitorImpl
{

  private:

    bool _initialised;
    bool _connected;

    ::ccs::base::SharedReference<pvxs::client::Context> _context;
    std::shared_ptr<pvxs::client::Subscription> _sub;
#if 0
    ::ccs::base::SharedReference<const ::ccs::types::CompoundType> _type;
    ::ccs::base::SharedReference<::ccs::types::AnyValue> _value;
#endif
    void MonitorEvent (pvxs::client::Subscription& sub); // In lieu of lambda 

  protected:

  public:

    // Initialiser methods
    virtual bool Initialise (void); // Specialises virtual method

    // Accessor methods
    virtual bool IsConnected (void) const; // Specialises virtual method

    // Constructor methods
    PVAccessMonitorImpl (void);

    // Destructor method
    virtual ~PVAccessMonitorImpl (void);

};

// Global variables

// Function declaration

// Function definition

} // namespace base

} // namespace ccs

#endif // _PVAccessMonitorImpl_h_

