/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/main/c++/pvxs/PVAccessMonitor.h $
* $Id: PVAccessMonitor.h 111795 2020-07-20 06:38:56Z bauvirb $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2019 ITER Organization,
*                 CS 90 046
*                 13067 St. Paul-lez-Durance Cedex
*                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

/**
 * @file PVAccessMonitor.h
 * @brief Header file for PVAccessMonitor helper routines.
 * @date 01/07/2020 (porting to PVXS)
 * @author Bertrand Bauvir (IO)
 * @copyright 2010-2017 ITER Organization
 * @detail This header file contains the definition of the PVAccessMonitor helper routines.
 */

#ifndef _PVAccessMonitor_h_
#define _PVAccessMonitor_h_

// Global header files

#include <common/PVMonitorImplFactory.h> // Transport plug-in factory

// Local header files

// Constants

#define PVAccessMonitor_Plugin_Name "ccs::base::PVAccessMonitor"

// Type definition

// Global variables

// Function declaration

// Function definition

#endif // _PVAccessMonitor_h_

