/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/main/c++/pvxs/PVAccessClient.cpp $
* $Id: PVAccessClient.cpp 111307 2020-07-08 07:45:00Z bauvirb $
*
* Project	: CODAC Core System
*
* Description	: Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2019 ITER Organization,
*		  CS 90 046
*		  13067 St. Paul-lez-Durance Cedex
*		  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <new> // std::nothrow

#include <common/BasicTypes.h> // Global type definition
#include <common/StringTools.h>

//#define LOG_TRACE_ENABLE
//#undef LOG_TRACE_ENABLE
//#define LOG_DEBUG_ENABLE
//#undef LOG_DEBUG_ENABLE
#include <common/log-api.h> // Syslog wrapper routines

#include <common/LookUpTable.h> // Look-up table class definition, etc.

#include <common/AnyThread.h> // Thread management class

#include <common/AnyValue.h> // Variable with introspectable data type ..
#include <common/AnyValueHelper.h> // .. associated helper routines

// Local header files

#include "PVAccessTypes.h"

#include "PVAccessClient.h" // This class definition
#include "PVAccessClientImpl.h"

// Constants

#undef LOG_ALTERN_SRC
#define LOG_ALTERN_SRC "pvxs-client"

#ifndef log_trace
#define log_trace(arg_msg...) {}
#endif

// Type definition

namespace ccs {

namespace base {

// Global variables

namespace PVAccessInterface {

PVAccessClient* __pvac_if = static_cast<PVAccessClient*>(NULL);

} // namespace PVAccessInterface

// Function declaration

// Function definition

// Initialiser methods
 
bool PVAccessClient::AddVariable (const char* name, ccs::types::DirIdentifier direction)
{
  return __impl->AddVariable(name, direction);
}

bool PVAccessClient::SetPeriod (ccs::types::uint64 period) { return __impl->SetPeriod(period); }

bool PVAccessClient::Launch (void) { return __impl->Launch(); }

// Accessor methods

bool PVAccessClient::IsValid (const char* name) const { return __impl->IsValid(name); }

bool PVAccessClient::IsConnected (const char* name) const { return __impl->IsConnected(name); }

ccs::types::AnyValue* PVAccessClient::GetVariable (const char* name) const { return __impl->GetVariable(name); }

bool PVAccessClient::UpdateVariable (const char* name) { return __impl->UpdateVariable(name); }

bool PVAccessClient::SetCallback (const char* name, const std::function<void(const ccs::types::AnyValue&)>& cb) { return __impl->SetCallback(name, cb); }

// Constructor methods

PVAccessClient::PVAccessClient (void)
{ 

  // Instantiate implementation class
  __impl = new (std::nothrow) PVAccessClientImpl;

  bool status = (NULL_PTR_CAST(PVAccessClientImpl*) != __impl);

  if (!status)
    {
      log_error("Unable to instantiate mplementation class");
    }

  return; 

}

// Destructor method

PVAccessClient::~PVAccessClient (void)
{ 

  if (NULL_PTR_CAST(PVAccessClientImpl*) != __impl)
    { 
      delete __impl; 
    }

  __impl= NULL_PTR_CAST(PVAccessClientImpl*);

  return;

}

} // namespace base

} // namespace ccs

#undef LOG_ALTERN_SRC
