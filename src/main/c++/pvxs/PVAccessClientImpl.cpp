/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/main/c++/pvxs/PVAccessClientImpl.cpp $
* $Id: PVAccessClientImpl.cpp 114049 2020-10-20 06:28:48Z bauvirb $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2019 ITER Organization,
*                 CS 90 046
*                 13067 St. Paul-lez-Durance Cedex
*                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <new> // std::nothrow

#include <common/BasicTypes.h> // Global type definition
#include <common/StringTools.h> // Misc. helper functions, e.g. hash, etc.

//#define LOG_TRACE_ENABLE
//#undef LOG_TRACE_ENABLE
//#define LOG_DEBUG_ENABLE
//#undef LOG_DEBUG_ENABLE
#include <common/log-api.h> // Syslog wrapper routines

#include <common/LookUpTable.h> // Look-up table class definition, etc.

#include <common/AnyObject.h> // Abstract base class definition ..
#include <common/ObjectDatabase.h> // .. associated object database

#include <common/AnyThread.h> // Thread management class

#include <common/AnyValue.h> // Variable with introspectable data type ..
#include <common/AnyValueHelper.h> // .. associated helper routines

// Local header files

#include "PVXSValueHelper.h"

#include "PVAccessMonitorImpl.h"

#include "PVAccessTypes.h"

#include "PVAccessClientImpl.h" // This class definition

// Constants

#define DEFAULT_PVAINTERFACE_THREAD_PERIOD 10000000ul // 100Hz
#define DEFAULT_PVAINTERFACE_INSTANCE_NAME "pvxs-client"

#undef LOG_ALTERN_SRC
#define LOG_ALTERN_SRC "pvxs-client"

#ifndef log_trace
#define log_trace(arg_msg...) {}
#endif

// Type definition

namespace ccs {

namespace base {

// Global variables

// Function declaration

// Function definition

MonitorCallback::MonitorCallback (const ::ccs::types::char8 * const channel, PVAccessClientImpl * const client) : PVAccessMonitorImpl()
{ 

  bool status = (!ccs::HelperTools::IsUndefinedString(channel) &&
                 (static_cast<PVAccessClientImpl*>(NULL) != client));

  if (status)
    {
      using namespace std::placeholders;
      status = PVAccessMonitorImpl::RegisterEventHandler(std::bind(&MonitorCallback::HandleEvent, this, _1));
    }

  if (status)
    {
      using namespace std::placeholders;
      status = PVAccessMonitorImpl::RegisterDataHandler(std::bind(&MonitorCallback::HandleMonitor, this, _1));
    }

  if (status)
    {
      status = PVAccessMonitorImpl::SetChannel(channel);
    }

  if (status)
    {
      status = PVAccessMonitorImpl::Initialise();
    }

  if (status)
    {
      __impl = client;
    }

}

// cppcheck-suppress unusedFunction // Callbacks associated to ccs::base::PVAccessMonitor
void MonitorCallback::HandleEvent (const PVMonitor::Event& event)
{

  bool status = (__impl->__var_list).IsValid(PVAccessMonitorImpl::GetChannel());

  ccs::base::PVAccessClientImpl::VariableInfo_t varInfo; 

  if (status)
    {
      (__impl->__var_list).AcquireLock();
      (__impl->__var_list).GetElement(PVAccessMonitorImpl::GetChannel(), varInfo);

      // Update status
      varInfo.connected = (PVMonitor::Event::Connect == event);

      (__impl->__var_list).SetElement(PVAccessMonitorImpl::GetChannel(), varInfo);
      (__impl->__var_list).ReleaseLock();
    }

  return;

}

// cppcheck-suppress unusedFunction // Callbacks associated to ccs::base::PVAccessMonitor
void MonitorCallback::HandleMonitor (const ccs::types::AnyValue& value)
{

  bool status = (__impl->__var_list).IsValid(PVAccessMonitorImpl::GetChannel());
#ifdef LOG_DEBUG_ENABLE
  if (status)
    {
      char buffer [1024];
      log_debug("MonitorCallback::HandleMonitor - Channel '%s' ..", PVAccessMonitorImpl::GetChannel());
      value.SerialiseType(buffer, 1024u);
      log_debug(".. type '%s' ..", buffer);
      value.SerialiseInstance(buffer, 1024u);
      log_debug(".. value '%s'", buffer);
    }
#endif
  ccs::base::PVAccessClientImpl::VariableInfo_t varInfo; 

  if (status)
    {
      (__impl->__var_list).AcquireLock();
      (__impl->__var_list).GetElement(PVAccessMonitorImpl::GetChannel(), varInfo);

      // Update variable cache
      *(varInfo.value) = value;

      (__impl->__var_list).SetElement(PVAccessMonitorImpl::GetChannel(), varInfo);
      (__impl->__var_list).ReleaseLock();
    }

  if (status && (NULL != varInfo.cb))
    { // Call handler method
      varInfo.cb(value);
    }

  return;

}

// Initialiser methods
 
bool PVAccessClientImpl::AddVariable (const ::ccs::types::char8 * const name, ccs::types::DirIdentifier direction)
{

  bool status = (false == __var_list.IsValid(name));

  VariableInfo_t varInfo;

  varInfo.direction = direction;
  ccs::HelperTools::SafeStringCopy(varInfo.name, name, sizeof(ccs::types::string));

  if (status)
    {
      varInfo.value = ::ccs::base::SharedReference<ccs::types::AnyValue>(new (std::nothrow) ccs::types::AnyValue ());

      __var_list.AcquireLock();

      // Create monitor for all variables .. 
      // .. handles connection state and updates cache
      // .. lock required to ensure the first callbacks would be delayed till variable registered
      varInfo.monitor = new (std::nothrow) MonitorCallback (name, this);
      status = (static_cast<MonitorCallback*>(NULL) != varInfo.monitor);

      if (status)
        {
          //      varInfo.channel = (varInfo.monitor)->GetChannelHandle();
          status = __var_list.Register(name, varInfo);
        }

      __var_list.ReleaseLock();
    }


  return status;

}

void PVAccessClientImpl::Initialise (void) 
{ 

  // Initialise resources
  __client = ccs::base::PVAccessClientContext::GetInstance();
  __sleep = DEFAULT_PVAINTERFACE_THREAD_PERIOD;
  __initialised = false;

  SetPeriod(__sleep);
  SetAccuracy(__sleep);

  return;

}

// Accessor methods

bool PVAccessClientImpl::IsValid (const ::ccs::types::char8 * const name) const { return __var_list.IsValid(name); }

bool PVAccessClientImpl::IsConnected (const ::ccs::types::char8 * const name) const
{ 

  bool status = this->IsValid(name); 

  VariableInfo_t varInfo; 

  if (status) 
    { 
      __var_list.GetElement(name, varInfo); 
      status = varInfo.connected;
    }
#ifdef LOG_DEBUG_ENABLE
  if (status)
    {
      log_debug("PVAccessClientImpl::IsConnected('%s') - Seems so ..", name);
    }
  else
    {
      log_debug("PVAccessClientImpl::IsConnected('%s') - Apparently not ..", name);
    }
#endif
  return status; 

}

ccs::types::AnyValue* PVAccessClientImpl::GetVariable (const ::ccs::types::char8 * const name) const
{ 

  bool status = this->IsValid(name); 

  ccs::types::AnyValue* var = NULL_PTR_CAST(ccs::types::AnyValue*); 

  VariableInfo_t varInfo; 

  if (status) 
    { 
      __var_list.GetElement(name, varInfo);
      status = static_cast<bool>(varInfo.value);
    }

  if (status)
    {
      var = varInfo.value.GetReference();
    }

  return var; 

}

bool PVAccessClientImpl::UpdateVariable (const ::ccs::types::char8 * const name)
{ 

  bool status = this->IsValid(name); 

  VariableInfo_t varInfo; 

  if (status) 
    { 
      __var_list.GetElement(name, varInfo); 
      varInfo.update = true; 
      status = __var_list.SetElement(name, varInfo); 
    }

  return status; 

}

bool PVAccessClientImpl::SetCallback (const ::ccs::types::char8 * const name, const std::function<void(const ccs::types::AnyValue&)>& cb)
{

  bool status = this->IsValid(name); 

  VariableInfo_t varInfo; 

  if (status) 
    { 
      __var_list.GetElement(name, varInfo); 
      varInfo.cb = cb; 
      status = __var_list.SetElement(name, varInfo); 
    } 

  return status; 

}

// Miscellaneous methods

// cppcheck-suppress unusedFunction // Callbacks associated to ccs::base::AnyThread
void PVAccessClientImpl::Opening (void)
{

  __initialised = true;

}

// cppcheck-suppress unusedFunction // Callbacks associated to ccs::base::AnyThread
void PVAccessClientImpl::Execute (void)
{

  bool status = __initialised;

  for (::ccs::types::uint32 index = 0; (status && (index < __var_list.GetSize())); index++)
    {
      VariableInfo_t varInfo;

      (void)__var_list.GetElement(index, varInfo);

      if ((varInfo.direction != ::ccs::types::InputVariable) && varInfo.update)
        {
          log_debug("Update PVA record '%s'", varInfo.name);
          if (status)
            {
              try
                {
                  pvxs::Value result = __client->put(varInfo.name)
                                               .build([&varInfo](pvxs::Value&& proto)
                                                 {
                                                   pvxs::Value _value; // Placeholder
                                                   
                                                   (void)ccs::HelperTools::SerialiseAnyValueToPVXSValue(*(varInfo.value), _value);
                                                   log_debug("PVAccessClientImpl::BuildCallback - Build request with ..");
#ifdef LOG_DEBUG_ENABLE
                                                   std::cout << _value << std::endl;
#endif
                                                   return _value;
                                                 })
                                           .exec()
                                           ->wait();
                }
              catch (const std::exception& e)
                {
                  log_error("(background thread) - Caught '%s' exception", e.what());
                }
            }

          varInfo.update = false;

          // Store for future use
          (void)__var_list.SetElement(index, varInfo);
        }
    }

  return;

}

// cppcheck-suppress unusedFunction // Callbacks associated to ccs::base::AnyThread
void PVAccessClientImpl::Closing (void)
{

  __initialised = false;

}

// Constructor methods

// Destructor method

PVAccessClientImpl::PVAccessClientImpl (void) : ::ccs::base::SynchronisedThread("PVA Interface cache")
{ 

  // Initialise resources
  Initialise();

  // Register PVAccess variable types
  PVAccessTypes::Initialise();

  // Register instance in object database - Necessary for globally scoped accessors
  ::ccs::base::AnyObject* ref = (::ccs::base::AnyObject*) this; ::ccs::base::GlobalObjectDatabase::Register(DEFAULT_PVAINTERFACE_INSTANCE_NAME, ref); 

  return;

}

// Destructor method

PVAccessClientImpl::~PVAccessClientImpl (void)
{ 

  // Explicitly stop background thread .. inheritance means the virtual method are already gone when the AnyThread is deleted 
  (void)Terminate();

  // Remove instance from object database
  ::ccs::base::GlobalObjectDatabase::Remove(DEFAULT_PVAINTERFACE_INSTANCE_NAME); 

  for (::ccs::types::uint32 index = 0u; index < __var_list.GetSize(); index += 1u)
    {
      VariableInfo_t varInfo; 

      __var_list.GetElement(index, varInfo);

      log_info("Delete monitor for '%s'", varInfo.name);
      delete varInfo.monitor;
    }

  // Clear variable cache
  __var_list.Remove();

  // Tear-down client context
  __client.Discard(); // Forget about it locally ..
  (void)::ccs::base::PVAccessClientContext::Terminate(); // .. and globally if this is the last reference

  return;

}

} // namespace base

} // namespace ccs

#undef LOG_ALTERN_SRC
