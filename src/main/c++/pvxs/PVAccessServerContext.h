/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/main/c++/pvxs/PVAccessServerContext.h $
* $Id: PVAccessServerContext.h 111336 2020-07-09 05:50:42Z bauvirb $
*
* Project	: CODAC Core System
*
* Description	: Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2019 ITER Organization,
*		  CS 90 046
*		  13067 St. Paul-lez-Durance Cedex
*		  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

/**
 * @file PVAccessServerContext.h
 * @brief Header file for PVAccessServerContext singleton.
 * @date 20/06/2020
 * @author Bertrand Bauvir (IO)
 * @copyright 2010-2020 ITER Organization
 * @detail This header file contains the definition of the PVAccessServerContext singleton.
 */

#ifndef _PVAccessServerContext_h_
#define _PVAccessServerContext_h_

// Global header files

#include <pvxs/server.h>

#include <common/BasicTypes.h> // Global type definition
#include <common/SharedReference.h>

// Local header files

// Constants

// Type definition

namespace ccs {

namespace base {

// Global variables

// Function declaration

namespace PVAccessServerContext {

::ccs::base::SharedReference<pvxs::server::Server> GetInstance (void); // Issue with static initialisation and loading shared library objects
bool SetInstance (::ccs::base::SharedReference<pvxs::server::Server>& context); // In case default configuration is not appropriate
bool IsInitialised (void);
bool Start (void);
bool Stop (void);
bool Terminate (void); // Tear-down

// Function definition

} // namespace PVAccessServerContext

} // namespace base

} // namespace ccs

#endif // _PVAccessServerContext_h_

