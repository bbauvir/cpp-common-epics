/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/main/c++/pvxs/PVAccessServerContext.cpp $
* $Id: PVAccessServerContext.cpp 111336 2020-07-09 05:50:42Z bauvirb $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2019 ITER Organization,
*                 CS 90 046
*                 13067 St. Paul-lez-Durance Cedex
*                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <new> // std::nothrow

#include <pvxs/server.h>

#include <common/BasicTypes.h> // Global type definition

//#define LOG_TRACE_ENABLE
//#undef LOG_TRACE_ENABLE
//#define LOG_DEBUG_ENABLE
//#undef LOG_DEBUG_ENABLE
#include <common/log-api.h> // Syslog wrapper routines

#include <common/SharedReference.h>

// Local header files

#include "PVAccessServerContext.h"

// Constants

#undef LOG_ALTERN_SRC
#define LOG_ALTERN_SRC "pvxs-server"

// Type definition

namespace ccs {

namespace base {

// Global variables

namespace PVAccessServerContext {

static ::ccs::base::SharedReference<pvxs::server::Server> __ctxt;
static bool __started = false;

// Function declaration

// Function definition

::ccs::base::SharedReference<pvxs::server::Server> GetInstance (void)
{

  bool status = static_cast<bool>(__ctxt);

  if (!status)
    {
      __ctxt = ::ccs::base::SharedReference<pvxs::server::Server>(new (std::nothrow) pvxs::server::Server (pvxs::server::Config::from_env().build()));
      //status = static_cast<bool>(__ctxt); // Unused
    }

  return __ctxt;

}

// cppcheck-suppress unusedFunction // Not yet part of unit tests
bool SetInstance (::ccs::base::SharedReference<pvxs::server::Server>& context)
{

  bool status = (false == static_cast<bool>(__ctxt));

  if (!status)
    {
      log_notice("PVAccessServerContext::SetInstance - Pre-existing server context ..");
      __ctxt.Discard();
      status = (false == static_cast<bool>(__ctxt));
    }

  if (status)
    {
      __ctxt = context;
    }

  return status;

}

bool IsInitialised (void)
{

  bool status = static_cast<bool>(__ctxt);

  return status;

}

bool Start (void)
{

  bool status = (static_cast<bool>(__ctxt) && (false == __started));

  if (status)
    {
      __ctxt->start();
      __started = true;
    }

  return status;

}

bool Stop (void)
{

  bool status = (static_cast<bool>(__ctxt) && __started);

  if (status)
    {
      __ctxt->stop();
      __started = false;
    }

  return status;

}

bool Terminate (void)
{

  bool status = static_cast<bool>(__ctxt);

  if (status)
    { // Assume destroying the reference is sufficient for context tear-down
      __ctxt.Discard();
      status = (false == static_cast<bool>(__ctxt));
    }

  return status;

}

// Accessor methods

// Constructor methods

// Destructor method

} // namespace PVAccessServerContext

} // namespace base

} // namespace ccs

#undef LOG_ALTERN_SRC
