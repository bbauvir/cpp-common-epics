/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/main/c++/pvxs/PVAccessServerImpl.h $
* $Id: PVAccessServerImpl.h 111303 2020-07-08 05:58:03Z bauvirb $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2019 ITER Organization,
*                 CS 90 046
*                 13067 St. Paul-lez-Durance Cedex
*                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

/**
 * @file PVAccessServerImpl.h
 * @brief Header file for PVAccessServerImpl class.
 * @date 01/07/2020 (porting to PVXS)
 * @author Bertrand Bauvir (IO)
 * @copyright 2010-2020 ITER Organization
 * @detail This header file contains the definition of the PVAccessServerImpl class.
 */

#ifndef _PVAccessServerImpl_h_
#define _PVAccessServerImpl_h_

// Global header files

#include <functional> // std::function

#include <pvxs/server.h>
#include <pvxs/sharedpv.h>

#include <common/BasicTypes.h> // Global type definition

#include <common/AnyObject.h>

#include <common/AnyThread.h>

#include <common/AnyValue.h> // Variable with introspectable data type ..
#include <common/AnyValueHelper.h> // .. associated helper routines

// Local header files

// Constants

// Type definition

namespace ccs {

namespace base {

class PVAccessServerImpl : public AnyObject, public SynchronisedThread
{

  private:

    ::ccs::base::SharedReference<pvxs::server::Server> __server;

    ::ccs::types::uint64 __sleep;
    bool __initialised;

    typedef struct VariableInfo {

      bool update = false;
      ::ccs::types::DirIdentifier direction;

      std::function<void(const ::ccs::types::AnyValue&)> cb = NULL;

      ::ccs::types::string name = "";
      ::ccs::base::SharedReference<::ccs::types::AnyValue> value; // Introspectable structured variable
      
      pvxs::server::SharedPV pvrecord;
      pvxs::Value pvvalue;
      
    } VariableInfo_t;

    ::ccs::base::LookUpTable<VariableInfo_t> __var_list;

    // Initialiser methods
    void Initialise (void);

    // Miscellaneous methods
    void PutCallback (pvxs::server::SharedPV& pv, std::unique_ptr<pvxs::server::ExecOp>&& op, pvxs::Value&& value);

  protected:

  public:

    // Initialiser methods
    bool AddVariable (const ::ccs::types::char8 * const name, ::ccs::types::DirIdentifier direction, const ::ccs::base::SharedReference<const ::ccs::types::AnyType>& type);

    // Accessor methods
    bool IsValid (const ::ccs::types::char8* name) const;

    ::ccs::types::AnyValue* GetVariable (const ::ccs::types::char8* name) const;

    bool UpdateVariable (const ::ccs::types::char8 * const name);

    bool SetCallback (const ::ccs::types::char8 * const name, const std::function<void(const ::ccs::types::AnyValue&)>& cb);

    // Miscellaneous methods
    virtual void Opening (void); // Specialises SynchronisedThread virtual method
    virtual void Execute (void); // Provides virtual method implementation
    virtual void Closing (void); // Specialises SynchronisedThread virtual method

    // Constructor methods
    PVAccessServerImpl (void);

    // Destructor method
    virtual ~PVAccessServerImpl (void);

};

// Global variables

// Function declaration

// Function definition

} // namespace base

} // namespace ccs

#endif // _PVAccessServerImpl_h_

