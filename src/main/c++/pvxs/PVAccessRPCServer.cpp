/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/main/c++/pvxs/PVAccessRPCServer.cpp $
* $Id: PVAccessRPCServer.cpp 111795 2020-07-20 06:38:56Z bauvirb $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2019 ITER Organization,
*                 CS 90 046
*                 13067 St. Paul-lez-Durance Cedex
*                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <memory> // std::shared_ptr
#include <new> // std::nothrow
#include <functional> // std::function

#include <pvxs/server.h>
#include <pvxs/sharedpv.h>

#include <common/BasicTypes.h> // Global type definition
#include <common/SysTools.h> // Misc. helper functions

//#define LOG_TRACE_ENABLE
//#undef LOG_TRACE_ENABLE
//#define LOG_DEBUG_ENABLE
//#undef LOG_DEBUG_ENABLE
#include <common/log-api.h> // Syslog wrapper routines

#include <common/AnyType.h> // Introspectable data type ..
#include <common/AnyTypeHelper.h> // .. associated helper routines

#include <common/AnyValue.h> // Variable with introspectable data type ..
#include <common/AnyValueHelper.h> // .. associated helper routines

#include <common/RPCServerImpl.h> 
#include <common/RPCServerImplFactory.h> 

#include <common/RPCTypes.h>

// Local header files

#include "PVXSValueHelper.h" // .. associated helper routines

#include "PVAccessServerContext.h" // The singleton server
#include "PVAccessRPCServer.h" // This class definition

// Constants

#undef LOG_ALTERN_SRC
#define LOG_ALTERN_SRC "pvxs-server"

// Type definition

namespace ccs {

namespace base {

// Function declaration

RPCServerImpl* PVAccessRPCServerConstructor (void);

// Global variables

static bool __is_registered = (RPCServerImplFactory::GetInstance()->Register("ccs::base::PVAccessRPCServer", &PVAccessRPCServerConstructor) &&
                               RPCServerImplFactory::GetInstance()->SetDefault("ccs::base::PVAccessRPCServer"));

// Function definition

RPCServerImpl* PVAccessRPCServerConstructor (void) { return dynamic_cast<RPCServerImpl*>(new (std::nothrow) PVAccessRPCServer ()); }

bool PVAccessRPCServer::Initialise (void)
{ 

  __server = ::ccs::base::PVAccessServerContext::GetInstance(); // Singleton managed externally

  bool status = static_cast<bool>(__server);

  if (status)
    {
      __pv = pvxs::server::SharedPV(pvxs::server::SharedPV::buildMailbox());
      __server->addPV(this->GetService(), __pv);

      // Install callback .. as lambda expression
      __pv.onRPC([this](pvxs::server::SharedPV& pv, std::unique_ptr<pvxs::server::ExecOp>&& op, pvxs::Value&& request) 
        {

          ccs::types::AnyValue _request;

          bool status = ccs::HelperTools::SerialiseAnyValueFromPVXSValue(_request, request);

          ccs::types::AnyValue _reply;
          pvxs::Value reply;

          if (status)
            {
              log_debug("PVAccessRPCServer::[]() - Calling registered handler ..");
              _reply = CallHandler(_request);
              log_debug("PVAccessRPCServer::[]() - .. returned");
              status = ccs::HelperTools::SerialiseAnyValueToPVXSValue(_reply, reply);
            }

          if (status)
            {
              op->reply(reply);
            }

          return;

        });

      __initialised = true; 
    }

  return status; 

}

bool PVAccessRPCServer::Launch (void)
{ 

  if (true != __initialised)
    {
      this->Initialise(); 
    }

  (void)::ccs::base::PVAccessServerContext::Start(); // I.e. ignore if already started .. use-case RPC and variable servers in the same process

  return true; 

}

bool PVAccessRPCServer::Terminate (void) { return ::ccs::base::PVAccessServerContext::Stop(); }

PVAccessRPCServer::PVAccessRPCServer (void) : RPCServerImpl() { __initialised = false; }
PVAccessRPCServer::~PVAccessRPCServer (void) {}

} // namespace base

} // namespace ccs

#undef LOG_ALTERN_SRC
