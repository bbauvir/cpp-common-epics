/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/main/c++/pvxs/PVAccessTypes.h $
* $Id: PVAccessTypes.h 111232 2020-07-06 11:37:18Z bauvirb $
*
* Project	: CODAC Core System
*
* Description	: Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2019 ITER Organization,
*		  CS 90 046
*		  13067 St. Paul-lez-Durance Cedex
*		  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

/**
 * @file PVAccessTypes.h
 * @brief Header file for PVAccess specific types.
 * @date 05/04/2019
 * @author Bertrand Bauvir (IO)
 * @copyright 2010-2019 ITER Organization
 * @detail This header file contains the definition of the PVAccess types.
 */

#ifndef _PVAccessTypes_h_
#define _PVAccessTypes_h_

// Global header files

//#include <memory> // std::shared_ptr, etc.

#include <common/BasicTypes.h>

#include <common/SharedReference.h> // In lieu of shared_ptr

#include <common/AnyType.h> // Introspectable data type definition ..
#include <common/AnyTypeHelper.h> // .. associated helper routines

#include <common/AnyValue.h> // Introspectable data type definition ..
#include <common/AnyValueHelper.h> // .. associated helper routines

// Local header files

// Constants

#define PVAccess_Timestamp_TypeName "ccs::Timestamp_t/v1.0"
#define PVAccess_Severity_TypeName "ccs::Severity_t/v1.0"
#define PVAccess_VariableBase_TypeName "ccs::Variable_t/v1.0"

// Type definition

namespace ccs {

namespace base {

namespace PVAccessTypes {

typedef struct __attribute__((packed)) Timestamp {

  enum Standard : ccs::types::uint8 {
    Undefined = 0,
    TAI,
    UTC,
  } standard = Standard::TAI;

  ccs::types::uint64 nanosec = 0ul;
  //ccs::types::char8 iso8601 [32] = "";

} Timestamp_t;

typedef struct __attribute__((packed)) Severity {

  enum Level : ccs::types::uint8 {
    Undefined = 0,
    Normal,
    Notice,
    Minor,
    Major,
    Severe,
    Invalid,
  } level = Level::Undefined;

  ccs::types::char8 level_hr [12] = "undefined";
  ccs::types::string reason = ""; // Optional substantiating information

} Severity_t;

typedef struct __attribute__((packed)) VariableBase {

  ccs::types::uint64 counter = 0ul; // Update counter .. at source
  Timestamp_t timestamp;
  Severity_t severity;

} VariableBase_t;

template <typename Type> struct __attribute__((packed)) VariableWithValue : public VariableBase_t {

  Type value;

};

class VariableType : public ccs::types::CompoundType
{

  private:

  public:

    VariableType (const ccs::base::SharedReference<const ccs::types::AnyType>& type); // Type of the value field
    virtual ~VariableType (void);

};
#if 0
class Variable : public ccs::types::AnyValue
{

  private:

    VariableBase_t __base;

  public:

    Variable (const std::shared_ptr<const ccs::types::AnyType>& type); // Type of the value field
    virtual ~Variable (void);

    Severity::Level GetSeverity (void) const;
    bool SetSeverity (Severity::Level level);

    virtual bool Update (void);

};
#endif
// Global variables

extern ccs::base::SharedReference<const ccs::types::CompoundType> Timestamp_int; // Introspectable type definition
extern ccs::base::SharedReference<const ccs::types::CompoundType> Severity_int; // Introspectable type definition
extern ccs::base::SharedReference<const ccs::types::CompoundType> VariableBase_int; // Introspectable type definition

// Test types
extern ccs::base::SharedReference<const ::ccs::types::CompoundType> Scalars_int; // Introspectable type definition
extern ccs::base::SharedReference<const ::ccs::types::CompoundType> ScalarArrays_int; // Introspectable type definition
extern ccs::base::SharedReference<const ::ccs::types::CompoundType> Collection_int; // Introspectable type definition

// Function declaration

// WARNING - ScalarType are statically initialised but there is no way to guarantee static
// initialisation order. The above types use ScalarType as attributes and must therefore be
// dynamically initialised .. called by PVAccess classes upon initialisation.
bool Initialise (void);

// Function definition

} // namespace PVAccessTypes

} // namespace base

} // namespace ccs

#endif // _PVAccessTypes_h_

