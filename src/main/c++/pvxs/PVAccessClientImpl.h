/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/main/c++/pvxs/PVAccessClientImpl.h $
* $Id: PVAccessClientImpl.h 111795 2020-07-20 06:38:56Z bauvirb $
*
* Project	: CODAC Core System
*
* Description	: Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2019 ITER Organization,
*		  CS 90 046
*		  13067 St. Paul-lez-Durance Cedex
*		  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

/**
 * @file PVAccessClient.h
 * @brief Header file for PVAccessClient implementation class.
 * @date 01/07/2020 (porting to PVXS)
 * @author Bertrand Bauvir (IO)
 * @copyright 2010-2017 ITER Organization
 * @detail This header file contains the definition of the PVAccessClient implementation class.
 */

#ifndef _PVAccessClientImpl_h_
#define _PVAccessClientImpl_h_

// Global header files

#include <functional> // std::function

#include <common/BasicTypes.h> // Global type definition

#include <common/AnyObject.h>

#include <common/AnyThread.h> // Thread management class

#include <common/SharedReference.h>

#include <common/AnyValue.h> // Variable with introspectable data type ..
#include <common/AnyValueHelper.h> // .. associated helper routines

// Local header files

#include "PVAccessClientContext.h" // The singleton context

#include "PVAccessMonitorImpl.h"

// Constants

// Type definition

namespace ccs {

namespace base {

class PVAccessClientImpl; // Forward class declaration

struct MonitorCallback : PVAccessMonitorImpl
{

  PVAccessClientImpl* __impl = NULL_PTR_CAST(PVAccessClientImpl*);

  MonitorCallback (const ::ccs::types::char8 * const channel, PVAccessClientImpl * const client);
  virtual ~MonitorCallback (void) {};

  // Implementation of ccs::base::PVAccessMonitor interface
  virtual void HandleEvent (const PVMonitor::Event& event);
  virtual void HandleMonitor (const ccs::types::AnyValue& value);

};

class PVAccessClientImpl : public AnyObject, public SynchronisedThread
{

  private:

    ::ccs::base::SharedReference<pvxs::client::Context> __client;

    ::ccs::types::uint64 __sleep;
    bool __initialised;

    // Initialiser methods
    void Initialise (void);

  protected:

  public:

    typedef struct VariableInfo {

      bool update = false;
      ::ccs::types::DirIdentifier direction;
      
      MonitorCallback* monitor = static_cast<MonitorCallback*>(NULL);
      std::function<void(const ::ccs::types::AnyValue&)> cb = NULL;

      ::ccs::types::string name = "";
      ::ccs::base::SharedReference<::ccs::types::AnyValue> value; // Introspectable structured variable

      bool connected = false;

    } VariableInfo_t;

    ccs::base::LookUpTable<VariableInfo_t> __var_list;

    // Initialiser methods
    bool AddVariable (const ::ccs::types::char8 * const name, ccs::types::DirIdentifier direction);

    // Accessor methods
    bool IsValid (const ::ccs::types::char8 * const name) const;
    bool IsConnected (const ::ccs::types::char8 * const name) const;

    ccs::types::AnyValue* GetVariable (const ::ccs::types::char8 * const name) const;

    bool UpdateVariable (const ::ccs::types::char8 * const name);

    bool SetCallback (const ::ccs::types::char8 * const name, const std::function<void(const ccs::types::AnyValue&)>& cb);

    // Miscellaneous methods
    virtual void Opening (void); // Specialises SynchronisedThread virtual method
    virtual void Execute (void); // Provides virtual method implementation
    virtual void Closing (void); // Specialises SynchronisedThread virtual method

    // Constructor methods
    PVAccessClientImpl (void);

    // Destructor method
    virtual ~PVAccessClientImpl (void);

};

} // namespace base

} // namespace ccs

#endif // _PVAccessClientImpl_h_

