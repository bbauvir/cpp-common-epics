/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/main/c++/pvxs/PVAccessRPCServer.h $
* $Id: PVAccessRPCServer.h 111795 2020-07-20 06:38:56Z bauvirb $
*
* Project	: CODAC Core System
*
* Description	: Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2019 ITER Organization,
*		  CS 90 046
*		  13067 St. Paul-lez-Durance Cedex
*		  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

/**
 * @file PVAccessRPCServer.h
 * @brief Header file for PVAccessRPCServer class.
 * @date 01/07/2020 (porting to PVXS)
 * @author Bertrand Bauvir (IO)
 * @copyright 2010-2018 ITER Organization
 * @detail This header file contains the definition of the PVAccessRPCServer class.
 */

#ifndef _PVAccessRPCServer_h_
#define _PVAccessRPCServer_h_

// Global header files

#include <memory> // std::shared_ptr, etc.

#include <pvxs/server.h>
#include <pvxs/sharedpv.h>

#include <common/RPCServerImpl.h>

// Local header files

// Constants

// Type definition

namespace ccs {

namespace base {

/**
 * @brief Class providing implementation for PVA RPC server.
 */

class PVAccessRPCServer : public RPCServerImpl
{

  private:

    bool __initialised;

    ::ccs::base::SharedReference<pvxs::server::Server> __server;
    pvxs::server::SharedPV __pv;

    // Initialiser methods
    bool Initialise (void);

  protected:

  public:

    // Initialiser methods
    virtual bool Launch (void);
    virtual bool Terminate (void);

    // Constructor methods
    PVAccessRPCServer (void);

    // Destructor method
    virtual ~PVAccessRPCServer (void); 

};

// Global variables

// Function declaration

// Function definition

} // namespace base

} // namespace ccs

#endif // _PVAccessRPCServer_h_

