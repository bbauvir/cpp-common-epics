/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-sup-common-cpp/branches/codac-core-6.2/src/test/c++/unit/FunctionDatabase-tests.cpp $
* $Id: FunctionDatabase-tests.cpp 105560 2020-01-13 10:24:44Z kimh19 $
*
* Project       : CODAC Core System
*
* Description   : Unit test code
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2020 ITER Organization,
*                                 CS 90 046
*                                 13067 St. Paul-lez-Durance Cedex
*                                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <common/BasicTypes.h> // Misc. type definition
#include <common/SysTools.h> // Misc. helper functions

#include <common/log-api.h> // Syslog wrapper routines

#include <common/AnyValue.h>
#include <common/AnyValueHelper.h>

#include <pvxs/data.h>

// Local header files

#include "PVXSValueHelper.h"

// Constants

// Type definition

namespace ccs {

namespace HelperTools {

typedef struct {

  pvxs::TypeCode scalars;
  pvxs::TypeCode arrays;

} TypeCodes_t;

typedef struct {

  bool(* scalars)(::ccs::types::AnyValue&, const pvxs::Value&);
  bool(* arrays)(::ccs::types::AnyValue&, const pvxs::Value&, ::ccs::types::uint32);

} AssignmentFromFunctionsPtr_t;

typedef struct {

  bool(* scalars)(const ::ccs::types::AnyValue&, pvxs::Value&);
  bool(* arrays)(const ::ccs::types::AnyValue&, pvxs::Value&, ::ccs::types::uint32);

} AssignmentToFunctionsPtr_t;

typedef struct {

  AssignmentToFunctionsPtr_t to;
  AssignmentFromFunctionsPtr_t from;

} AssignmentFunctionsPtr_t;

// Function declaration

static inline pvxs::Member AnyTypeToPVMember (const ::ccs::base::SharedReference<const ::ccs::types::AnyType>& type, const ::ccs::types::char8 * const name);
static inline ::ccs::base::SharedReference<const ::ccs::types::AnyType> AnyTypeFromPVXSValue (const pvxs::Value& pv_value);

#define FUNCTION_DEFINITION(BASIC_TYPE) \
bool AssignToPVXSScalar_##BASIC_TYPE (const ::ccs::types::AnyValue& in_value, pvxs::Value& pv_value) \
{ \
\
  bool status = static_cast<bool>(pv_value); \
\
  if (status) \
    { \
      pv_value = static_cast<BASIC_TYPE>(in_value); \
    } \
\
  return status; \
\
}

FUNCTION_DEFINITION(bool)
FUNCTION_DEFINITION(int8_t)
FUNCTION_DEFINITION(uint8_t)
FUNCTION_DEFINITION(int16_t)
FUNCTION_DEFINITION(uint16_t)
FUNCTION_DEFINITION(int32_t)
FUNCTION_DEFINITION(uint32_t)
FUNCTION_DEFINITION(int64_t)
FUNCTION_DEFINITION(uint64_t)
FUNCTION_DEFINITION(float)
FUNCTION_DEFINITION(double)

#undef FUNCTION_DEFINITION

bool AssignToPVXSScalar_char8 (const ::ccs::types::AnyValue& in_value, pvxs::Value& pv_value)
{

  bool status = static_cast<bool>(pv_value);

  if (status)
    {
      pv_value = static_cast<uint8_t>(in_value);
    }

  return status;

}

bool AssignToPVXSScalar_string (const ::ccs::types::AnyValue& in_value, pvxs::Value& pv_value)
{

  bool status = static_cast<bool>(pv_value);

  if (status)
    {
      pv_value = std::string(static_cast<const ::ccs::types::char8*>(in_value.GetInstance()));
    }

  return status;

}

#define FUNCTION_DEFINITION(BASIC_TYPE) \
bool AssignToPVXSArray_##BASIC_TYPE (const ::ccs::types::AnyValue& in_value, pvxs::Value& pv_value, ::ccs::types::uint32 size) \
{ \
\
  bool status = static_cast<bool>(pv_value); \
\
  if (status) \
    { \
      pvxs::shared_array<BASIC_TYPE> _array (size); \
\
      for (::ccs::types::uint32 index = 0u; (status && (index < size)); index++) \
        { \
          _array[index] = ::ccs::HelperTools::GetElementValue<BASIC_TYPE>(&in_value, index); \
        } \
\
      pv_value = _array.freeze().castTo<const void>(); \
    } \
\
  return status; \
\
}

FUNCTION_DEFINITION(bool)
FUNCTION_DEFINITION(int8_t)
FUNCTION_DEFINITION(uint8_t)
FUNCTION_DEFINITION(int16_t)
FUNCTION_DEFINITION(uint16_t)
FUNCTION_DEFINITION(int32_t)
FUNCTION_DEFINITION(uint32_t)
FUNCTION_DEFINITION(int64_t)
FUNCTION_DEFINITION(uint64_t)
FUNCTION_DEFINITION(float)
FUNCTION_DEFINITION(double)

#undef FUNCTION_DEFINITION

bool AssignToPVXSArray_char8 (const ::ccs::types::AnyValue& in_value, pvxs::Value& pv_value, ::ccs::types::uint32 size)
{

  bool status = static_cast<bool>(pv_value);

  if (status)
    {
      pvxs::shared_array<uint8_t> _array (size);

      for (::ccs::types::uint32 index = 0u; (status && (index < size)); index++)
        {
          _array[index] = ::ccs::HelperTools::GetElementValue<::ccs::types::char8>(&in_value, index);
        }

      pv_value = _array.freeze().castTo<const void>();
    }

  return status;

}

bool AssignToPVXSArray_string (const ::ccs::types::AnyValue& in_value, pvxs::Value& pv_value, ::ccs::types::uint32 size)
{

  bool status = static_cast<bool>(pv_value);

  if (status)
    {
      pvxs::shared_array<std::string> _array (size);

      for (::ccs::types::uint32 index = 0u; (status && (index < size)); index++)
        {
          _array[index] = std::string(static_cast<const ::ccs::types::char8*>(::ccs::HelperTools::GetElementReference(&in_value, index)));
        }

      pv_value = _array.freeze().castTo<const void>();
    }

  return status;

}

#define FUNCTION_DEFINITION(BASIC_TYPE) \
bool AssignFromPVXSScalar_##BASIC_TYPE (::ccs::types::AnyValue& out_value, const pvxs::Value& pv_value) \
{ \
\
  bool status = static_cast<bool>(pv_value); \
\
  if (status) \
    { \
      out_value = pv_value.as<const BASIC_TYPE>(); \
    } \
\
  return status; \
\
}

FUNCTION_DEFINITION(bool)
FUNCTION_DEFINITION(int8_t)
FUNCTION_DEFINITION(uint8_t)
FUNCTION_DEFINITION(int16_t)
FUNCTION_DEFINITION(uint16_t)
FUNCTION_DEFINITION(int32_t)
FUNCTION_DEFINITION(uint32_t)
FUNCTION_DEFINITION(int64_t)
FUNCTION_DEFINITION(uint64_t)
FUNCTION_DEFINITION(float)
FUNCTION_DEFINITION(double)

#undef FUNCTION_DEFINITION

bool AssignFromPVXSScalar_char8 (::ccs::types::AnyValue& out_value, const pvxs::Value& pv_value)
{

  bool status = static_cast<bool>(pv_value);

  if (status)
    {
      out_value = static_cast<::ccs::types::char8>(pv_value.as<const uint8_t>());
    }

  return status;

}

bool AssignFromPVXSScalar_string (::ccs::types::AnyValue& out_value, const pvxs::Value& pv_value)
{

  bool status = static_cast<bool>(pv_value);

  if (status)
    {
      (void)::ccs::HelperTools::SafeStringCopy(static_cast<::ccs::types::char8*>(out_value.GetInstance()), pv_value.as<std::string>().c_str(), ::ccs::types::MaxStringLength);
    }

  return status;

}

#define FUNCTION_DEFINITION(BASIC_TYPE) \
bool AssignFromPVXSArray_##BASIC_TYPE (::ccs::types::AnyValue& out_value, const pvxs::Value& pv_value, ::ccs::types::uint32 size) \
{ \
\
  bool status = static_cast<bool>(pv_value); \
\
  pvxs::shared_array<const BASIC_TYPE> array; \
\
  if (status && (0u != size)) \
    { \
      array = pv_value.as<pvxs::shared_array<const BASIC_TYPE>>(); \
      status = (NULL_PTR_CAST(const BASIC_TYPE*) != array.data()); \
    } \
\
  if (status && (0u != size)) \
    { \
      (void)memcpy(out_value.GetInstance(), array.data(), size*sizeof(BASIC_TYPE)); \
    } \
\
  return status; \
\
}

FUNCTION_DEFINITION(bool)
FUNCTION_DEFINITION(int8_t)
FUNCTION_DEFINITION(uint8_t)
FUNCTION_DEFINITION(int16_t)
FUNCTION_DEFINITION(uint16_t)
FUNCTION_DEFINITION(int32_t)
FUNCTION_DEFINITION(uint32_t)
FUNCTION_DEFINITION(int64_t)
FUNCTION_DEFINITION(uint64_t)
FUNCTION_DEFINITION(float)
FUNCTION_DEFINITION(double)

#undef FUNCTION_DEFINITION

bool AssignFromPVXSArray_char8 (::ccs::types::AnyValue& out_value, const pvxs::Value& pv_value, ::ccs::types::uint32 size)
{

  bool status = static_cast<bool>(pv_value);

  pvxs::shared_array<const uint8_t> array;

  if (status && (0u != size))
    {
      array = pv_value.as<pvxs::shared_array<const uint8_t>>();
      status = (NULL_PTR_CAST(const uint8_t*) != array.data());
    }

  if (status && (0u != size))
    {
      (void)memcpy(out_value.GetInstance(), array.data(), size);
    }

  return status;

}

bool AssignFromPVXSArray_string (::ccs::types::AnyValue& out_value, const pvxs::Value& pv_value, ::ccs::types::uint32 size)
{

  bool status = static_cast<bool>(pv_value);

  pvxs::shared_array<const std::string> array;

  if (status)
    {
      array = pv_value.as<pvxs::shared_array<const std::string>>();
    }

  for (::ccs::types::uint32 index = 0u; (status && (index < size)); index++)
    {
      (void)::ccs::HelperTools::SafeStringCopy(static_cast<::ccs::types::char8*>(::ccs::HelperTools::GetElementReference(&out_value, index)), array[index].c_str(), ::ccs::types::MaxStringLength);
    }

  return status;

}

// Global variables

static ::ccs::base::NameValuePair<TypeCodes_t> __typecode_table [] = {

  { "bool", { pvxs::TypeCode::Bool, pvxs::TypeCode::BoolA } },
  { "char8", { pvxs::TypeCode::UInt8, pvxs::TypeCode::UInt8A } }, // char8 only exists for arrays
  { "int8", { pvxs::TypeCode::Int8, pvxs::TypeCode::Int8A } },
  { "uint8", { pvxs::TypeCode::UInt8, pvxs::TypeCode::UInt8A } },
  { "int16", { pvxs::TypeCode::Int16, pvxs::TypeCode::Int16A } },
  { "uint16", { pvxs::TypeCode::UInt16, pvxs::TypeCode::UInt16A } },
  { "int32", { pvxs::TypeCode::Int32, pvxs::TypeCode::Int32A } },
  { "uint32", { pvxs::TypeCode::UInt32, pvxs::TypeCode::UInt32A } },
  { "int64", { pvxs::TypeCode::Int64, pvxs::TypeCode::Int64A } },
  { "uint64", { pvxs::TypeCode::UInt64, pvxs::TypeCode::UInt64A } },
  { "float32", { pvxs::TypeCode::Float32, pvxs::TypeCode::Float32A } },
  { "float64", { pvxs::TypeCode::Float64, pvxs::TypeCode::Float64A } },
  { "string", { pvxs::TypeCode::String, pvxs::TypeCode::StringA } },

  { EOT_KEYWORD, { pvxs::TypeCode::Null, pvxs::TypeCode::Null } } // End-of-table marker

};

static ::ccs::base::LookUpTable<TypeCodes_t> __typecode_map (__typecode_table);

static ::ccs::base::NameValuePair<AssignmentFunctionsPtr_t> __function_table [] = {

  { "bool", { { &AssignToPVXSScalar_bool, &AssignToPVXSArray_bool } , { &AssignFromPVXSScalar_bool, &AssignFromPVXSArray_bool } } },
  { "char8", { { &AssignToPVXSScalar_char8, &AssignToPVXSArray_char8 } , { &AssignFromPVXSScalar_char8, &AssignFromPVXSArray_char8 } } },
  { "int8", { { &AssignToPVXSScalar_int8_t, &AssignToPVXSArray_int8_t } , { &AssignFromPVXSScalar_int8_t, &AssignFromPVXSArray_int8_t } } },
  { "uint8", { { &AssignToPVXSScalar_uint8_t, &AssignToPVXSArray_uint8_t } , { &AssignFromPVXSScalar_uint8_t, &AssignFromPVXSArray_uint8_t } } },
  { "int16", { { &AssignToPVXSScalar_int16_t, &AssignToPVXSArray_int16_t } , { &AssignFromPVXSScalar_int16_t, &AssignFromPVXSArray_int16_t } } },
  { "uint16", { { &AssignToPVXSScalar_uint16_t, &AssignToPVXSArray_uint16_t } , { &AssignFromPVXSScalar_uint16_t, &AssignFromPVXSArray_uint16_t } } },
  { "int32", { { &AssignToPVXSScalar_int32_t, &AssignToPVXSArray_int32_t } , { &AssignFromPVXSScalar_int32_t, &AssignFromPVXSArray_int32_t } } },
  { "uint32", { { &AssignToPVXSScalar_uint32_t, &AssignToPVXSArray_uint32_t } , { &AssignFromPVXSScalar_uint32_t, &AssignFromPVXSArray_uint32_t } } },
  { "int64", { { &AssignToPVXSScalar_int64_t, &AssignToPVXSArray_int64_t } , { &AssignFromPVXSScalar_int64_t, &AssignFromPVXSArray_int64_t } } },
  { "uint64", { { &AssignToPVXSScalar_uint64_t, &AssignToPVXSArray_uint64_t } , { &AssignFromPVXSScalar_uint64_t, &AssignFromPVXSArray_uint64_t } } },
  { "float32", { { &AssignToPVXSScalar_float, &AssignToPVXSArray_float } , { &AssignFromPVXSScalar_float, &AssignFromPVXSArray_float } } },
  { "float64", { { &AssignToPVXSScalar_double, &AssignToPVXSArray_double } , { &AssignFromPVXSScalar_double, &AssignFromPVXSArray_double } } },
  { "string", { { &AssignToPVXSScalar_string, &AssignToPVXSArray_string } , { &AssignFromPVXSScalar_string, &AssignFromPVXSArray_string } } },

  // WARNING - Missing string support

  { EOT_KEYWORD, { { NULL, NULL }, { NULL, NULL } } } // End-of-table marker

};

static ::ccs::base::LookUpTable<AssignmentFunctionsPtr_t> __function_map (__function_table);

// Function definition

static inline pvxs::TypeCode AnyTypeToPVTypeCode (const ::ccs::base::SharedReference<const ::ccs::types::AnyType>& type)
{

  bool status = static_cast<bool>(type);

  pvxs::TypeCode _code;

  if (status && ::ccs::HelperTools::Is<::ccs::types::ArrayType>(type))
    {
      ::ccs::base::SharedReference<const ::ccs::types::ArrayType> _type = type;

      if (::ccs::HelperTools::Is<::ccs::types::CompoundType>(_type->GetElementType()))
        {
          _code = pvxs::TypeCode::StructA;
        }
      else
        {
          _code = __typecode_map.GetElement(_type->GetElementType()->GetName()).arrays;
        }
    }
  else if (status && ::ccs::HelperTools::Is<::ccs::types::CompoundType>(type))
    {
      _code = pvxs::TypeCode::Struct;
    }
  else if (status && ::ccs::HelperTools::Is<::ccs::types::ScalarType>(type))
    {
      _code = __typecode_map.GetElement(type->GetName()).scalars;
    }

  return _code;

}

static inline bool CollectPVXSMembers (const ::ccs::base::SharedReference<const ::ccs::types::CompoundType>& type, std::vector<pvxs::Member>& children)
{

  bool status = static_cast<bool>(type);

  for (::ccs::types::uint32 index = 0u; (status && (index < type->GetAttributeNumber())); index++)
    {
      children.push_back(AnyTypeToPVMember(type->GetAttributeType(index), type->GetAttributeName(index)));
    }

  return status;

}

// Member of a compound type
static inline pvxs::Member AnyTypeToPVMember (const ::ccs::base::SharedReference<const ::ccs::types::AnyType>& type, const ::ccs::types::char8 * const name)
{

  bool status = (static_cast<bool>(type) && !::ccs::HelperTools::IsUndefinedString(name));
#ifdef LOG_DEBUG_ENABLE
  if (status)
    {
      log_debug("AnyTypeToPVMember - Method called with type '%s' and name '%s'", type->GetName(), name);
    }
#endif
  pvxs::TypeCode _code = AnyTypeToPVTypeCode(type);
  pvxs::Member _member;

  if (status && ::ccs::HelperTools::Is<::ccs::types::ArrayType>(type))
    {
      log_debug("AnyTypeToPVMember<ArrayType> - Testing element type ..");
      ::ccs::base::SharedReference<const ::ccs::types::ArrayType> _type = type;

      if (::ccs::HelperTools::Is<::ccs::types::CompoundType>(_type->GetElementType()))
        {
          log_debug("AnyTypeToPVMember<ArrayType> - .. is CompoundType");
          ::ccs::base::SharedReference<const ::ccs::types::CompoundType> __type = _type->GetElementType();
          std::vector<pvxs::Member> _children;

          status = CollectPVXSMembers(__type, _children);

          _member = pvxs::Member(_code, std::string(name), std::string(__type->GetName()), _children);
        }
      else
        {
          log_debug("AnyTypeToPVMember<ArrayType> - .. is ScalarType");
          _member = pvxs::Member(_code, std::string(name));
        }
    }
  else if (status && ::ccs::HelperTools::Is<::ccs::types::CompoundType>(type))
    {
      ::ccs::base::SharedReference<const ::ccs::types::CompoundType> _type = type;

      std::vector<pvxs::Member> _children;

      log_debug("AnyTypeToPVMember<CompoundType> - Collect children ..");
      status = CollectPVXSMembers(_type, _children);

      _member = pvxs::Member(_code, std::string(name), std::string(type->GetName()), _children);
    }
  else if (status && ::ccs::HelperTools::Is<::ccs::types::ScalarType>(type))
    {
      log_debug("AnyTypeToPVMember<ScalarType> - Create member with code '%s' ..", _code.name());
      _member =  pvxs::Member(_code, std::string(name));
    }

  return _member;

}

static inline pvxs::TypeDef AnyTypeToPVTypeDef (const ::ccs::base::SharedReference<const ::ccs::types::AnyType>& type)
{

  bool status = (static_cast<bool>(type) && ::ccs::HelperTools::Is<::ccs::types::CompoundType>(type));

  pvxs::TypeDef _type;

  // ToDo - Support for non-compound types

  if (status && ::ccs::HelperTools::Is<::ccs::types::CompoundType>(type))
    { // Create empty structure
      log_debug("AnyTypeToPVTypeDef<CompoundType> - Create empty PVXS structure ..");
      _type = pvxs::TypeDef(AnyTypeToPVTypeCode(type), type->GetName(), {});

      ::ccs::base::SharedReference<const ::ccs::types::CompoundType> __type = type;

      for (::ccs::types::uint32 index = 0u; (status && (index < __type->GetAttributeNumber())); index++)
        {
          log_debug("AnyTypeToPVTypeDef<CompoundType> - .. adding '%s' attribute of type '%s'", __type->GetAttributeName(index), __type->GetAttributeType(index)->GetName());
          _type += { AnyTypeToPVMember(__type->GetAttributeType(index), __type->GetAttributeName(index)) };
        }
    }

  return _type;

}

static inline bool AssignAnyValueToPVXSValue_ArrayType (const ::ccs::types::AnyValue& in_value, pvxs::Value& pv_value)
{

  const ::ccs::base::SharedReference<const ::ccs::types::ArrayType> _type = in_value.GetType();

  bool status = (static_cast<bool>(_type) && static_cast<bool>(pv_value));

  if (status)
    {
      if (::ccs::HelperTools::Is<::ccs::types::CompoundType>(_type->GetElementType()))
        {
          pvxs::shared_array<pvxs::Value> _array (_type->GetMultiplicity());

          for (::ccs::types::uint32 index = 0u; (status && (index < _type->GetMultiplicity())); index++)
            {
              // Allocate structure
              _array[index] = pv_value.allocMember();

              // Recursion
              status = AssignAnyValueToPVXSValue(::ccs::HelperTools::GetElementValue<::ccs::types::AnyValue>(&in_value, index), _array[index]);
            }

          // Replace array
          pv_value = _array.freeze();
        }
      else // Scalar
        {
          status = (*(__function_map.GetElement(_type->GetElementType()->GetName()).to.arrays))(in_value, pv_value, _type->GetMultiplicity());
        }
    }

  return status;

}

static inline bool AssignAnyValueToPVXSValue_CompoundType (const ::ccs::types::AnyValue& in_value, pvxs::Value& pv_value)
{

  const ::ccs::base::SharedReference<const ::ccs::types::CompoundType> _type = in_value.GetType();

  bool status = (static_cast<bool>(_type) && static_cast<bool>(pv_value));

  // Recursion
  for (::ccs::types::uint32 index = 0u; (status && (index < _type->GetAttributeNumber())); index++)
    {
      const ::ccs::types::char8* _name = _type->GetAttributeName(index);
      pvxs::Value _value = pv_value[_name];

      status = AssignAnyValueToPVXSValue(::ccs::HelperTools::GetAttributeValue<::ccs::types::AnyValue>(&in_value, _name), _value);
    }

  return status;

}

bool AssignAnyValueToPVXSValue (const ::ccs::types::AnyValue& in_value, pvxs::Value& pv_value)
{

  const ::ccs::base::SharedReference<const ::ccs::types::AnyType> type = in_value.GetType();

  bool status = (static_cast<bool>(type) && static_cast<bool>(pv_value));
#ifdef LOG_DEBUG_ENABLE
  if (status)
    {
      ::ccs::types::char8 buffer [1024]; 
      in_value.SerialiseInstance(buffer, 1024u);
      log_debug("AssignAnyValueToPVXSValue - Value is '%s' ..", buffer);
      log_debug("AssignAnyValueToPVXSValue - And PV ..");
      std::cout << pv_value << std::endl;
    }
#endif
  if (status && ::ccs::HelperTools::Is<::ccs::types::ArrayType>(type))
    {
      status = AssignAnyValueToPVXSValue_ArrayType(in_value, pv_value);
    }
  else if (status && ::ccs::HelperTools::Is<::ccs::types::CompoundType>(type))
    {
      status = AssignAnyValueToPVXSValue_CompoundType(in_value, pv_value);
    }
  else if (status && ::ccs::HelperTools::Is<::ccs::types::ScalarType>(type))
    {
      status = (*(__function_map.GetElement(type->GetName()).to.scalars))(in_value, pv_value);
    }

  return status;

}

bool SerialiseAnyValueToPVXSValue (const ::ccs::types::AnyValue& in_value, pvxs::Value& pv_value)
{

  bool status = !static_cast<bool>(pv_value); // Invalid, uninitialised

  if (status)
    {
      try
        {
          pv_value = AnyTypeToPVTypeDef(in_value.GetType()).create();
        }
      catch (const std::exception& e)
        {
          log_error("AnyValueToPVXSValue - .. '%s' exception caught", e.what());
          status = false;
        }
#if 0 // PVXS and/or AnyValue are throwing from std::exception
      catch (...)
        {
          log_error("AnyValueToPVXSValue - .. Unknown exception caught");
          status = false;
        }
#endif
    }

  if (status)
    {
      // Copy contents over
      status = AssignAnyValueToPVXSValue(in_value, pv_value);
    }

  return status;

}

static inline bool IsPVXSScalarArrayCode (const pvxs::TypeCode& code)
{
#if 0
  bool status = ((pvxs::TypeCode::BoolA == code) || 
                 (pvxs::TypeCode::Int8A == code) ||
                 (pvxs::TypeCode::UInt8A == code) ||
                 (pvxs::TypeCode::Int16A == code)  ||
                 (pvxs::TypeCode::UInt16A == code) ||
                 (pvxs::TypeCode::Int32A == code)  ||
                 (pvxs::TypeCode::UInt32A == code) ||
                 (pvxs::TypeCode::Int64A == code)  ||
                 (pvxs::TypeCode::UInt64A == code) ||
                 (pvxs::TypeCode::Float32A == code) ||
                 (pvxs::TypeCode::Float64A == code));
#else // TypeCode.isarray() && ! StructA, AnyA, etc.
  bool status = (code.isarray() &&  
                 (pvxs::TypeCode::AnyA != code) &&
                 (pvxs::TypeCode::StructA != code) &&
                 (pvxs::TypeCode::UnionA != code));
#endif
  return status;

}

static inline ::ccs::base::SharedReference<const ::ccs::types::AnyType> AnyTypeFromPVXSValue_ArrayType (const pvxs::Value& pv_value)
{

  ::ccs::base::SharedReference<const ::ccs::types::AnyType> type;

  bool status = static_cast<bool>(pv_value); // Valid .. initialised

  if (status)
    {
      status = ((pvxs::TypeCode::StructA == pv_value.type()) ||
                IsPVXSScalarArrayCode(pv_value.type()));
    }

  ::ccs::types::string _name = STRING_UNDEFINED;
  ::ccs::types::uint32 _size = 0u;

  if (status)
    {
      log_debug("AnyTypeFromPVXSValue<ArrayType> - .. is array ..'");
      _size = pv_value.as<pvxs::shared_array<const void>>().size();
      status = (0u < _size);
    }

  if (status)
    { // Create array type
      ::ccs::types::ArrayType* _type = NULL_PTR_CAST(::ccs::types::ArrayType*);

      if (pvxs::TypeCode::StructA == pv_value.type())
        {
          log_debug("AnyTypeFromPVXSValue<ArrayType> - .. extract first element ..'");
          pvxs::shared_array<const pvxs::Value> _array = pv_value.as<pvxs::shared_array<const pvxs::Value>>();
          pvxs::Value _value = _array[0]; 

          // Recursion on element
          ::ccs::base::SharedReference<const ::ccs::types::AnyType> __type = AnyTypeFromPVXSValue(_value);
#if 0
          _type = new (std::nothrow) ::ccs::types::ArrayType(pv_value.type().name(), // I.e. 'struct[]'
                                                             __type, 
                                                             _size);
#else
          // Compose useful type name .. lost with PVXS porting
          snprintf(_name, ::ccs::types::MaxStringLength, "%s[%u]", __type->GetName(), _size);
          _type = new (std::nothrow) ::ccs::types::ArrayType(_name,
                                                             __type, 
                                                             _size);
#endif
        }
      else
        { // Compose useful type name .. lost with PVXS porting
#if 0
          _type = new (std::nothrow) ::ccs::types::ArrayType(pv_value.type().name(), // E.g. 'bool[]'
                                                             ::ccs::base::GlobalTypeDatabase::GetType(::ccs::types::GetName(::ccs::types::GetIdentifier(pv_value.type().scalarOf().name()))),
                                                             _size);
#else
          snprintf(_name, ::ccs::types::MaxStringLength, "%s[%u]", ::ccs::types::GetName(::ccs::types::GetIdentifier(pv_value.type().scalarOf().name())), _size);
          _type = new (std::nothrow) ::ccs::types::ArrayType(_name,
                                                             ::ccs::base::GlobalTypeDatabase::GetType(::ccs::types::GetName(::ccs::types::GetIdentifier(pv_value.type().scalarOf().name()))),
                                                             _size);
#endif
        }

      type = ::ccs::base::SharedReference<const ::ccs::types::AnyType>(dynamic_cast<const ::ccs::types::AnyType*>(_type));
      // cppcheck-suppress memleak // Passed to a smart pointer that will take care of memory management .. the raw pointer is preferable for const casting
    }

  return type;

}

static inline ::ccs::base::SharedReference<const ::ccs::types::AnyType> AnyTypeFromPVXSValue_CompoundType (const pvxs::Value& pv_value)
{

  ::ccs::base::SharedReference<const ::ccs::types::AnyType> type;

  bool status = static_cast<bool>(pv_value); // Valid .. initialised

  if (status)
    {
      status = (pvxs::TypeCode::Struct == pv_value.type());
    }

  if (status)
    { // Create empty structure
      log_debug("AnyTypeFromPVXSValue<CompoundType> - .. is structure ..'");
      ::ccs::types::CompoundType* _type = new (std::nothrow) ::ccs::types::CompoundType (pv_value.id().c_str());

      for (auto fld : pv_value.ichildren())
        {
          log_debug("AnyTypeFromPVXSValue<CompoundType> - .. add attribute '%s'", pv_value.nameOf(fld).c_str());
          _type->AddAttribute(pv_value.nameOf(fld).c_str(), AnyTypeFromPVXSValue(fld));
        }

      type = ::ccs::base::SharedReference<const ::ccs::types::AnyType>(dynamic_cast<const ::ccs::types::AnyType*>(_type));
      // cppcheck-suppress memleak // Passed to a smart pointer that will take care of memory management .. the raw pointer is preferable for const casting
    }

  return type;

}

static inline ::ccs::base::SharedReference<const ::ccs::types::AnyType> AnyTypeFromPVXSValue (const pvxs::Value& pv_value)
{

  ::ccs::base::SharedReference<const ::ccs::types::AnyType> type;

  bool status = static_cast<bool>(pv_value); // Valid .. initialised

  if (status)
    {
      log_debug("AnyTypeFromPVXSValue<AnyType> - Test PVXS type '%s' ..", pv_value.type().name());

      if ((pvxs::TypeCode::StructA == pv_value.type()) || IsPVXSScalarArrayCode(pv_value.type()))
        {
          type = AnyTypeFromPVXSValue_ArrayType(pv_value);
        }
      else if (pvxs::TypeCode::Struct == pv_value.type())
        {
          type = AnyTypeFromPVXSValue_CompoundType(pv_value);
        }
      else // Scalar TypeCode
        { // Map to relevant ScalarType
          log_debug("AnyTypeFromPVXSValue<ScalarType> - .. is scalar ..'");
          type = ::ccs::base::GlobalTypeDatabase::GetType(::ccs::types::GetName(::ccs::types::GetIdentifier(pv_value.type().name())));
        }
    }

  return type;

}

static inline bool AssignAnyValueFromPVXSValue_ArrayType (::ccs::types::AnyValue& out_value, const pvxs::Value& pv_value)
{

  const ::ccs::base::SharedReference<const ::ccs::types::ArrayType> _type = out_value.GetType();

  bool status = (static_cast<bool>(_type) && static_cast<bool>(pv_value));

  if (status)
    {
      ::ccs::types::uint32 size = _type->GetMultiplicity();
      ::ccs::types::uint32 _size = pv_value.as<pvxs::shared_array<const void>>().size();

      if (_size < size)
	{
	  log_notice("AssignAnyValueFromPVXSValue<ArrayType> - Array size mismatch '%u' vs '%u'", size, _size);
	  size = _size;
	}

      if (::ccs::HelperTools::Is<::ccs::types::CompoundType>(_type->GetElementType()))
        {
          pvxs::shared_array<const pvxs::Value> array = pv_value.as<pvxs::shared_array<const pvxs::Value>>();

          for (::ccs::types::uint32 index = 0u; (status && (index < size)); index++)
            {
              ::ccs::types::AnyValue _value (_type->GetElementType());
              pvxs::Value _elem = array[index];

              (void)AssignAnyValueFromPVXSValue(_value, _elem);
              status = ::ccs::HelperTools::SetElementValue(&out_value, index, _value);
            }
        }
      else // Scalar
        {
	  try
	    {
	      status = (*(__function_map.GetElement(_type->GetElementType()->GetName()).from.arrays))(out_value, pv_value, size);
	    }
	  catch (const std::exception& e)
	    {
	      log_error("AssignAnyValueFromPVXSValue<ArrayType> - '%s' exception caught", e.what());
	      status = false;
	    }
        }
    }

  return status;

}

static inline bool AssignAnyValueFromPVXSValue_CompoundType (::ccs::types::AnyValue& out_value, const pvxs::Value& pv_value)
{

  const ::ccs::base::SharedReference<const ::ccs::types::CompoundType> _type = out_value.GetType();

  bool status = (static_cast<bool>(_type) && static_cast<bool>(pv_value));

  // Recursion
  for (::ccs::types::uint32 index = 0u; (status && (index < _type->GetAttributeNumber())); index++)
    {
      const ::ccs::types::char8* _name = _type->GetAttributeName(index);
      ::ccs::types::AnyValue _value = ::ccs::HelperTools::GetAttributeValue<::ccs::types::AnyValue>(&out_value, _name);
      pvxs::Value _attr = pv_value[_name];

      (void)AssignAnyValueFromPVXSValue(_value, _attr);
      status = ::ccs::HelperTools::SetAttributeValue<::ccs::types::AnyValue>(&out_value, _name, _value);
    }

  return status;

}

bool AssignAnyValueFromPVXSValue (::ccs::types::AnyValue& out_value, const pvxs::Value& pv_value)
{

  const ::ccs::base::SharedReference<const ::ccs::types::AnyType> type = out_value.GetType();

  bool status = (static_cast<bool>(type) && static_cast<bool>(pv_value));

  if (status && ::ccs::HelperTools::Is<::ccs::types::ArrayType>(type))
    {
      status = AssignAnyValueFromPVXSValue_ArrayType(out_value, pv_value);
    }
  else if (status && ::ccs::HelperTools::Is<::ccs::types::CompoundType>(type))
    {
      status = AssignAnyValueFromPVXSValue_CompoundType(out_value, pv_value);
    }
  else if (status && ::ccs::HelperTools::Is<::ccs::types::ScalarType>(type))
    {
      status = (*(__function_map.GetElement(type->GetName()).from.scalars))(out_value, pv_value);
    }

  return status;

}

bool SerialiseAnyValueFromPVXSValue (::ccs::types::AnyValue& out_value, const pvxs::Value& pv_value)
{

  bool status = !static_cast<bool>(out_value.GetType()); // Invalid, uninitialised

  if (status)
    {
      try
        {
          ::ccs::base::SharedReference<const ::ccs::types::AnyType> _type = AnyTypeFromPVXSValue(pv_value);
          out_value = ::ccs::types::AnyValue(_type);
        }
      catch (const std::exception& e)
        {
          log_error("AnyValueFromPVXSValue - .. '%s' exception caught", e.what());
          status = false;
        }
#if 0 // PVXS and/or AnyValue are throwing from std::exception
      catch (...)
        {
          log_error("AnyValueFromPVXSValue - .. Unknown exception caught");
          status = false;
        }
#endif
    }

  if (status)
    {
      // Copy contents over
      status = AssignAnyValueFromPVXSValue(out_value, pv_value);
    }

  return status;

}

} // namespace HelperTools
} // namespace ccs
