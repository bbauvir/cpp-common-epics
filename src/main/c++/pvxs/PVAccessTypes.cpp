/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/main/c++/pvxs/PVAccessTypes.cpp $
* $Id: PVAccessTypes.cpp 111232 2020-07-06 11:37:18Z bauvirb $
*
* Project	: CODAC Core System
*
* Description	: Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2019 ITER Organization,
*		  CS 90 046
*		  13067 St. Paul-lez-Durance Cedex
*		  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <new> // std::nothrow

#include <common/BasicTypes.h> // Global type definition
#include <common/SysTools.h> // Misc. helper functions

//#define LOG_TRACE_ENABLE
//#undef LOG_TRACE_ENABLE 
//#define LOG_DEBUG_ENABLE
//#undef LOG_DEBUG_ENABLE 
#include <common/log-api.h> // Syslog wrapper routines

#include <common/AnyType.h> // Introspectable data type ..
#include <common/AnyTypeHelper.h> // .. associated helper routines
#include <common/AnyTypeDatabase.h>

#include <common/AnyValue.h> // Variable with introspectable data type ..
#include <common/AnyValueHelper.h> // .. associated helper routines

// Local header files

#include "PVAccessTypes.h"

// Constants

#undef LOG_ALTERN_SRC
#define LOG_ALTERN_SRC "pva-if"

// Type definition

namespace ccs {

namespace base {

namespace PVAccessTypes {

// Function declaration

// Global variables

ccs::base::SharedReference<const ::ccs::types::CompoundType> Timestamp_int; // Introspectable type definition
ccs::base::SharedReference<const ::ccs::types::CompoundType> Severity_int; // Introspectable type definition
ccs::base::SharedReference<const ::ccs::types::CompoundType> VariableBase_int; // Introspectable type definition

// Test types
ccs::base::SharedReference<const ::ccs::types::CompoundType> Scalars_int; // Introspectable type definition
ccs::base::SharedReference<const ::ccs::types::CompoundType> ScalarArrays_int; // Introspectable type definition
ccs::base::SharedReference<const ::ccs::types::CompoundType> Collection_int; // Introspectable type definition

// Function definition

bool Initialise (void)
{

  using namespace ::ccs::types;

  bool status = (static_cast<bool>(Timestamp_int) && static_cast<bool>(Severity_int) && static_cast<bool>(VariableBase_int) );

  if (!status)
    {
      Timestamp_int = ccs::base::SharedReference<const CompoundType>((new (std::nothrow) CompoundType (PVAccess_Timestamp_TypeName))
	->AddAttribute("standard", UnsignedInteger8)
	->AddAttribute("nanosec", UnsignedInteger64));
      //->AddAttribute("iso8601", (new (std::nothrow) ArrayType ("CharArray_t", ccs::types::Character8, 32u)));

      Severity_int = ccs::base::SharedReference<const CompoundType>((new (std::nothrow) CompoundType (PVAccess_Severity_TypeName))
	->AddAttribute("level", UnsignedInteger8)
	->AddAttribute("level_hr", (new (std::nothrow) ArrayType ("CharArray_t", Character8, 12u)))
	->AddAttribute("reason", String));

      // Introspectable type definition
      VariableBase_int = ccs::base::SharedReference<const CompoundType>((new (std::nothrow) CompoundType (PVAccess_VariableBase_TypeName))
	->AddAttribute("counter", UnsignedInteger64)
	->AddAttribute("timestamp", Timestamp_int)
	->AddAttribute("severity", Severity_int));
      
      status = ((static_cast<bool>(Timestamp_int) && GlobalTypeDatabase::Register(Timestamp_int)) &&
	        (static_cast<bool>(Severity_int) && GlobalTypeDatabase::Register(Severity_int)) &&
	        (static_cast<bool>(VariableBase_int) && GlobalTypeDatabase::Register(VariableBase_int)));
    }

  if (status)
    {
      status = (GlobalTypeDatabase::IsValid(PVAccess_Timestamp_TypeName) && 
	        GlobalTypeDatabase::IsValid(PVAccess_Severity_TypeName) && 
	        GlobalTypeDatabase::IsValid(PVAccess_VariableBase_TypeName));
    }

  if (status)
    {
      status = ((sizeof(Timestamp_t) == Timestamp_int->GetSize()) && 
	        (sizeof(Severity_t) == Severity_int->GetSize()) &&
	        (sizeof(VariableBase_t) == VariableBase_int->GetSize()));
    }

  { // Define test types
    Scalars_int = ccs::base::SharedReference<const CompoundType>((new (std::nothrow) CompoundType ("test::Scalars_t/v1.0"))
						      ->AddAttribute("bool", "bool")
						      ->AddAttribute("char8", "char8")
						      ->AddAttribute("int8", "int8")
						      ->AddAttribute("uint8", "uint8")
						      ->AddAttribute("int16", "int16")
						      ->AddAttribute("uint16", "uint16")
						      ->AddAttribute("int32", "int32")
						      ->AddAttribute("uint32", "uint32")
						      ->AddAttribute("int64", "int64")
						      ->AddAttribute("uint64", "uint64")
						      ->AddAttribute("float32", "float32")
						      ->AddAttribute("float64", "float64")
						      ->AddAttribute("string", "string"));

    ScalarArrays_int = ccs::base::SharedReference<const CompoundType>((new (std::nothrow) CompoundType ("test::ScalarArrays_t/v1.0"))
							   ->AddAttribute("bool", (new (std::nothrow) ArrayType ("Array_t", Boolean, 8u)))
							   ->AddAttribute("char8", (new (std::nothrow) ArrayType ("Array_t", Character8, 8u)))
							   ->AddAttribute("int8", (new (std::nothrow) ArrayType ("Array_t", SignedInteger8, 8u)))
							   ->AddAttribute("uint8", (new (std::nothrow) ArrayType ("Array_t", UnsignedInteger8, 8u)))
							   ->AddAttribute("int16", (new (std::nothrow) ArrayType ("Array_t", SignedInteger16, 8u)))
							   ->AddAttribute("uint16", (new (std::nothrow) ArrayType ("Array_t", UnsignedInteger16, 8u)))
							   ->AddAttribute("int32", (new (std::nothrow) ArrayType ("Array_t", SignedInteger32, 8u)))
							   ->AddAttribute("uint32", (new (std::nothrow) ArrayType ("Array_t", UnsignedInteger32, 8u)))
							   ->AddAttribute("int64", (new (std::nothrow) ArrayType ("Array_t", SignedInteger64, 8u)))
							   ->AddAttribute("uint64", (new (std::nothrow) ArrayType ("Array_t", UnsignedInteger64, 8u)))
							   ->AddAttribute("float32", (new (std::nothrow) ArrayType ("Array_t", Float32, 8u)))
							   ->AddAttribute("float64", (new (std::nothrow) ArrayType ("Array_t", Float64, 8u)))
							   ->AddAttribute("string", (new (std::nothrow) ArrayType ("Array_t", String, 8u))));

    Collection_int = ccs::base::SharedReference<const CompoundType>((new (std::nothrow) CompoundType ("test::Collect_t/v1.0"))
							 ->AddAttribute("scalars", Scalars_int)
							 ->AddAttribute("arrays", ScalarArrays_int));
  }
    
  return status;

}
#if 0
Severity::Level Variable::GetSeverity (void) const { return __base.severity.level; }
bool Variable::SetSeverity (Severity::Level level) { __base.severity.level = level; return true; }

bool Variable::Update (void)
{ 

  __base.counter += 1ul; 
  __base.timestamp.nanosec = ccs::HelperTools::GetCurrentTime(); 
  
  // Etc.

  return true; 

}

Variable::Variable (const ccs::base::SharedReference<const ccs::types::AnyType>& type) : ccs::types::AnyValue()
{ 

  // Instantiate approrpiate VariableType
  VariableType __type (type);
  ccs::types::AnyValue::SetType();

  return; 

}

Variable::~Variable (void) {}
#endif
VariableType::VariableType (const ccs::base::SharedReference<const ccs::types::AnyType>& type) : ccs::types::CompoundType()
{ 

  // Register introspectable types
  Initialise();

  // Base type attributes
  AddAttribute("counter", ccs::types::UnsignedInteger64);
  AddAttribute("timestamp", Timestamp_int);
  AddAttribute("severity", Severity_int);

  // Value attribute
  AddAttribute("value", type);

  return; 

}

VariableType::~VariableType (void) {}

} // namespace PVAccessTypes

} // namespace base

} // namespace ccs

#undef LOG_ALTERN_SRC
