/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/main/c++/pvxs/PVAccessServerImpl.cpp $
* $Id: PVAccessServerImpl.cpp 112584 2020-08-25 17:34:04Z langer $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2019 ITER Organization,
*                 CS 90 046
*                 13067 St. Paul-lez-Durance Cedex
*                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <functional> // std::function
#include <new> // std::nothrow

#include <pvxs/server.h>
#include <pvxs/sharedpv.h>

#include <common/tools.h> // Misc. helper functions, e.g. hash, etc.
#include <common/types.h> // Global type definition

//#define LOG_TRACE_ENABLE
//#undef LOG_TRACE_ENABLE
//#define LOG_DEBUG_ENABLE
//#undef LOG_DEBUG_ENABLE
#include <common/log-api.h> // Syslog wrapper routines

#include <common/LookUpTable.h> // Look-up table class definition, etc.

#include <common/AnyObject.h> // Abstract base class definition ..
#include <common/ObjectDatabase.h> // .. associated object database

#include <common/AnyThread.h> // Thread management class

#include <common/AnyValue.h> // Variable with introspectable data type ..
#include <common/AnyValueHelper.h> // .. associated helper routines

// Local header files

#include "PVXSValueHelper.h" // .. associated helper routines

#include "PVAccessTypes.h"

#include "PVAccessServerContext.h" // The singleton context
#include "PVAccessServerImpl.h" // This class definition

// Constants

#define DEFAULT_PVAINTERFACE_THREAD_PERIOD 10000000ul // 100Hz
#define DEFAULT_PVAINTERFACE_INSTANCE_NAME "pvas-if"

#undef LOG_ALTERN_SRC
#define LOG_ALTERN_SRC "pvxs-server"

#ifndef log_trace
#define log_trace(arg_msg...) {}
#endif

// Type definition

// Global variables

// Function declaration

// Function definition

namespace ccs {

namespace base {

// Initialiser methods
 
bool PVAccessServerImpl::AddVariable (const ::ccs::types::char8* name, ::ccs::types::DirIdentifier direction, const ::ccs::base::SharedReference<const ::ccs::types::AnyType>& type)
{

  bool status = ::ccs::HelperTools::Is<::ccs::types::CompoundType>(type);

  VariableInfo_t varInfo;

  varInfo.direction = direction;
  (void)::ccs::HelperTools::SafeStringCopy(varInfo.name, name, ::ccs::types::MaxStringLength);

  if (status)
    {
      // Create value
      varInfo.value = ::ccs::base::SharedReference<::ccs::types::AnyValue>(new (std::nothrow) ::ccs::types::AnyValue (type));
      status = static_cast<bool>(varInfo.value);
    }
  else
    {
      // Encapsulate non-compound type as value field in a PVAccessTypes::VariableType
      ::ccs::base::PVAccessTypes::VariableType _type (type);
      varInfo.value = ::ccs::base::SharedReference<::ccs::types::AnyValue>(new (std::nothrow) ::ccs::types::AnyValue (_type));
      status = static_cast<bool>(varInfo.value);
    }

  if (status)
    {
      // Create associated resources
      if (::ccs::types::OutputVariable == direction)
        {
          log_debug("PVAccessServerImpl::AddVariable - Create read/only PV ..");
          varInfo.pvrecord = pvxs::server::SharedPV(pvxs::server::SharedPV::buildReadonly());
        }
      else
        {
          log_debug("PVAccessServerImpl::AddVariable - Create read/write PV ..");
          varInfo.pvrecord = pvxs::server::SharedPV(pvxs::server::SharedPV::buildMailbox());

          // Set callback related to client connection
          varInfo.pvrecord.onFirstConnect([](pvxs::server::SharedPV &)
            {
              log_notice("PVAccessServerImpl::[]() - One client connected");
            });

          varInfo.pvrecord.onLastDisconnect([](pvxs::server::SharedPV &)
            {
              log_notice("PVAccessServerImpl::[]() - No more connected client");
            });

          // Set callback to update the AnyValue cache .. callback and not lambda due to cppncss issue with complexity analysis
          using namespace std::placeholders;
          varInfo.pvrecord.onPut(std::bind(&PVAccessServerImpl::PutCallback, this, _1, _2, _3));
        }

      log_debug("PVAccessServerImpl::AddVariable - .. add to the server");
      __server->addPV(varInfo.name, varInfo.pvrecord);

      log_debug("PVAccessServerImpl::AddVariable - .. create related PV value");
      status = ccs::HelperTools::SerialiseAnyValueToPVXSValue(*(varInfo.value), varInfo.pvvalue);
    }

  if (status)
    {
      // .. provide data type and start-up value
      log_debug("PVAccessServerImpl::AddVariable - .. bind with PV");
      varInfo.pvrecord.open(varInfo.pvvalue);
    }

  if (status)
    {
      status = __var_list.Register(name, varInfo);
    }
#ifdef LOG_DEBUG_ENABLE
  if (status)
    {
      ::ccs::types::char8 buffer [1024] = STRING_UNDEFINED; ::ccs::HelperTools::Serialise(type, buffer, 1024u);
      log_debug("PVAccessServerImpl::AddVariable - Success with type '%s'", buffer);
    }
#endif
  return status;
      
}

void PVAccessServerImpl::Initialise (void) 
{ 

  // Initialize resources
  __server = ccs::base::PVAccessServerContext::GetInstance();
  __sleep = DEFAULT_PVAINTERFACE_THREAD_PERIOD;
  __initialised = false;

  SetPeriod(__sleep);
  SetAccuracy(__sleep);

  return;

}

// Accessor methods

bool PVAccessServerImpl::IsValid (const ::ccs::types::char8 * const name) const { return __var_list.IsValid(name); }

::ccs::types::AnyValue* PVAccessServerImpl::GetVariable (const ::ccs::types::char8 * const name) const
{ 

  bool status = IsValid(name); 

  ::ccs::types::AnyValue* var = NULL_PTR_CAST(::ccs::types::AnyValue*); 

  VariableInfo_t varInfo; 

  if (status) 
    { 
      // Fetch from variable list
      (void)__var_list.GetElement(name, varInfo); 

      var = varInfo.value.GetReference();
    }

  return var; 

}

bool PVAccessServerImpl::UpdateVariable (const ::ccs::types::char8 * const name)
{ 

  bool status = IsValid(name); 

  VariableInfo_t varInfo; 

  if (status) 
    { 
      // Fetch from variable list
      (void)__var_list.GetElement(name, varInfo); 

      // Mark variable for update
      varInfo.update = true; 

      // Update PVXS value .. no need to re-generate the data type
      status = ccs::HelperTools::AssignAnyValueToPVXSValue(*(varInfo.value), varInfo.pvvalue);
    }

  if (status)
    {
      // Post update
      varInfo.pvrecord.post(std::move(varInfo.pvvalue));
    }

  if (status)
    {
      // Store for future use
      status = __var_list.SetElement(name, varInfo); 
    }

  return status; 

}

bool PVAccessServerImpl::SetCallback (const ::ccs::types::char8 * const name, const std::function<void(const ::ccs::types::AnyValue&)>& cb)
{

  bool status = IsValid(name); 

  VariableInfo_t varInfo; 

  if (status) 
    { 
      // Fetch from variable list
      (void)__var_list.GetElement(name, varInfo); 

      varInfo.cb = cb; 

      // Store for future use
      status = __var_list.SetElement(name, varInfo); 
    } 

  return status; 

}

// Miscellaneous methods

void PVAccessServerImpl::PutCallback (pvxs::server::SharedPV& pv, std::unique_ptr<pvxs::server::ExecOp>&& op, pvxs::Value&& value)
{

  log_notice("PVAccessServerImpl::PutCallback - Update variable '%s' from peer '%s'", op->name().c_str(), op->peerName().c_str());
#ifdef LOG_DEBUG_ENABLE
  log_debug("PVAccessServerImpl::PutCallback - Update cache with ..");
  std::cout << value << std::endl;
#endif
  bool status = IsValid(op->name().c_str()); 
  
  VariableInfo_t varInfo; 
  
  if (status) 
    { 
      // Fetch from variable list
      (void)__var_list.GetElement(op->name().c_str(), varInfo); 

      // Update record
      //varInfo.pvvalue = value;
      varInfo.pvrecord.post(std::move(value));
      
      // Update variable cache .. no need to re-generate the data type
      //status = ccs::HelperTools::AssignAnyValueFromPVXSValue(*(varInfo.value), varInfo.pvvalue); 
      status = ccs::HelperTools::AssignAnyValueFromPVXSValue(*(varInfo.value), value); 
#ifdef LOG_DEBUG_ENABLE
      if (status)
        {
          ::ccs::types::char8 buffer [1024];
          (varInfo.value)->SerialiseInstance(buffer, 1024u);
          log_debug("PVAccessServer::PutCallback - Result in '%s' ..", buffer);
        }
#endif
    }
  
  if (status && (NULL != varInfo.cb))
    {
      // Call associated callback, if any
      log_debug("PVAccessServer::PutCallback - Invoke application callback ..");
      varInfo.cb(*(varInfo.value));
    }
  
  if (status)
    {
      log_debug("PVAccessServer::PutCallback - Reply ..");
      op->reply();
    }
  else
    {
      log_error("PVAccessServer::PutCallback - Internal error in PUT callback");
      op->error("Internal error in PVAccessServer::PutCallback");
    }
  
  if (status)
    {
      // Store for future use
      (void)__var_list.SetElement(op->name().c_str(), varInfo); 
    }
  
  return;

}

// cppcheck-suppress unusedFunction // Callbacks associated to ccs::base::AnyThread
void PVAccessServerImpl::Opening (void)
{

  log_info("(background thread) Start PVA server");
  (void)ccs::base::PVAccessServerContext::Start(); // I.e. ignore if already started .. use-case RPC and variable servers in the same process
  __initialised = true;

  return;

}

// cppcheck-suppress unusedFunction // Callbacks associated to ccs::base::AnyThread
void PVAccessServerImpl::Execute (void)
{

  // ToDo - Move Variable update to this thread to avoid any blocking operation in calling application

  return;

}

// cppcheck-suppress unusedFunction // Callbacks associated to ccs::base::AnyThread
void PVAccessServerImpl::Closing (void)
{

  // Stop server
  log_info("(background thread) Stop PVA server");
  (void)ccs::base::PVAccessServerContext::Stop();

  return;

}

// Constructor methods

PVAccessServerImpl::PVAccessServerImpl (void) : ::ccs::base::SynchronisedThread("PVA Interface cache")
{ 

  // Initialise resources
  Initialise();

  // Register PVAccess variable types
  PVAccessTypes::Initialise();

  // Register instance in object database - Necessary for globally scoped accessors
  ::ccs::base::AnyObject* ref = (::ccs::base::AnyObject*) this; ::ccs::base::GlobalObjectDatabase::Register(DEFAULT_PVAINTERFACE_INSTANCE_NAME, ref); 

}

// Destructor method

PVAccessServerImpl::~PVAccessServerImpl (void)
{ 

  // Explicitly stop background thread .. inheritance means the virtual method are already gone when the AnyThread is deleted 
  (void)Terminate();

  // Remove instance from object database
  ::ccs::base::GlobalObjectDatabase::Remove(DEFAULT_PVAINTERFACE_INSTANCE_NAME); 

  // Clean-up server context
  for (::ccs::types::uint32 index = 0u; index < __var_list.GetSize(); index++)
    {
      __server->removePV(__var_list.GetName(index)); 
    }

  // Clear variable cache
  __var_list.Remove();

  // ToDo - Tear-down server context
  __server.Discard(); // Forget about it

  return;

}

} // namespace base

} // namespace ccs

#undef LOG_ALTERN_SRC
