/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/main/c++/pvxs/PVAccessRPCClient.cpp $
* $Id: PVAccessRPCClient.cpp 114049 2020-10-20 06:28:48Z bauvirb $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2019 ITER Organization,
*                 CS 90 046
*                 13067 St. Paul-lez-Durance Cedex
*                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <new> // std::nothrow

#include <pvxs/data.h>
#include <pvxs/client.h>

#include <common/BasicTypes.h> // Global type definition
#include <common/SysTools.h> // Misc. helper functions

//#define LOG_TRACE_ENABLE
//#undef LOG_TRACE_ENABLE
//#define LOG_DEBUG_ENABLE
//#undef LOG_DEBUG_ENABLE
#include <common/log-api.h> // Syslog wrapper routines

#include <common/AnyType.h> // Introspectable data type ..
#include <common/AnyTypeHelper.h> // .. associated helper routines

#include <common/AnyValue.h> // Variable with introspectable data type ..
#include <common/AnyValueHelper.h> // .. associated helper routines

#include <common/RPCClientImpl.h> 
#include <common/RPCClientImplFactory.h> 

#include <common/RPCTypes.h>

// Local header files

#include "PVXSValueHelper.h" // .. associated helper routines

#include "PVAccessClientContext.h" // The singleton context
#include "PVAccessRPCClient.h" // This class definition

// Constants

#undef LOG_ALTERN_SRC
#define LOG_ALTERN_SRC "pvxs-client"

// Type definition

namespace ccs {

namespace base {

// Function declaration

RPCClientImpl* PVAccessRPCClientConstructor (void);

// Global variables

static bool __is_registered = (RPCClientImplFactory::GetInstance()->Register("ccs::base::PVAccessRPCClient", &PVAccessRPCClientConstructor) &&
                               RPCClientImplFactory::GetInstance()->SetDefault("ccs::base::PVAccessRPCClient"));

// Function definition

RPCClientImpl* PVAccessRPCClientConstructor (void) { return dynamic_cast<RPCClientImpl*>(new (std::nothrow) PVAccessRPCClient ()); }

bool PVAccessRPCClient::Initialise (void)
{

  // Acquire client context
  __context = ::ccs::base::PVAccessClientContext::GetInstance(); // Singleton managed externally

  bool status = static_cast<bool>(__context);

  if (status)
    {
      __initialised = true;
    }

  return status;

}

bool PVAccessRPCClient::Launch (void)
{ 

  if (true != __initialised) 
    {
      this->Initialise(); 
    }

  return true; 

}

bool PVAccessRPCClient::Terminate (void) { return true; }

bool PVAccessRPCClient::IsConnected (void) const
{

  if (true != __connected)
    {
      // Try and connect
      ::ccs::types::AnyValue request ("{\"type\":\"RPCMessage_t\",\"attributes\":[{\"qualifier\":{\"type\":\"string\"}}]}");
      (void)ccs::HelperTools::SetAttributeValue(&request, "qualifier", "echo");
      pvxs::Value _request;
      (void)ccs::HelperTools::SerialiseAnyValueToPVXSValue(request, _request);

      try
        {
          log_debug("PVAccessRPCClient::IsConnected - Launch synchronous request ..");

          // Create synchronous request
          auto result = __context->rpc(this->GetService(), std::move(_request))
                                  .exec()->wait(1.0);

          if (static_cast<bool>(result))
            {
              *(const_cast<bool*>(&__connected)) = true;
            }
        }
      catch (const std::exception& e) // Interrupted in case of timeout
        {
          log_error("PVAccessRPCClient::IsConnected - .. '%s' exception caught", e.what());
          *(const_cast<bool*>(&__connected)) = false;
        }
    }

  return __connected; 

}

::ccs::types::AnyValue PVAccessRPCClient::SendRequest (const ::ccs::types::AnyValue& request, ::ccs::types::uint64 timeout) const
{

  bool status = (static_cast<bool>(request.GetType()) &&  // Is valid
                 ccs::HelperTools::Is<ccs::types::CompoundType>(request.GetType()));

  pvxs::Value _request;

  if (status)
    {
      log_info("PVAccessRPCClient::SendRequest - Create PVA RPC request with appropriate type");
      status = ccs::HelperTools::SerialiseAnyValueToPVXSValue(request, _request);
    }

  // Unit tests are logging reason which must be initialised
  ::ccs::types::string reason = "Success";

  if (!status) // 'Uninitialised request' or 'Not a structure' or 'Unable to create a PVXS Value'
    {
      (void)ccs::HelperTools::SafeStringCopy(reason, "Invalid request type", ::ccs::types::MaxStringLength);
    }

  pvxs::Value _reply;

  if (status)
    {
      try
        {
          log_debug("PVAccessRPCClient::SendRequest - Launch synchronous request ..");
          ::ccs::types::float64 _timeout = static_cast<::ccs::types::float64>(timeout) / 1.e9;

          // Create synchronous request .. with timeout
          auto result = __context->rpc(this->GetService(), std::move(_request))
                                  .exec()->wait(_timeout);

          status = static_cast<bool>(result);

          if (status)
            {
              // Extract reply
              _reply = result; // Asynchronous call returns a Result instance where the value is retrieved using result();
            }
        }
      catch (const std::exception& e)
        {
          log_error("PVAccessRPCClient::SendRequest - .. '%s' exception caught", e.what());
          (void)ccs::HelperTools::SafeStringCopy(reason, e.what(), ::ccs::types::MaxStringLength);
          status = false;
        }
    }

  ::ccs::types::AnyValue reply;

  if (status)
    {
      status = ccs::HelperTools::SerialiseAnyValueFromPVXSValue(reply, _reply);
    }
  else
    {
      reply = ccs::HelperTools::InstantiateRPCReply(status, "error", reason);
    }
#ifdef LOG_DEBUG_ENABLE
  if (status)
    {
      ::ccs::types::char8 buffer [1024];
      (void)reply.SerialiseInstance(buffer, 1024u);
      log_debug("PVAccessRPCClient::SendRequest - .. reply '%s'", buffer);
    }
#endif
  return reply;

}

ccs::types::AnyValue PVAccessRPCClient::SendRequest (const ccs::types::AnyValue& request) const { return SendRequest(request, 5000000000ul); }

PVAccessRPCClient::PVAccessRPCClient (void) { __initialised = false; __connected = false; return; }
PVAccessRPCClient::~PVAccessRPCClient (void) {}

} // namespace base

} // namespace ccs

#undef LOG_ALTERN_SRC
