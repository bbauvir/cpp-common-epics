/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/main/c++/pvxs/PVAccessClientContext.h $
* $Id: PVAccessClientContext.h 111303 2020-07-08 05:58:03Z bauvirb $
*
* Project	: CODAC Core System
*
* Description	: Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2019 ITER Organization,
*		  CS 90 046
*		  13067 St. Paul-lez-Durance Cedex
*		  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

/**
 * @file PVAccessClientContext.h
 * @brief Header file for PVAccessClientContext singleton.
 * @date 20/06/2020
 * @author Bertrand Bauvir (IO)
 * @copyright 2010-2020 ITER Organization
 * @detail This header file contains the definition of the PVAccessClientContext singleton.
 */

#ifndef _PVAccessClientContext_h_
#define _PVAccessClientContext_h_

// Global header files

#include <pvxs/client.h>

#include <common/BasicTypes.h> // Global type definition
#include <common/SharedReference.h>

// Local header files

// Constants

// Type definition

namespace ccs {

namespace base {

// Global variables

// Function declaration

// ToDo - Check API for client and server which is templated

namespace PVAccessClientContext {

::ccs::base::SharedReference<pvxs::client::Context> GetInstance (void); // Issue with static initialisation and loading shared library objects
bool SetInstance (::ccs::base::SharedReference<pvxs::client::Context>& context); // In case default configuration is not appropriate
bool IsInitialised (void);
bool Terminate (void); // Tear-down

// Function definition

} // namespace PVAccessClientContext

} // namespace base

} // namespace ccs

#endif // _PVAccessClientContext_h_

