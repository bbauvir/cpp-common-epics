/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/main/c++/pvxs/PVAccessServer.cpp $
* $Id: PVAccessServer.cpp 111232 2020-07-06 11:37:18Z bauvirb $
*
* Project	: CODAC Core System
*
* Description	: Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2019 ITER Organization,
*		  CS 90 046
*		  13067 St. Paul-lez-Durance Cedex
*		  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <functional> // std::function
#include <new> // std::nothrow

#include <common/tools.h> // Misc. helper functions, e.g. hash, etc.
#include <common/types.h> // Global type definition

//#define LOG_TRACE_ENABLE
//#undef LOG_TRACE_ENABLE
//#define LOG_DEBUG_ENABLE
//#undef LOG_DEBUG_ENABLE
#include <common/log-api.h> // Syslog wrapper routines

#include <common/LookUpTable.h> // Look-up table class definition, etc.

#include <common/AnyObject.h> // Abstract base class definition ..
#include <common/ObjectDatabase.h> // .. associated object database

#include <common/AnyThread.h> // Thread management class

#include <common/AnyValue.h> // Variable with introspectable data type ..
#include <common/AnyValueHelper.h> // .. associated helper routines

// Local header files

#include "PVXSValueHelper.h" // .. associated helper routines

#include "PVAccessServer.h" // This class definition
#include "PVAccessServerImpl.h" // .. associated implementation class

// Constants

#undef LOG_ALTERN_SRC
#define LOG_ALTERN_SRC "pvxs-server"

#ifndef log_trace
#define log_trace(arg_msg...) {}
#endif

// Type definition

namespace ccs {

namespace base {

// Global variables

namespace PVAccessInterface {

PVAccessServer* __pvas_if = NULL_PTR_CAST(PVAccessServer*);

} // namespace PVAccessInterface

// Function declaration

// Function definition

// Initializer methods
 
bool PVAccessServer::AddVariable (const ::ccs::types::char8 * const name, ::ccs::types::DirIdentifier direction, const ::ccs::types::AnyType * const type)
{

  ::ccs::base::SharedReference<const ::ccs::types::AnyType> _type (type);

  return __impl->AddVariable(name, direction, _type);

}

bool PVAccessServer::AddVariable (const ::ccs::types::char8 * const name, ::ccs::types::DirIdentifier direction, const ::ccs::base::SharedReference<const ::ccs::types::AnyType>& type)
{

  return __impl->AddVariable(name, direction, type);

}

bool PVAccessServer::AddVariable (const ::ccs::types::char8 * const name, ::ccs::types::DirIdentifier direction, const ::ccs::types::char8 * const type)
{

  ::ccs::base::SharedReference<::ccs::types::AnyType> _type; // Placeholder

  bool status = ccs::HelperTools::Parse(_type, type);

  if (status)
    {
      status = __impl->AddVariable(name, direction, _type);
    }

  return status;

}

bool PVAccessServer::Launch (void) { return __impl->Launch(); }

// Accessor methods

bool PVAccessServer::IsValid (const ::ccs::types::char8 * const name) const { return __impl->IsValid(name); }

::ccs::types::AnyValue* PVAccessServer::GetVariable (const ::ccs::types::char8 * const name) const { return __impl->GetVariable(name); }

bool PVAccessServer::UpdateVariable (const ::ccs::types::char8 * const name) { return __impl->UpdateVariable(name); }

bool PVAccessServer::SetCallback (const ::ccs::types::char8 * const name, const std::function<void(const ::ccs::types::AnyValue&)>& cb) { return __impl->SetCallback(name, cb); }

bool PVAccessServer::SetPeriod (::ccs::types::uint64 period) { return __impl->SetPeriod(period); }

// Constructor methods

PVAccessServer::PVAccessServer (void)
{ 

  // Instantiate implementation class
  __impl = new (std::nothrow) PVAccessServerImpl;

  bool status = (NULL_PTR_CAST(PVAccessServerImpl*) != __impl);

  if (status)
    {
      log_debug("PVAccessServer::PVAccessServer - Instantiated implementation class");
    }

  return;

}

// Destructor method

PVAccessServer::~PVAccessServer (void)
{ 

  if (NULL_PTR_CAST(PVAccessServerImpl*) != __impl)
    { 
      delete __impl; 
    }

  __impl= NULL_PTR_CAST(PVAccessServerImpl*);

  return;

}

} // namespace base

} // namespace ccs

#undef LOG_ALTERN_SRC
