/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/main/c++/pvxs/PVAccessMonitorImpl.cpp $
* $Id: PVAccessMonitorImpl.cpp 114214 2020-10-27 10:53:01Z bauvirb $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2019 ITER Organization,
*                 CS 90 046
*                 13067 St. Paul-lez-Durance Cedex
*                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <new> // std::nothrow
#include <functional> // std::function

#include <pvxs/data.h>
#include <pvxs/client.h>

#include <common/BasicTypes.h> // Global type definition
#include <common/StringTools.h> // Misc. helper functions

//#define LOG_TRACE_ENABLE
//#undef LOG_TRACE_ENABLE
//#define LOG_DEBUG_ENABLE
//#undef LOG_DEBUG_ENABLE
#include <common/log-api.h> // Syslog wrapper routines

#include <common/AnyType.h> // Introspectable data type ..
#include <common/AnyTypeHelper.h> // .. associated helper routines
#include <common/AnyValue.h> // Variable with introspectable data type ..
#include <common/AnyValueHelper.h> // .. associated helper routines

#include <common/PVMonitorImpl.h>
#include <common/PVMonitorImplFactory.h>

// Local header files

#include "PVXSValueHelper.h" // .. associated helper routines

#include "PVAccessClientContext.h" // The singleton context
#include "PVAccessMonitorImpl.h" // This class definition

#include "PVAccessMonitor.h"

// Constants

#undef LOG_ALTERN_SRC
#define LOG_ALTERN_SRC "pvxs-client"

// Type definition

namespace ccs {

namespace base {

// Function declaration

PVMonitorImpl* PVAccessMonitorConstructor (void);

// Global variables

static bool __is_registered = (PVMonitorImplFactory::GetInstance()->Register(PVAccessMonitor_Plugin_Name, &PVAccessMonitorConstructor) &&
                               PVMonitorImplFactory::GetInstance()->SetDefault(PVAccessMonitor_Plugin_Name));

// Function definition

PVMonitorImpl* PVAccessMonitorConstructor (void) { return dynamic_cast<PVMonitorImpl*>(new (std::nothrow) PVAccessMonitorImpl ()); }

// Initialiser methods
 
bool PVAccessMonitorImpl::Initialise (void) 
{ 

  // Acquire client context
  _context = ::ccs::base::PVAccessClientContext::GetInstance(); // Singleton managed externally

  bool status = static_cast<bool>(_context);

  if (status)
    {
      using namespace std::placeholders;

      // Create subscription .. with callback in lieu of lambda expression
      _sub = _context->monitor(this->GetChannel())
                        .maskConnected(false)
                        .maskDisconnected(false)
                        .event(std::bind(&PVAccessMonitorImpl::MonitorEvent, this, _1))
                        .exec();

      _initialised = true;
    }
  
  return status;

}

// Accessor methods

// Miscellaneous methods

// cppcheck-suppress unusedFunction // Callbacks associated to pvxs::client::Subscription
void PVAccessMonitorImpl::MonitorEvent (pvxs::client::Subscription& sub) 
{

  // Re-code for lower complexity metric
  ::ccs::types::AnyValue value; // Placeholder 
  ::ccs::base::PVMonitor::Event event = ::ccs::base::PVMonitor::Event::Undefined;

  bool status = (false == static_cast<bool>(value.GetType()));

  while (status) // Ensure the event queue is empty when leaving the callback ..
    {
      try
        {
          pvxs::Value _value = sub.pop();
          status = static_cast<bool>(_value);

          if (status)
            {
              status = ccs::HelperTools::SerialiseAnyValueFromPVXSValue(value, _value);
            }
#ifdef LOG_DEBUG_ENABLE      
          if (status)
            {
              ccs::types::char8 buffer [1024];
              value.SerialiseType(buffer, 1024u);
              log_debug("PVAccessMonitorImpl::MonitorEvent - Type '%s' ..", buffer);
            }
          
          if (status)
            {
              ccs::types::char8 buffer [1024];
              value.SerialiseInstance(buffer, 1024u);
              log_debug("PVAccessMonitorImpl::MonitorEvent - Instance '%s' ..", buffer);
            }
#endif
        }
      catch (const pvxs::client::Connected& e)
        {
          log_notice("PVAccessMonitorImpl::MonitorEvent - .. connect event caught with peer '%s'", e.peerName.c_str());
	  event = ::ccs::base::PVMonitor::Event::Connect;
          _connected = true;
        }
      catch (const pvxs::client::Disconnect& e)
        {
          log_notice("PVAccessMonitorImpl::MonitorEvent - .. disconnect exception caught");
	  event = ::ccs::base::PVMonitor::Event::Disconnect;
          _connected = false;
        }
      catch (const pvxs::client::Timeout& e)
        {
          log_warning("PVAccessMonitorImpl::MonitorEvent - .. timeout exception caught");
	  //event = ::ccs::base::PVMonitor::Event::Timeout;
	  event = ::ccs::base::PVMonitor::Event::Disconnect;
          _connected = false;
        }
      catch (const std::exception& e)
        {
          log_warning("PVAccessMonitorImpl::MonitorEvent - .. '%s' exception caught", e.what());
        }
#if 0
      catch (...)
        {
          log_warning("PVAccessMonitorImpl::MonitorEvent - .. unknown exception caught");
        }
#endif
    }

  if (true == static_cast<bool>(value.GetType())) // At least one monitor
    {
      log_debug("PVAccessMonitorImpl::MonitorEvent - Call data handler");
      // Call handler method
      PVMonitorImpl::CallDataHandler(value);
    }

  if (::ccs::base::PVMonitor::Event::Undefined != event) // At least one event
    {
      log_debug("PVAccessMonitorImpl::MonitorEvent - Call event handler");
      // Call handler method
      PVMonitorImpl::CallEventHandler(event);
    }
  
  return;

}

bool PVAccessMonitorImpl::IsConnected (void) const { return _connected; };

// Constructor methods

PVAccessMonitorImpl::PVAccessMonitorImpl (void)
{ 

  // Initialise attributes
  _connected = false;
  _initialised = false;

}

// Destructor method

PVAccessMonitorImpl::~PVAccessMonitorImpl (void) 
{ 

  if (true == _initialised)
    {
      // Cancel monitor
      _sub.reset();
      //_monitor.cancel(); 
    }

  // Tear-down client context
  _context.Discard(); // Forget about it locally ..
  (void)::ccs::base::PVAccessClientContext::Terminate(); // .. and globally if this is the last reference

  // Let the rest go out of scope ..

  return;

}

} // namespace base

} // namespace ccs

#undef LOG_ALTERN_SRC
