/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-sup-common-cpp/branches/codac-core-6.2/src/test/c++/unit/FunctionDatabase-tests.cpp $
* $Id: FunctionDatabase-tests.cpp 105560 2020-01-13 10:24:44Z kimh19 $
*
* Project	: CODAC Core System
*
* Description	: Unit test code
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2020 ITER Organization,
*				  CS 90 046
*				  13067 St. Paul-lez-Durance Cedex
*				  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

/**
 * @file PVXSValueHelper.h
 * @brief Header file for serialisation to/from epics::pvxs::Value helper routines.
 * @date 20/06/2020
 * @author Bertrand Bauvir (IO)
 * @copyright 2010-2020 ITER Organization
 * @detail This header file contains the definition of the serialisation helper routines.
 */

#ifndef __PVXSValueHelper_h_
#define __PVXSValueHelper_h_

// Global header files

#include <common/BasicTypes.h> // Misc. type definition

#include <common/AnyValue.h>

#include <pvxs/data.h>

// Local header files

// Constants

// Type definition

// Global variables

// Function declaration

#ifdef __cplusplus

namespace ccs {
namespace HelperTools {

bool AssignAnyValueFromPVXSValue (::ccs::types::AnyValue& out_value, const pvxs::Value& pv_value);
bool SerialiseAnyValueFromPVXSValue (::ccs::types::AnyValue& out_value, const pvxs::Value& pv_value);

bool AssignAnyValueToPVXSValue (const ::ccs::types::AnyValue& in_value, pvxs::Value& pv_value);
bool SerialiseAnyValueToPVXSValue (const ::ccs::types::AnyValue& in_value, pvxs::Value& pv_value);

// Function definition

} // namespace HelperTools
} // namespace ccs

#endif // __cplusplus
#endif // __PVXSValueHelper_h_
