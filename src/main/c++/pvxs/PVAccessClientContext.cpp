/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/main/c++/pvxs/PVAccessClientContext.cpp $
* $Id: PVAccessClientContext.cpp 114049 2020-10-20 06:28:48Z bauvirb $
*
* Project	: CODAC Core System
*
* Description	: Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2019 ITER Organization,
*		  CS 90 046
*		  13067 St. Paul-lez-Durance Cedex
*		  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <new> // std::nothrow

#include <pvxs/client.h>

#include <common/BasicTypes.h> // Global type definition

//#define LOG_TRACE_ENABLE
//#undef LOG_TRACE_ENABLE
//#define LOG_DEBUG_ENABLE
//#undef LOG_DEBUG_ENABLE
#include <common/log-api.h> // Syslog wrapper routines

#include <common/SharedReference.h>

// Local header files

#include "PVAccessClientContext.h"

// Constants

#undef LOG_ALTERN_SRC
#define LOG_ALTERN_SRC "pvxs-client"

// Type definition

namespace ccs {

namespace base {

// Global variables

namespace PVAccessClientContext {

static ::ccs::base::SharedReference<pvxs::client::Context> __ctxt;

// Function declaration

// Function definition

::ccs::base::SharedReference<pvxs::client::Context> GetInstance (void)
{

  bool status = static_cast<bool>(__ctxt);

  if (!status)
    {
      log_notice("PVAccessClientContext::GetInstance - Initialise the shared reference ..");
      __ctxt = ::ccs::base::SharedReference<pvxs::client::Context>(new (std::nothrow) pvxs::client::Context (pvxs::client::Config::from_env().build()));
      //status = static_cast<bool>(__ctxt); // Unused
    }

  return __ctxt;

}

bool SetInstance (::ccs::base::SharedReference<pvxs::client::Context>& context)
{

  bool status = (false == static_cast<bool>(__ctxt));

  if (!status)
    {
      log_notice("PVAccessClientContext::SetInstance - Pre-existing client context ..");
      status = Terminate();
    }

  if (status)
    {
      __ctxt = context;
    }

  return status;

}

bool IsInitialised (void)
{

  bool status = static_cast<bool>(__ctxt);

  return status;

}

bool Terminate (void)
{

  bool status = static_cast<bool>(__ctxt);

  if (status)
    {
      status = (1u == __ctxt.GetCount());
    }

  if (status)
    { // Assume destroying the reference is sufficient for context tear-down
      log_notice("PVAccessClientContext::Terminate - Forgetting the shared reference ..");
      __ctxt.Discard();
      status = (false == static_cast<bool>(__ctxt));
    }

  return status;

}

// Accessor methods

// Constructor methods

// Destructor method

} // namespace PVAccessClientContext

} // namespace base

} // namespace ccs

#undef LOG_ALTERN_SRC
