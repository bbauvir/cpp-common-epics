/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/main/c++/pvxs/PVAccessRPCClient.h $
* $Id: PVAccessRPCClient.h 111795 2020-07-20 06:38:56Z bauvirb $
*
* Project	: CODAC Core System
*
* Description	: Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2019 ITER Organization,
*		  CS 90 046
*		  13067 St. Paul-lez-Durance Cedex
*		  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

/**
 * @file PVAccessRPCClient.h
 * @brief Header file for PVAccessRPCClient class.
 * @date 01/07/2020 (porting to PVXS)
 * @author Bertrand Bauvir (IO)
 * @copyright 2010-2018 ITER Organization
 * @detail This header file contains the definition of the PVAccessRPCClient implementation class.
 */

#ifndef _PVAccessRPCClient_h_
#define _PVAccessRPCClient_h_

// Global header files

#include <memory> // std::shared_ptr

#include <pvxs/data.h>
#include <pvxs/client.h>

#include <common/AnyValue.h> // Variable with introspectable data type ..
#include <common/AnyValueHelper.h> // .. associated helper routines

#include <common/RPCClientImpl.h> 

// Local header files

// Constants

// Type definition

namespace ccs {

namespace base {

/**
 * @brief Implementation class providing support for PVA RPC client.
 * @detail ToDo.
 */

class PVAccessRPCClient : public RPCClientImpl
{

  private:

    bool __initialised;
    bool __connected;

    ::ccs::base::SharedReference<pvxs::client::Context> __context;
    std::shared_ptr<pvxs::client::Subscription> __sub;

    // Initialiser methods
    bool Initialise (void);

    // Miscellaneous methods
    ccs::types::AnyValue SendRequest (const ::ccs::types::AnyValue& request, ::ccs::types::uint64 timeout) const;

  protected:

  public:

    // Initialiser methods
    virtual bool Launch (void);
    virtual bool Terminate (void);

    // Accessor methods
    virtual bool IsConnected (void) const;

    // Miscellaneous methods
    virtual ccs::types::AnyValue SendRequest (const ::ccs::types::AnyValue& request) const;

    // Constructor methods
    PVAccessRPCClient (void);

    // Destructor method
    virtual ~PVAccessRPCClient (void); 

};

// Global variables

// Function declaration

// Function definition

} // namespace base

} // namespace ccs

#endif // _PVAccessRPCClient_h_

