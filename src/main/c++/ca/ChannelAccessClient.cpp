/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/main/c++/ca/ChannelAccessClient.cpp $
* $Id: ChannelAccessClient.cpp 111617 2020-07-15 09:35:47Z bauvirb $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2019 ITER Organization,
*                 CS 90 046
*                 13067 St. Paul-lez-Durance Cedex
*                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

/* ToDo 
     - Manage default value from configuration
*/

// Global header files

#include <functional> // std::function
#include <new> // std::nothrow

#include <cadef.h> // Channel Access API definition, etc.

#include <common/tools.h> // Misc. helper functions, e.g. hash, etc.
#include <common/types.h> // Global type definition

//#define LOG_TRACE_ENABLE
//#undef LOG_TRACE_ENABLE
//#define LOG_DEBUG_ENABLE
//#undef LOG_DEBUG_ENABLE
#include <common/log-api.h> // Syslog wrapper routines

//#include <common/lu-table.h> // Look-up table class definition, etc.
#include <common/LookUpTable.h> // LookUpTable<>able class definition, etc.

#include <common/AnyObject.h> // Abstract base class definition ..
#include <common/ObjectDatabase.h> // .. associated object database

#include <common/any-thread.h> // Thread management class

#include <common/AnyValue.h> // Variable with introspectable data type ..
#include <common/AnyValueHelper.h> // .. associated helper routines

// Local header files

#include "AnyTypeToCA.h" // .. associated helper routines

#include "ChannelAccessHelper.h"

#include "ChannelAccessClient.h" // This class definition

// Constants

#define DEFAULT_CAINTERFACE_THREAD_PERIOD 10000000ul // 100Hz
#define DEFAULT_CAINTERFACE_INSTANCE_NAME "ca-interface"

#define MAXIMUM_VARIABLE_NUM 256

#undef LOG_ALTERN_SRC
#define LOG_ALTERN_SRC "ca-if"

#ifndef log_trace
#define log_trace(arg_msg...) {}
#endif

// Type definition

namespace ccs {

namespace base {

class ChannelAccessClientImpl : public AnyObject
{

  private:

    ::ccs::types::uint64 m_sleep;

    ccs::base::SynchronisedThreadWithCallback* m_thread;

    // Initializer methods
    void Initialize (void);

  protected:

  public:

    ::ccs::types::CompoundType* m_type; // Introspectable type definition for the variable cache ..
    ::ccs::types::AnyValue* m_value; // Introspectable variable mapped to the whole cache ..

    bool m_initialized;

    typedef struct VariableInfo {

      bool connected;
      bool update;
      ::ccs::types::DirIdentifier direction;
      
      std::function<void(const ::ccs::types::char8 * const, const ccs::types::AnyValue&)> cb;

      ::ccs::types::string name;
      chid channel;
      chtype type; // ChannelAccess native type
      ::ccs::types::uint32 mult;
      
      void* reference = NULL; // Reference in the cache
      
      ::ccs::base::SharedReference<const ccs::types::AnyType> __type; // Introspectable type definition for the variable cache ..
      ::ccs::types::AnyValue* value = static_cast<ccs::types::AnyValue*>(NULL);
      
    } VariableInfo_t;

    ::ccs::base::LookUpTable<VariableInfo_t> __var_table;

    // Initializer methods
    bool AddVariable (const ::ccs::types::char8 * const name, ccs::types::DirIdentifier direction, const ccs::base::SharedReference<const ccs::types::AnyType>& type);
    bool AddVariable (const ::ccs::types::char8 * const name, bool isInput, chtype type, ccs::types::uint32 mult = 1u); // From SDD PV list */

    bool SetPeriod (ccs::types::uint64 period);
    bool Launch (void); // Should be called after the variable table is populated

    // Accessor methods
    bool IsValid (ccs::types::uint32 id) const;
    bool IsValid (const ::ccs::types::char8 * const name) const;

    ::ccs::types::uint32 GetVariableId (const ::ccs::types::char8 * const name) const;

    ::ccs::types::AnyValue* GetVariable (ccs::types::uint32 id) const;
    ::ccs::types::AnyValue* GetVariable (const ::ccs::types::char8 * const name) const;

    template <typename Type> bool GetVariable (const ::ccs::types::char8 * const name, Type& value) const;

    ::ccs::base::SharedReference<const ccs::types::AnyType> GetVariableType (ccs::types::uint32 id) const;
    ::ccs::base::SharedReference<const ccs::types::AnyType> GetVariableType (const ::ccs::types::char8 * const name) const;

    template <typename Type> bool SetVariable (const ::ccs::types::char8 * const name, Type& value);

    bool UpdateVariable (ccs::types::uint32 id);
    bool UpdateVariable (const ::ccs::types::char8 * const name);

    // Miscellaneous methods
    bool SetCallback (ccs::types::uint32 id, const std::function<void(const ::ccs::types::char8 * const, const ccs::types::AnyValue&)>& cb);
    bool SetCallback (const ::ccs::types::char8 * const name, const std::function<void(const ::ccs::types::char8 * const, const ccs::types::AnyValue&)>& cb);

    // Constructor methods
    ChannelAccessClientImpl (void);

    // Destructor method
    virtual ~ChannelAccessClientImpl (void);

};

// Global variables

namespace ChannelAccessInterface {

static ChannelAccessClient* __ca_if = static_cast<ChannelAccessClient*>(NULL); // Issue with static initialisation and loading shared library objects .. makes this static

// Function declaration

// Function definition

ChannelAccessClient* GetUniqueChannelAccessClientInstance (void)
{

  if (static_cast<ChannelAccessClient*>(NULL) == __ca_if)
    {
      __ca_if = new (std::nothrow) ChannelAccessClient ();
    }

  return __ca_if;

}

void TerminateUniqueChannelAccessClientInstance (void)
{

  if (static_cast<ChannelAccessClient*>(NULL) != __ca_if)
    {
      delete __ca_if;
      __ca_if = static_cast<ChannelAccessClient*>(NULL);
    }

  return;

}

} // namespace ChannelAccessInterface

static inline bool IsStringType (const ccs::base::ChannelAccessClientImpl::VariableInfo_t& varInfo)
{

  bool status = ((varInfo.type == DBR_STRING) ||
		 ((varInfo.type == DBR_CHAR) && (varInfo.mult > 1)));

  return status;

}

} // namespace base

} // namespace ccs

void ChannelAccessInterface_Get_CB (struct event_handler_args args)
{

  log_trace("Entering '%s' routine", __FUNCTION__);

  bool status = (args.status == ECA_NORMAL);

  if (!status)
    {
      log_warning("%s - Routine invoked with '%d'", __FUNCTION__, args.status);
    }

  ccs::base::ChannelAccessClientImpl* self = (ccs::base::ChannelAccessClientImpl*) args.usr;
  bool found = false;
  
  for (uint_t index = 0; (status && (index < (self->__var_table).GetSize())); index += 1)
    {

      ccs::types::string name;
      (void)ccs::HelperTools::SafeStringCopy(name, (self->__var_table).GetName(index), ccs::types::MaxStringLength);

      ccs::base::ChannelAccessClientImpl::VariableInfo_t varInfo = (self->__var_table).GetElement(index);
      
      if (varInfo.channel == args.chid)
        {
          log_debug("(Monitor callback) Notification received for '%s' channel", (char*) name);

          // Bug 12902 - Handling of STRING or CHAR[]
          if (IsStringType(varInfo))
            {
              log_debug("(Monitor callback) Variable '%s' holds '%s'", name, (char*) args.dbr);
              (void)ccs::HelperTools::SafeStringCopy(static_cast<ccs::types::char8*>(varInfo.reference), static_cast<const ccs::types::char8*>(args.dbr), (varInfo.__type)->GetSize());
            }
          else
            {
              memcpy(varInfo.reference, (void*) args.dbr, (varInfo.__type)->GetSize());
            }

          // Invoke callback, if any
          if (NULL != varInfo.cb)
            {
              log_debug("(Monitor callback) Invoke callback for '%s' channel", __FUNCTION__, name);
              varInfo.cb(name, *varInfo.value);
            }

          found = true; 
          break; 
        }
      else
        {
          continue;
        }

    }

  if (found == false)
    {
      log_warning("(Monitor callback) Notification received for unknown channel");
    }

  log_trace("Leaving '%s' routine", __FUNCTION__);

  return;

}

void ChannelAccessInterface_Thread_PRBL (ccs::base::ChannelAccessClientImpl* self)
{

  log_trace("Entering '%s' routine", __FUNCTION__);

  // Create variable cache
  self->m_value = new (std::nothrow) ccs::types::AnyValue (self->m_type);

  // CA context
  bool status = ccs::HelperTools::ChannelAccess::CreateContext(true); // Bug 13475 - Enable preemptive callbacks .. else callbacks are made from this thread whilst calling upon CA API

  if (status)
    {
      status =  ccs::HelperTools::ChannelAccess::AttachContext(ccs::HelperTools::ChannelAccess::GetContext());
    }

  for (::ccs::types::uint32 index = 0u; index < (self->__var_table).GetSize(); index++)
    {
      ::ccs::types::string name;
      (void)ccs::HelperTools::SafeStringCopy(name, (self->__var_table).GetName(index), ccs::types::MaxStringLength);

      ccs::base::ChannelAccessClientImpl::VariableInfo_t varInfo = (self->__var_table).GetElement(index);

      // Store cache reference
      varInfo.reference = ccs::HelperTools::GetAttributeReference(self->m_value, name);
      varInfo.value = new (std::nothrow) ccs::types::AnyValue (varInfo.__type, varInfo.reference);
  
      // Connect to channel
      status = ccs::HelperTools::ChannelAccess::ConnectVariable(name, varInfo.channel);

      if (status)
        {
          log_debug("(Preamble) Connection to channel '%s' has been successfully verified", name);
          varInfo.connected = true;
        }

      // Install subscription for INPUT or ANY variable
      if (varInfo.direction != ccs::types::OutputVariable)
        { // Element count set to '0' to support arrays
          if (ca_create_subscription(varInfo.type, 0, varInfo.channel, DBE_VALUE, &ChannelAccessInterface_Get_CB, (void*) self, NULL) != ECA_NORMAL)
            {
              log_error("(Preamble) Subscription to channel '%s' failed", name);
              continue;
            }
          else
            {
              log_debug("(Preamble) Subscription to channel '%s' has been successfully created", name);
            }
        }
      
      // Store for future use
      (self->__var_table).SetElement(index, varInfo);

    }

  // Flush IO
  ccs::HelperTools::ChannelAccess::FlushIOBuffer();

  self->m_initialized = true;

  log_trace("Leaving '%s' routine", __FUNCTION__);

  return;

}

void ChannelAccessInterface_Thread_CB (ccs::base::ChannelAccessClientImpl* self)
{

  log_trace("Entering '%s' routine", __FUNCTION__);

  for (::ccs::types::uint32 index = 0u; index < (self->__var_table).GetSize(); index++)
    {
      ::ccs::types::string name;
      (void)ccs::HelperTools::SafeStringCopy(name, (self->__var_table).GetName(index), ccs::types::MaxStringLength);

      ccs::base::ChannelAccessClientImpl::VariableInfo_t varInfo = (self->__var_table).GetElement(index);
      
      if ((varInfo.direction == ccs::types::InputVariable) || (varInfo.update != true)) // Inputs are managed through notification - Proceed only for OUTPUT or ANY variable which require update
        {
          continue; // Nothing to do for this channel
        }

      if (false == ccs::HelperTools::ChannelAccess::IsConnected(varInfo.channel))
        {
          if (varInfo.connected == true) log_warning("(Write thread) Connection to channel '%d %s' has been lost", index, name);
          varInfo.connected = false; (self->__var_table).SetElement(index, varInfo);
          continue;
        }
      else
        {
          if (varInfo.connected == false) log_info("(Write thread) Connection to channel '%d %s' has been re-established", index, name);
          varInfo.connected = true;
        }

      log_debug("(Write thread) Channel '%s' needs update", name);
#ifdef LOG_DEBUG_ENABLE
      if (varInfo.type == DBR_STRING) log_debug("(Write thread) Variable '%s' holds '%s'", name, (char*) varInfo.reference);
      if ((varInfo.type == DBR_CHAR) && (varInfo.mult > 1)) log_debug("(Write thread) Variable '%s' holds '%s'", name, (char*) varInfo.reference);
#endif
      varInfo.update = false;
          
      (void)ccs::HelperTools::ChannelAccess::WriteVariable(varInfo.channel, varInfo.type, varInfo.mult, varInfo.reference);

      // Store value for later use - Only the update request flag which has been cleared
      (self->__var_table).SetElement(index, varInfo);
    }

  log_trace("Leaving '%s' routine", __FUNCTION__);

  return;

}

void ChannelAccessInterface_Thread_POST (ccs::base::ChannelAccessClientImpl* self)
{

  for (::ccs::types::uint32 index = 0u; index < (self->__var_table).GetSize(); index++)
    { // Detach from channel
      ccs::base::ChannelAccessClientImpl::VariableInfo_t varInfo = (self->__var_table).GetElement(index);
      (void)ccs::HelperTools::ChannelAccess::DetachVariable(varInfo.channel);
    }

  // CA context
  (void)ccs::HelperTools::ChannelAccess::DetachContext();
  (void)ccs::HelperTools::ChannelAccess::ClearContext();

  (void)self;

  return;

}

namespace ccs {

namespace base {

// Initializer methods
 
bool ChannelAccessClient::AddVariable (const ::ccs::types::char8 * const name, ccs::types::DirIdentifier direction, const ccs::base::SharedReference<const ccs::types::AnyType>& type)
{
  return __impl->AddVariable(name, direction, type);
}

bool ChannelAccessClient::AddVariable (const ::ccs::types::char8 * const name, bool isInput, ccs::types::uint32 type, ccs::types::uint32 mult)
{
  return __impl->AddVariable(name, isInput, static_cast<chtype>(type), mult);
}

static inline bool VerifyType (const ccs::base::SharedReference<const ccs::types::AnyType>& type)
{

  bool status = (ccs::HelperTools::Is<ccs::types::ScalarType>(type) || 
                 (ccs::HelperTools::Is<ccs::types::ArrayType>(type) && 
                  ccs::HelperTools::Is<ccs::types::ScalarType>(ccs::base::SharedReference<const ccs::types::ArrayType>(type)->GetElementType())));

  return status;

}

static inline bool GetMultiplicity (const ccs::base::SharedReference<const ccs::types::AnyType>& type)
{

  ::ccs::types::uint32 mult = 1u;

  if (::ccs::HelperTools::Is<::ccs::types::ArrayType>(type))
    {
      mult = ccs::base::SharedReference<const ccs::types::ArrayType>(type)->GetElementNumber();
    }

  return mult;

}

bool ChannelAccessClientImpl::AddVariable (const ::ccs::types::char8 * const name, ccs::types::DirIdentifier direction, const ccs::base::SharedReference<const ccs::types::AnyType>& type)
{

  log_debug("ChannelAccessClientImpl::AddVariable('%s %u %s') - Entering method ..", name, direction, type->GetName());

  VariableInfo_t varInfo;

  varInfo.cb = NULL;
  varInfo.update = false;
  varInfo.direction = direction;
  ccs::HelperTools::SafeStringCopy(varInfo.name, name, ::ccs::types::MaxStringLength);

  bool status = VerifyType(type);

  if (status)
    {
      varInfo.type = ccs::HelperTools::AnyTypeToCAScalar(type);
      varInfo.mult = GetMultiplicity(type);
      varInfo.__type = type;
    }

  if (status)
    {
      log_debug("ChannelAccessClientImpl::AddVariable - Testing attributes ..");
      status = (static_cast<ccs::types::CompoundType*>(NULL) != this->m_type);
    }

  if (status)
    {
      status = ((false == (this->m_type)->HasAttribute(name)) && (false == __var_table.IsValid(name)));

      if (!status && (this->m_type)->HasAttribute(name) && __var_table.IsValid(name)) // Already registered
        { // Ignore
          status = true;
        }
      else
        {
          if (status)
            {
              log_info("ChannelAccessClientImpl::AddVariable - Adding attribute '%s' of type '%s' to '%s'", name, type->GetName(), (this->m_type)->GetName());
              status = (this->m_type)->AddAttribute(name, varInfo.__type); // Type definition for the variable cache
            }
          
          if (status)
            {
              log_info("ChannelAccessClientImpl::AddVariable - Registering pair ..");
              status = __var_table.Register(name, varInfo);
            }
        }
    }

  return status;

}

bool ChannelAccessClientImpl::AddVariable (const ::ccs::types::char8 * const name, bool isInput, chtype type, ccs::types::uint32 mult)
{

  return ChannelAccessClientImpl::AddVariable(name, ((isInput == true) ? ccs::types::AnyputVariable : ccs::types::OutputVariable), ccs::HelperTools::CAScalarToAnyType(type, mult));

}

void ChannelAccessClientImpl::Initialize (void) 
{ 

  // Initialize resources
  this->m_sleep = DEFAULT_CAINTERFACE_THREAD_PERIOD;
  this->m_initialized = false;

  this->m_type = new (std::nothrow) ccs::types::CompoundType ("caif::VariableCache_t");
  this->m_value = static_cast<ccs::types::AnyValue*>(NULL);

  this->m_thread = new ccs::base::SynchronisedThreadWithCallback ("CA Interface"); 
  (this->m_thread)->SetPeriod(this->m_sleep); (this->m_thread)->SetAccuracy(this->m_sleep);
  (this->m_thread)->SetPreamble((void (*)(void*)) &ChannelAccessInterface_Thread_PRBL, (void*) this); 
  (this->m_thread)->SetCallback((void (*)(void*)) &ChannelAccessInterface_Thread_CB, (void*) this); 
  (this->m_thread)->SetPostface((void (*)(void*)) &ChannelAccessInterface_Thread_POST, (void*) this); 
  //(this->m_thread)->Launch(); // Should be performed after the variable table is populated

  return;

}

bool ChannelAccessClient::Launch (void) { return __impl->Launch(); }
bool ChannelAccessClientImpl::Launch (void) { return ((this->m_thread)->Launch() == STATUS_SUCCESS); } // Should be called after the variable table is populated

bool ChannelAccessClient::SetPeriod (ccs::types::uint64 period) { return __impl->SetPeriod(period); }
bool ChannelAccessClientImpl::SetPeriod (ccs::types::uint64 period) { return ((this->m_thread)->SetPeriod(period) == STATUS_SUCCESS); }

// Accessor methods

bool ChannelAccessClient::IsValid (const ::ccs::types::char8 * const name) const { return __impl->IsValid(name); }

bool ChannelAccessClientImpl::IsValid (uint_t id) const { return (__var_table.IsValid(id) == STATUS_SUCCESS); }
bool ChannelAccessClientImpl::IsValid (const ::ccs::types::char8 * const name) const { return (__var_table.IsValid(name) == STATUS_SUCCESS); }

uint_t ChannelAccessClientImpl::GetVariableId (const ::ccs::types::char8 * const name) const { uint_t id = __var_table.GetIndex(name); return id; }

ccs::types::AnyValue* ChannelAccessClient::GetVariable (const ::ccs::types::char8 * const name) const { return __impl->GetVariable(__impl->GetVariableId(name)); }

ccs::types::AnyValue* ChannelAccessClientImpl::GetVariable (uint_t id) const { VariableInfo_t varInfo; varInfo.value = static_cast<ccs::types::AnyValue*>(NULL); if (this->IsValid(id)) __var_table.GetElement(id, varInfo); return varInfo.value; }
ccs::types::AnyValue* ChannelAccessClientImpl::GetVariable (const ::ccs::types::char8 * const name) const { return this->GetVariable(this->GetVariableId(name)); }

ccs::base::SharedReference<const ccs::types::AnyType> ChannelAccessClientImpl::GetVariableType (uint_t id) const { ccs::base::SharedReference<const ccs::types::AnyType> type; if (this->IsValid(id)) type = this->GetVariable(id)->GetType(); return type; }
ccs::base::SharedReference<const ccs::types::AnyType> ChannelAccessClientImpl::GetVariableType (const ::ccs::types::char8 * const name) const { return this->GetVariableType(this->GetVariableId(name)); }

bool ChannelAccessClient::UpdateVariable (const ::ccs::types::char8 * const name) { return __impl->UpdateVariable(__impl->GetVariableId(name)); }

bool ChannelAccessClientImpl::UpdateVariable (uint_t id) { bool status = this->IsValid(id); if (status) { VariableInfo_t varInfo; __var_table.GetElement(id, varInfo); varInfo.update = true; __var_table.SetElement(id, varInfo); } return status; }
bool ChannelAccessClientImpl::UpdateVariable (const ::ccs::types::char8 * const name) { return this->UpdateVariable(this->GetVariableId(name)); }

bool ChannelAccessClient::SetCallback (const ::ccs::types::char8 * const name, std::function<void(const ::ccs::types::char8 * const, const ccs::types::AnyValue&)> cb) { return __impl->SetCallback(name, cb); }

bool ChannelAccessClientImpl::SetCallback (ccs::types::uint32 id, const std::function<void(const ::ccs::types::char8 * const, const ccs::types::AnyValue&)>& cb) { VariableInfo_t varInfo; bool status = this->IsValid(id); if (status) { __var_table.GetElement(id, varInfo); varInfo.cb = cb; __var_table.SetElement(id, varInfo); } return status; }
bool ChannelAccessClientImpl::SetCallback (const ::ccs::types::char8 * const name, const std::function<void(const ::ccs::types::char8 * const, const ccs::types::AnyValue&)>& cb) { return this->SetCallback(this->GetVariableId(name), cb); }

// Constructor methods

ChannelAccessClient::ChannelAccessClient (void)
{ 

  // Instantiate implementation class
  __impl = new (std::nothrow) ChannelAccessClientImpl;

  bool status = (static_cast<ChannelAccessClientImpl*>(NULL) != __impl);

  if (status)
    {
      log_info("ChannelAccessClient::ChannelAccessClient - Successfully instantiated implementation class");
    }

  return; 

}

ChannelAccessClientImpl::ChannelAccessClientImpl (void)
{ 

  // Initialize resources
  this->Initialize();

  // Register instance in object database - Necessary for globally scoped accessors
  AnyObject* p_ref = (AnyObject*) this; ccs::base::GlobalObjectDatabase::Register(DEFAULT_CAINTERFACE_INSTANCE_NAME, p_ref); 

}

// Destructor method

ChannelAccessClient::~ChannelAccessClient (void)
{ 

  if (static_cast<ChannelAccessClientImpl*>(NULL) != __impl)
    { 
      delete __impl; 
    }

  __impl = static_cast<ChannelAccessClientImpl*>(NULL);

  return;

}

ChannelAccessClientImpl::~ChannelAccessClientImpl (void)
{ 

  // Release resources
  if (this->m_thread != NULL) delete this->m_thread;

  // Remove instance from object database
  ccs::base::GlobalObjectDatabase::Remove(DEFAULT_CAINTERFACE_INSTANCE_NAME); 

}

} // namespace base

} // namespace ccs

#undef LOG_ALTERN_SRC
