/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/main/c++/ca/AnyTypeToCA.h $
* $Id: AnyTypeToCA.h 110292 2020-06-12 16:15:26Z bauvirb $
*
* Project       : CODAC Core System
*
* Description   : Generic type class definition
*
* Author        : Bertrand Bauvir (IO)
*
* Copyright (c) : 2010-2019 ITER Organization,
*                                 CS 90 046
*                                 13067 St. Paul-lez-Durance Cedex
*                                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

/**
 * @file AnyTypeToCA.h
 * @brief Header file for AnyTypeToCA helper routines.
 * @date 01/11/2017
 * @author Bertrand Bauvir (IO)
 * @copyright 2010-2017 ITER Organization
 * @detail This header file contains the definition of the AnyTypeToCA helper routines.
 */

#ifndef _AnyTypeToCA_h_
#define _AnyTypeToCA_h_

// Global header files

//#include <memory> // std::shared_ptr, etc.

#include <cadef.h>

#include <common/types.h> // Misc. type definition
#include <common/tools.h> // Misc. helper functions

#include <common/log-api.h> // Logging functions

#include <common/AnyType.h>
#include <common/ArrayType.h>
#include <common/CompoundType.h>
#include <common/ScalarType.h>

#include <common/AnyTypeHelper.h>

#include <common/LookUpTable.h>

// Local header files

// Constants

// Type definition

namespace ccs {

namespace types {

typedef chtype CATypeIdentifier;

} // namespace types

namespace HelperTools {

// Global variables

static ccs::base::NameValuePair<ccs::types::CATypeIdentifier> __table [] = {

  { "bool", DBR_CHAR },
  { "char8", DBR_CHAR },
  { "int8", DBR_CHAR },
  { "uint8", DBR_CHAR },
  { "int16", DBR_SHORT },
  { "uint16", DBR_SHORT },
  { "int32", DBR_LONG },
  { "uint32", DBR_LONG },
  //  { "int64", DBR_CHAR },
  //  { "uint64", DBR_CHAR },
  { "float32", DBR_FLOAT },
  { "float64", DBR_DOUBLE },
  { "string", DBR_STRING },
  { EOT_KEYWORD, -1 } // End-of-table marker

};

static ccs::base::LookUpTable<ccs::types::CATypeIdentifier> __map_table (__table);

// Function declaration

static inline ccs::types::CATypeIdentifier AnyTypeToCAScalar (const ccs::base::SharedReference<const ccs::types::AnyType>& type);

// Function definition

static inline ccs::types::CATypeIdentifier AnyTypeToCAScalar (const ccs::base::SharedReference<const ccs::types::AnyType>& type)
{

  bool status = (ccs::HelperTools::Is<ccs::types::ScalarType>(type) || 
                 (ccs::HelperTools::Is<ccs::types::ArrayType>(type) && 
                  ccs::HelperTools::Is<ccs::types::ScalarType>(ccs::base::SharedReference<const ccs::types::ArrayType>(type)->GetElementType())));

  ccs::base::SharedReference<const ccs::types::ScalarType> inp_type;

  if (status)
    {
      inp_type = (ccs::HelperTools::Is<ccs::types::ScalarType>(type) ? 
                  type :
                  ccs::base::SharedReference<const ccs::types::ArrayType>(type)->GetElementType());
      status = static_cast<bool>(inp_type);
    }

  ccs::types::CATypeIdentifier out_type = -1;

  if (status)
    {
      out_type = __map_table.GetElement(inp_type->GetName());
    }

  return out_type;

}

static inline ccs::base::SharedReference<const ccs::types::AnyType> CAScalarToAnyType (ccs::types::CATypeIdentifier inp_type, ccs::types::uint32 mult)
{

  ccs::base::SharedReference<const ccs::types::AnyType> out_type;

  switch (inp_type)
    {
      case DBR_CHAR:
        out_type = ccs::base::SharedReference<const ccs::types::AnyType>(ccs::types::Character8);
        break;
      case DBR_ENUM:
        out_type = ccs::base::SharedReference<const ccs::types::AnyType>(ccs::types::SignedInteger8);
        break;
      case DBR_SHORT:
        out_type = ccs::base::SharedReference<const ccs::types::AnyType>(ccs::types::SignedInteger16);
        break;
      case DBR_LONG:
        out_type = ccs::base::SharedReference<const ccs::types::AnyType>(ccs::types::SignedInteger32);
        break;
      case DBR_FLOAT:
        out_type = ccs::base::SharedReference<const ccs::types::AnyType>(ccs::types::Float32);
        break;
      case DBR_DOUBLE:
        out_type = ccs::base::SharedReference<const ccs::types::AnyType>(ccs::types::Float64);
        break;
      case DBR_STRING:
        out_type = ccs::base::SharedReference<const ccs::types::AnyType>(ccs::types::String);
        break;
      default:
        break;
    }

  if (mult > 1U)
    {
      out_type = ccs::base::SharedReference<const ccs::types::AnyType>(ccs::base::SharedReference<const ccs::types::ArrayType>(ccs::HelperTools::NewArrayType("array", out_type, mult)));
    }

  return out_type;

}

} // namespace HelperTools

} // namespace ccs

#endif // _AnyTypeToCA_h_

