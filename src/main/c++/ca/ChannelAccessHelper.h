/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/main/c++/ca/ChannelAccessHelper.h $
* $Id: ChannelAccessHelper.h 111307 2020-07-08 07:45:00Z bauvirb $
*
* Project	: CODAC Core System
*
* Description	: Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2019 ITER Organization,
*		  CS 90 046
*		  13067 St. Paul-lez-Durance Cedex
*		  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

/**
 * @file ChannelAccessHelper.h
 * @brief Header file for ChannelAccess helper routines.
 * @date 01/07/2020
 * @author Bertrand Bauvir (IO)
 * @copyright 2010-2017 ITER Organization
 * @detail This header file contains the definition of the ChannelAccess helper routines.
 */

#ifndef _ChannelAccessHelper_h_
#define _ChannelAccessHelper_h_

// Global header files

#include <cadef.h> // Channel Access API definition, etc.

#include <common/BasicTypes.h> // Global type definition

//#define LOG_TRACE_ENABLE
//#undef LOG_TRACE_ENABLE
//#define LOG_DEBUG_ENABLE
//#undef LOG_DEBUG_ENABLE
#include <common/log-api.h>

// Local header files

// Constants

// Type definition

#ifdef __cplusplus

namespace ccs {

namespace HelperTools {

namespace ChannelAccess {

// Global variables

// Function declaration

// Function definition

static inline bool CreateContext (bool preempt = false) // CA wrappers
{

  log_trace("%s - Entering method", __FUNCTION__);
  
  // Create CA context
  log_debug("%s - Create CA context with '%s' preemptive callbacks", __FUNCTION__, (preempt ? "enabled" : "disabled"));
  bool status = (ECA_NORMAL == ca_context_create((preempt ? ca_enable_preemptive_callback : ca_disable_preemptive_callback)));

  log_trace("%s - Leaving method", __FUNCTION__);

  return status;

}

static inline bool AttachContext (struct ca_client_context* context) // CA wrappers
{

  log_trace("%s - Entering method", __FUNCTION__);
  
  // Attach to CA context
  log_debug("%s - Attach to CA context", __FUNCTION__);
  int _status = ca_attach_context(context);
  bool status = ((ECA_NORMAL == _status) || (ECA_ISATTACHED == _status));

  log_trace("%s - Leaving method", __FUNCTION__);

  return status;

}

static inline struct ca_client_context* GetContext (void) // CA wrappers
{

  log_trace("%s - Entering method", __FUNCTION__);
  
  // Retrieve CA context
  struct ca_client_context* ret = ca_current_context();

  log_trace("%s - Leaving method", __FUNCTION__);

  return ret;

}

static inline bool DetachContext (void) // CA wrappers
{

  log_trace("%s - Entering method", __FUNCTION__);
  
  // Detach from current CA context
  log_debug("%s - Detach from CA context", __FUNCTION__);
  ca_detach_context();

  log_trace("%s - Leaving method", __FUNCTION__);

  return true;

}

static inline bool ClearContext (void) // CA wrappers
{

  log_trace("%s - Entering method", __FUNCTION__);
  
  // Destroy CA context
  log_debug("%s - Destroy CA context", __FUNCTION__);
  ca_context_destroy();

  log_trace("%s - Leaving method", __FUNCTION__);

  return true;

}

static inline bool FlushIOBuffer (void) // CA wrappers
{

  log_trace("%s - Entering method", __FUNCTION__);
  
  // Flush pending IO
  bool status = (ECA_NORMAL == ca_pend_io(1));

  log_trace("%s - Leaving method", __FUNCTION__);

  return status;

}

static inline bool IsConnected (chid channel) // CA wrappers
{ 

  bool status = (cs_conn == ca_state(channel));

  return status;
 
}

static inline bool ConnectVariable (const ::ccs::types::char8 * const name, chid& channel) // CA wrappers
{

  log_trace("%s - Entering method", __FUNCTION__);
  
  bool status = (false == ::ccs::HelperTools::IsUndefinedString(name));

  if (status)
    { // Connect to channel
      log_debug("%s - Trying and connect to '%s' channel", __FUNCTION__, name);
      status = (ECA_NORMAL == ca_create_channel(name, NULL, NULL, 10, &channel));
    }

  if (status)
    { // Flush pending IO
      status = FlushIOBuffer();
    }

  if (status)
    { // Verify channel
      status = IsConnected(channel);

      if (status)
        {
          log_debug("%s - Connection to channel '%s' has been successfully verified", __FUNCTION__, name);
        }
      else
        {
          log_warning("%s - Connection to channel '%s' has not been successful", __FUNCTION__, name);
        }
    }

  log_trace("%s - Leaving method", __FUNCTION__);

  return status;

}
 
static inline bool DetachVariable (chid channel) // CA wrappers
{

  log_trace("%s - Entering method", __FUNCTION__);
  
  // Clear channel
  log_debug("%s - Trying and detach from '%d' channel", __FUNCTION__, channel);
  bool status = (ECA_NORMAL == ca_clear_channel(channel));

  if (status)
    { // Flush pending IO
      status = FlushIOBuffer();
    }

  log_trace("%s - Leaving method", __FUNCTION__);

  return status;

}
 
static inline bool ReadVariable (chid channel, chtype type, void* p_value) // CA wrappers
{

  log_trace("%s - Entering method", __FUNCTION__);

  bool status = IsConnected(channel);

  if (status)
    {
      status = (ECA_NORMAL == ca_get(type, channel, p_value));
    }
  
  if (status)
    { // Flush pending IO
      status = FlushIOBuffer();
    }

  log_trace("%s - Leaving method", __FUNCTION__);

  return status;

}

static inline bool ReadVariable (chid channel, chtype type, ::ccs::types::uint32 mult, void* p_value) // CA wrappers
{

  log_trace("%s - Entering method", __FUNCTION__);

  bool status = IsConnected(channel);

  if (status)
    {
      status = (ECA_NORMAL == ca_array_get(type, mult, channel, p_value));
    }
  
  if (status)
    { // Flush pending IO
      status = FlushIOBuffer();
    }

  log_trace("%s - Leaving method", __FUNCTION__);

  return status;

}

static inline bool WriteVariable (chid channel, chtype type, void * const p_value) // CA wrappers
{

  log_trace("%s - Entering method", __FUNCTION__);

  bool status = IsConnected(channel);

  if (status)
    {
      status = (ECA_NORMAL == ca_put(type, channel, p_value));
    }
  
  if (status)
    { // Flush pending IO
      status = FlushIOBuffer();
    }

  log_trace("%s - Leaving method", __FUNCTION__);

  return status;

}

static inline bool WriteVariable (chid channel, chtype type, ::ccs::types::uint32 mult, void * const p_value) // CA wrappers
{

  log_trace("%s - Entering method", __FUNCTION__);

  bool status = IsConnected(channel);

  if (status)
    {
      status = (ECA_NORMAL == ca_array_put(type, mult, channel, p_value));
    }
  
  if (status)
    { // Flush pending IO
      status = FlushIOBuffer();
    }

  log_trace("%s - Leaving method", __FUNCTION__);

  return status;

}

} // namespace ChannelAccess

} // namespace base

} // namespace ccs

#endif // __cplusplus

#endif // _ChannelAccessHelper_h_

