#ifndef CA_WRAP_H
#define CA_WRAP_H

/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/main/c++/ca/ca-wrap.h $
* $Id: ca-wrap.h 111307 2020-07-08 07:45:00Z bauvirb $
*
* Project       : CODAC Core System
*
* Description   : Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2019 ITER Organization,
*                 CS 90 046
*                 13067 St. Paul-lez-Durance Cedex
*                 France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

// Global header files

#include <cadef.h> // Channel Access API definition, etc.

// Local header files

#include "ChannelAccessHelper.h"

// Constants

// Type definition

// Global variables

// Public function declaration

#ifdef __cplusplus

static inline bool CAInterface_CreateContext (void) { return ::ccs::HelperTools::ChannelAccess::CreateContext(); } // CA wrappers
static inline bool CAInterface_ClearContext (void) { return ::ccs::HelperTools::ChannelAccess::ClearContext(); } // CA wrappers

static inline bool CAInterface_ConnectVariable (const ::ccs::types::char8 * const name, chid& channel) { return ::ccs::HelperTools::ChannelAccess::ConnectVariable(name, channel); } // CA wrappers
static inline bool CAInterface_DetachVariable (chid channel) { return ::ccs::HelperTools::ChannelAccess::DetachVariable(channel); } // CA wrappers

static inline bool CAInterface_IsConnected (chid channel) { return ::ccs::HelperTools::ChannelAccess::IsConnected(channel); } // CA wrappers

static inline bool CAInterface_ReadVariable (chid channel, chtype type, void* p_value) { return ::ccs::HelperTools::ChannelAccess::ReadVariable(channel, type, p_value); } // CA wrappers
static inline bool CAInterface_WriteVariable (chid channel, chtype type, void* p_value) { return ::ccs::HelperTools::ChannelAccess::WriteVariable(channel, type, p_value); } // CA wrappers

#endif // __cplusplus

#endif // CA_WRAP_H

