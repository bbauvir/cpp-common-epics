/******************************************************************************
* $HeadURL: https://svnpub.codac.iter.org/codac/iter/codac/dev/units/m-cpp-common-epics/trunk/src/main/c++/ca/ChannelAccessClient.h $
* $Id: ChannelAccessClient.h 110288 2020-06-12 11:17:42Z bauvirb $
*
* Project	: CODAC Core System
*
* Description	: Infrastructure tools - Prototype
*
* Author        : Bertrand Bauvir
*
* Copyright (c) : 2010-2019 ITER Organization,
*		  CS 90 046
*		  13067 St. Paul-lez-Durance Cedex
*		  France
*
* This file is part of ITER CODAC software.
* For the terms and conditions of redistribution or use of this software
* refer to the file ITER-LICENSE.TXT located in the top level directory
* of the distribution package.
******************************************************************************/

/**
 * @file ChannelAccessClient.h
 * @brief Header file for ChannelAccessClient class.
 * @date 11/11/2018
 * @author Bertrand Bauvir (IO)
 * @copyright 2010-2017 ITER Organization
 * @details This header file contains the definition of the ChannelAccessClient class.
 */

#ifndef _ChannelAccessClient_h_
#define _ChannelAccessClient_h_

// Global header files

#include <functional> // std::function
#include <new> // std::nothrow

//#include <cadef.h> // Channel Access API definition, etc.

#include <common/BasicTypes.h> // Global type definition

#include <common/AnyValue.h> // Variable with introspectable data type ..
#include <common/AnyValueHelper.h> // .. associated helper routines

// Local header files

// Constants

#undef DBR_NOACCESS
#define DBR_NOACCESS DBR_LONG // SDD bug in CCSv5.0
#undef DBR_ULONG
#define DBR_ULONG DBR_DOUBLE // SDD bug in CCSv5.1
#undef DBR_UNDEF
#define DBR_UNDEF 1024 

// Type definition

namespace ccs {

namespace base {

class ChannelAccessClientImpl; // Forward class declaration

/**
 * @brief Interface class providing support for CA client with variable cache.
 * @details The class provides access to a variable cache and asynchronous CA
 * update to ensure non-blocking calls on the application side. The variable
 * cache is implemented by means of ccs::types::AnyValue instances which allow
 * assignment to/from C-like structures.
 *
 * The class also offers a callback mechanism for input or bi-directional
 * variables to allow for application-specific synchronous handling of CA
 * monitor events.
 *
 * @note The design is based on a bridge pattern to avoid exposing CA specific
 * internals through the interface class.
 */

class ChannelAccessClient
{

  private:

    /**
     * @brief Attribute. 
     * @details Bridge (PIMPL) pattern.
     */

    ChannelAccessClientImpl* __impl;

    ChannelAccessClient (const ChannelAccessClient& copy); // Undefined
    ChannelAccessClient& operator= (const ChannelAccessClient& copy); // Undefined

  protected:

  public:

    ChannelAccessClient (void);

    virtual ~ChannelAccessClient (void);

    bool AddVariable (const ccs::types::char8 * const name, ccs::types::DirIdentifier direction, const ccs::base::SharedReference<const ccs::types::AnyType>& type);
    bool AddVariable (const ccs::types::char8 * const name, bool isInput, ccs::types::uint32 type, ccs::types::uint32 mult = 1); // From SDD-generated PV list */

    bool Launch (void); // Should be called after the variable table is populated
    bool SetPeriod (ccs::types::uint64 period);

    bool IsValid (const ccs::types::char8 * const name) const;

    template <typename Type> bool GetVariable (const ccs::types::char8 * const name, Type& value) const;
    template <typename Type> bool SetVariable (const ccs::types::char8 * const name, Type& value);

    ccs::types::AnyValue* GetVariable (const ccs::types::char8 * const name) const;

    bool UpdateVariable (const ccs::types::char8 * const name);

    /**
     * @brief Accessor. SetCallback method.
     * @details The method installs an application callback to be called synchronously when
     * an input or bi-directional variable has been externally updated. The application can
     * use other methods to access the variable cache asynchronously.
     * @param name Variable identifier.
     * @param cb Callback method.
     * @return True if successful.
     *
     * @code
       // Synchronous callback on record change
       void HandleUpdate (const char* name, const ccs::types::AnyValue& value)
       {
         // Handle update ..
         log_info("CA record '%s' updated", name);
	 return;
       }

       bool status = ccs::base::ChannelAccessInterface::GetInstance<ccs::base::ChannelAccessClient>()
                       ->SetCallback("MyVariable", &HandleUpdate);
       @endcode
     *
     * @todo Mechanism for callback to identify the corresponding variable
     */ 

    bool SetCallback (const ccs::types::char8 * const name, std::function<void(const ccs::types::char8 * const, const ccs::types::AnyValue&)> cb);

};

// Global variables

// Function declaration

namespace ChannelAccessInterface {

ChannelAccessClient* GetUniqueChannelAccessClientInstance (void); // Issue with static initialisation and loading shared library objects
void TerminateUniqueChannelAccessClientInstance (void);

template <typename Type> Type* GetInstance (void);
template <typename Type> void Terminate (void);

// Function definition

template <> inline ChannelAccessClient* GetInstance<ChannelAccessClient> (void) 
{ 

  return GetUniqueChannelAccessClientInstance();

} 

template <> inline void Terminate<ChannelAccessClient> (void) 
{ 

  TerminateUniqueChannelAccessClientInstance();
  return;

} 

} // namespace ChannelAccessInterface

template <typename Type> bool ChannelAccessClient::GetVariable (const ccs::types::char8 * const name, Type& value) const 
{ 

  const ccs::types::AnyValue* var = static_cast<ccs::types::AnyValue*>(NULL); 

  bool status = this->IsValid(name); 

  if (status) 
    { 
      var = this->GetVariable(name); 
      value = static_cast<Type>(*var); 
    } 
  
  return status; 
  
}

template <typename Type> bool ChannelAccessClient::SetVariable (const ccs::types::char8 * const name, Type& value) 
{ 

  ccs::types::AnyValue* var = static_cast<ccs::types::AnyValue*>(NULL); 

  bool status = this->IsValid(name); 

  if (status) 
    { 
      var = this->GetVariable(name); 
      *var = value; 
      this->UpdateVariable(name); 
    } 

  return status; 

}

} // namespace base

} // namespace ccs

#endif // _ChannelAccessClient_h_

